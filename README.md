<a name="readme-top"></a>

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="#">
    <img src="public/assets/img/extended/logo_ragunan.png" alt="Logo" width="200">
  </a>

  <h3 align="center"><b>Redesain Website Taman Margasatwa Ragunan</b></h3>

  <p align="center">
    <b>Kelompok 8 IMK - 3SI3</b>
    <br />
  </p>
</div>

<!-- ANGGOTA KELOMPOK -->
<details>
  <summary><b>Anggota Kelompok</b></summary>
  <ol>
    <li>
        Ahmad Faqih Pratama (<a href="mailto:222011345@stis.ac.id">222011345</a>)
    </li>
    <li>
        Roby Awaludin Fajar (<a href="mailto:222011392@stis.ac.id">222011392</a>)
    </li>
    <li>
        Rafel Ilham Febrian (<a href="mailto:222011407@stis.ac.id">222011407</a>)
    </li>
    <li>
        Zenitha Shafira Arrasya (<a href="mailto:222011718@stis.ac.id">222011718</a>)
    </li>

  </ol>
</details>

<!-- TABLE OF CONTENTS -->
<details>
  <summary><b>Daftar Isi</b></summary>
  <ol>
    <li>
      <a href="#about-the-project">Tentang Website Taman Margasatwa Ragunan</a>
      <ul>
        <li><a href="#built-with">Dibangun Dengan</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Prasyarat dan Instalasi</a>
      <ul>
        <li><a href="#prerequisites">Prasyarat</a></li>
        <li><a href="#installation">Instalasi</a></li>
      </ul>
    </li>
    <li><a href="#sw">Konten Website Taman Margasatwa Ragunan</a></li>
    <li><a href="#gambar">Gallery</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

<a name="about-the-project"></a>

## Tentang Website Taman Margasatwa Ragunan

Webite ragunan digunakan oleh Taman Margasatwa Ragunan telah menggunakan situs web sebagai sarana promosi dan komunikasi dengan calon pengunjung.

Untuk mengakses hasil redesain website Taman Margasatwa Ragunan dapat melalui tautan berikut 
"<a href="https://222011392.student.stis.ac.id" target="_blank">Hasil Redesain Website Taman Margasatwa Ragunan</a>"


<a name="built-with"></a>

### Dibangun Dengan

Hasil redesign website ragunan dibangun dengan beberapa teknologi

-   [![CodeIgniter4][codeigniter4]][ci4-url]
-   [![Bootstrap][bootstrap.com]][bootstrap-url]
-   [![JQuery][jquery.com]][jquery-url]
-   [![Fontawesome][fontawesome]][fontawesome-url]
-   [![Sweetalert2][sweetalert2]][sweetalert2-url]

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- GETTING STARTED -->

<a name="getting-started"></a>

## Prasyarat dan Instalasi

<a name="prerequisites"></a>

### Prasyarat

-   Pastikan git sudah terinstall di komputer anda. Unduh git [disini](https://git-scm.com/downloads).
-   Pastikan composer sudah terinstall di komputer anda. Unduh composer [disini](https://getcomposer.org/download/).

<a name="installation"></a>

### Instalasi

1. Clone repository ini ke dalam folder tertentu di komputer Anda. Jika Anda mengakses lewat `gitstis`, maka clone melalui repository berikut `git clone https://git.stis.ac.id/robeeh13/ragunanzoo.git` dan jika anda mengakses lewat `gitlab`, silahkan clone melalui repository berikut `git clone https://gitlab.com/byyy13/ragunanzoo.git`
2. Jalankan perintah `composer install` pada folder proyek untuk menginstall library yang dibutuhkan.
3. Jalankan perintah `php spark serve` untuk menjalankan server lokal. Buka browser dan akses `localhost:8080` untuk
   melihat hasilnya.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- Konten Website Taman Margasatwa Ragunan -->

<a name="sw"></a>

## Konten Web Ragunan

-   [x] <a href="#dberanda"> Beranda </a>
-   [x] <a href="#dpengaduan"> Pengaduan </a>
-   [x] <a href="#dpengunjung"> Info Pengunjung </a>
-   [x] <a href="#dflorafauna"> Flora & Fauna </a>
-   [x] <a href="#dfasilitas"> Fasilitas </a>
-   [x] <a href="#dtentang"> Tentang </a>

Untuk lihat screenshot website secara keseluruhan dapat dilihat
<a href="#gambar">
disini
</a>

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<a name="gambar"></a>

## Gallery

<!-- GALLERY NYA BISA DI FOLD BIAR ENAK -->
<details>
  <summary>Foto</summary>

<a name="dberanda"></a>

### Beranda

##### Slider Carousel dan Informasi Penting

![beranda_1][beranda_1]
##### Statistik dan Berita Terbaru
![beranda_2][beranda_2]

##### Informasi Satwa
![beranda_3][beranda_3]

##### Newsletter dan Footer
![beranda_4][beranda_4]

<p align="right">(<a href="#sw">Another Konten</a>)</p>

<a name="dpengaduan"></a>

### Pengaduan

#### Landing Page Pengaduan
![pengaduan_1][pengaduan_1]
![pengaduan_2][pengaduan_2]
![pengaduan_3][pengaduan_3]
![pengaduan_4][pengaduan_4]

#### Pengaduan Masyarakat
![pengaduan_2][pengaduan_3]

#### Pengaduan Whistle Blowing System
![pengaduan_3][pengaduan_2]


<p align="right">(<a href="#sw">Another Konten</a>)</p>

<a name="dpengunjung"></a>

### Info Pengunjung

#### Berita dan Kegiatan
![pengunjung_1][pengunjung_1]

##### Detail Berita dan Kegiatan
![pengunjung_11][pengunjung_11]
![pengunjung_12][pengunjung_12]

#### Peta dan Petunjuk Arah
![pengunjung_2][pengunjung_2]

#### Informasi Tiket
![pengunjung_3][pengunjung_3]

#### Jadwal Makan Satwa
![pengunjung_4][pengunjung_4]

#### Standar Pelayanan
![pengunjung_5][pengunjung_5]

#### Layanan
![pengunjung_6][pengunjung_6]

<p align="right">(<a href="#sw">Another Konten</a>)</p>

<a name="dflorafauna"></a>

### Flora & Fauna

#### Daftar Flora
![florafauna_1][florafauna_1]

#### Detail Flora
![florafauna_2][florafauna_2]

#### Daftar Fauna
![florafauna_3][florafauna_3]
![florafauna_4][florafauna_4]

#### Detail Fauna
![florafauna_5][florafauna_5]

<p align="right">(<a href="#sw">Another Konten</a>)</p>

<a name="dfasilitas"></a>

### Fasilitas

![fasilitas_1][fasilitas_1]
![fasilitas_2][fasilitas_2]

<p align="right">(<a href="#sw">Another Konten</a>)</p>

<a name="dtentang"></a>

### Tentang

![tentang_1][tentang_1]
![tentang_2][tentang_2]

<p align="right">(<a href="#sw">Another Konten</a>)</p>

<!-- LINK IMAGES -->

[beranda_1]: public/dokumentasi/beranda_1.png
[beranda_2]: public/dokumentasi/beranda_2.png
[beranda_3]: public/dokumentasi/beranda_3.png
[beranda_4]: public/dokumentasi/beranda_4.png
[pengaduan_1]: public/dokumentasi/pengaduan_1.png
[pengaduan_2]: public/dokumentasi/pengaduan_2.png
[pengaduan_3]: public/dokumentasi/pengaduan_3.png
[pengaduan_4]: public/dokumentasi/pengaduan_4.png
[pengunjung_1]: public/dokumentasi/pengunjung_1.png
[pengunjung_2]: public/dokumentasi/pengunjung_2.png
[pengunjung_3]: public/dokumentasi/pengunjung_3.png
[pengunjung_4]: public/dokumentasi/pengunjung_4.png
[pengunjung_5]: public/dokumentasi/pengunjung_5.png
[pengunjung_6]: public/dokumentasi/pengunjung_6.png
[pengunjung_11]: public/dokumentasi/pengunjung_11.png
[pengunjung_12]: public/dokumentasi/pengunjung_12.png
[florafauna_1]: public/dokumentasi/florafauna_1.png
[florafauna_2]: public/dokumentasi/florafauna_2.png
[florafauna_3]: public/dokumentasi/florafauna_3.png
[florafauna_4]: public/dokumentasi/florafauna_4.png
[florafauna_5]: public/dokumentasi/florafauna_5.png
[fasilitas_1]: public/dokumentasi/fasilitas_1.png
[fasilitas_2]: public/dokumentasi/fasilitas_2.png
[tentang_1]: public/dokumentasi/tentang_1.png
[tentang_2]: public/dokumentasi/tentang_2.png
[codeigniter4]: https://img.shields.io/badge/codeigniter4-F8333C?style=for-the-badge&logo=codeigniter&logoColor=white
[ci4-url]: https://www.codeigniter.com/
[bootstrap.com]: https://img.shields.io/badge/Bootstrap-563D7C?style=for-the-badge&logo=bootstrap&logoColor=white
[bootstrap-url]: https://getbootstrap.com
[jquery.com]: https://img.shields.io/badge/jQuery-0769AD?style=for-the-badge&logo=jquery&logoColor=white
[jquery-url]: https://jquery.com
[leaflet]: https://img.shields.io/badge/leaflet.js-7DCD85?style=for-the-badge&logo=leaflet&logoColor=white
[leaflet-url]: https://leafletjs.com/
[fontawesome]: https://img.shields.io/badge/Font_Awesome-339AF0?style=for-the-badge&logo=font-awesome&logoColor=white
[fontawesome-url]: https://fontawesome.com/
[sweetalert2]: https://img.shields.io/badge/sweetalert2-EE2E24?style=for-the-badge&logo=sweetalert2&logoColor=white
[sweetalert2-url]: https://sweetalert2.github.io/
