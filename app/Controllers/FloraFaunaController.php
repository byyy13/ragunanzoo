<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\FaunaModel;
use App\Models\FloraModel;

class FloraFaunaController extends BaseController
{
    public function flora()
    {
        $model = new FloraModel();
        $tumbuhan = $model->getAllFlora();

        // jumlah item perhalaman
        $perPage = 8;

        // Mendapatkan nomor halaman saat ini dari parameter URL
        $page = $this->request->getVar('page') ? $this->request->getVar('page') : 1;

        // Menghitung informasi paginasi
        $totalItems = count($tumbuhan);
        $totalPages = ceil($totalItems / $perPage);
        $offset = ($page - 1) * $perPage;

        // Mendapatkan data yang dipaginasi
        $dataPaginated = array_slice($tumbuhan, $offset, $perPage);

        $data = [
            'active' => 'flora',
            'tumbuhan' => $dataPaginated,
            'currentPage' => $page,
            'totalPages' => $totalPages
        ];

        // dd($data);
        return view('florafauna/flora', $data);
    }

    public function floraDetail($tumbuhan)
    {
        $model = new FloraModel();
        $tumbuhan = $model->getFloraBySlug(trim($tumbuhan));

        $data = [
            'active' => 'flora',
            'tumbuhan' => $tumbuhan,
        ];

        // dd($data);
        return view('florafauna/flora_detail', $data);
    }

    public function fauna()
    {
        $data = [
            'active' => 'fauna',
        ];
        return view('florafauna/fauna', $data);
    }

    public function faunaTipe($tipe)
    {
        $trimmed_tipe = trim($tipe);
        $model = new FaunaModel();
        $hewan = $model->getAllAnimalByTipe($trimmed_tipe);

        if ($trimmed_tipe == 'mamalia') {
            $promo_img = '/assets/img/extended/mamalia3.jpg';
        } else if ($trimmed_tipe == 'reptil') {
            $promo_img = '/assets/img/extended/reptil4.jpg'; 
            // $promo_img = '/assets/img/extended/buaya_1.jpg';
        } else if ($trimmed_tipe == 'aves') {
            $promo_img = '/assets/img/extended/aves3.jpg';
        } else if ($trimmed_tipe == 'pisces') {
            $promo_img = '/assets/img/extended/pisces4.jpg';
        }

        // jumlah item perhalaman
        $perPage = 8;

        // Mendapatkan nomor halaman saat ini dari parameter URL
        $page = $this->request->getVar('page') ? $this->request->getVar('page') : 1;

        // Menghitung informasi paginasi
        $totalItems = count($hewan);
        $totalPages = ceil($totalItems / $perPage);
        $offset = ($page - 1) * $perPage;

        // Mendapatkan data yang dipaginasi
        $dataPaginated = array_slice($hewan, $offset, $perPage);

        $data = [
            'promo_img' => $promo_img,
            'active' => 'fauna',
            'tipe' => $trimmed_tipe,
            'hewan' => $dataPaginated,
            'currentPage' => $page,
            'totalPages' => $totalPages
        ];
        // dd($data);
        return view('florafauna/fauna_tipe', $data);
    }

    public function faunaDetail($tipe, $slug)
    {
        $model = new FaunaModel();
        $hewan = $model->getAnimalBySlug(trim($tipe), trim($slug));

        $data = [
            'active' => 'fauna',
            'tipe' => trim($tipe),
            'slug' => $slug,
            'hewan' => $hewan,
        ];

        // dd($data);
        return view('florafauna/fauna_detail', $data);
    }
}