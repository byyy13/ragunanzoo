<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Fasilitas extends BaseController
{
    public function index()
    {
        //
    }

    public function pusatPrimata()
    {
        $data = [
            'active' => 'pusat_primata',
        ];
        return view('/fasilitas/pusat_primata', $data);
    }

    public function tamanRefleksi()
    {
        $data = [
            'active' => 'taman_refleksi',
        ];


        return view('/fasilitas/taman_refleksi', $data);
    }

    public function satwaAnak()
    {
        $data = [
            'active' => 'satwa_anak',
        ];
        return view('/fasilitas/satwa_anak', $data);
    }

    public function rekreasi()
    {
        $data = [
            'active' => 'sarana_rekreasi',
        ];
        return view('/fasilitas/sarana_rekreasi', $data);
    }
}
