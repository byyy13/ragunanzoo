<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Tentang extends BaseController
{
    public function profil()
    {
        $data = [
            'active' => 'profil',
        ];
        return view('/tentang/profil', $data);
    }

    public function kontak()
    {
        $data = [
            'active' => 'kontak',
        ];
        return view('/tentang/kontak', $data);
    }
}