<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\BeritaModel;
use App\Models\StandarPelayananModel;

class InfoPengunjung extends BaseController
{
    public function beritaKegiatan()
    {
        $model = new BeritaModel();
        $berita = $model->getAllBerita();

        // jumlah item perhalaman
        $perPage = 4;

        // Mendapatkan nomor halaman saat ini dari parameter URL
        $page = $this->request->getVar('page') ? $this->request->getVar('page') : 1;

        // Menghitung informasi paginasi
        $totalItems = count($berita);
        $totalPages = ceil($totalItems / $perPage);
        $offset = ($page - 1) * $perPage;

        // Mendapatkan data yang dipaginasi
        $dataPaginated = array_slice($berita, $offset, $perPage);

        $data = [
            'active' => 'berita_kegiatan',
            'berita' => $dataPaginated,
            'currentPage' => $page,
            'totalPages' => $totalPages,
            'berita_terbaru' => $berita,
        ];

        // dd($data);

        return view('/infopengunjung/berita_kegiatan', $data);
    }

    public function beritaKegiatanDetail($slug)
    {
        $model = new BeritaModel();
        $berita_all = $model->getAllBerita();
        $berita = $model->getBeritaBySlug(trim($slug));
        $data = [
            'active' => 'berita_kegiatan',
            'berita' => $berita,
            'berita_terbaru' => $berita_all,
        ];

        // dd($data);

        return view('/infopengunjung/berita_kegiatan_detail', $data);
    }

    public function cariBerita()
    {
        $model = new BeritaModel();
        $berita = $model->getAllBerita();
        $keyword = $this->request->getVar('search');
        $data = [
            'active' => 'berita_kegiatan',
            'berita' => $berita,
            'keyword' => $keyword,
        ];

        // dd($data);

        return view('/infopengunjung/berita_kegiatan_cari', $data);
    }

    public function jadwalMakanSatwa()
    {
        $data = [
            'active' => 'jadwal_makan',
        ];

        return view('infopengunjung/jadwal_makan', $data);
    }

    public function tiket()
    {
        $data = [
            'active' => 'tiket',
        ];

        return view('/infopengunjung/tiket', $data);
    }

    public function layanan()
    {
        $data = [
            'active' => 'layanan',
        ];

        return view('infopengunjung/layanan', $data);
    }

    public function peta()
    {
        $data = [
            'active' => 'peta',
        ];

        return view('/infopengunjung/peta', $data);
    }

    public function standarPelayanan()
    {
        $model = new StandarPelayananModel();
        $standar_pelayanan = $model->getAllStandarPelayanan();

        $data = [
            'active' => 'standar_pelayanan',
            'sp' => $standar_pelayanan,
            'sp2' => $standar_pelayanan,
        ];

        // dd($data);

        return view('/infopengunjung/standar_pelayanan', $data);
    }
}