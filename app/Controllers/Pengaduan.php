<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Pengaduan extends BaseController
{
    public function index()
    {
        $data = [
            'active' => 'pengaduan',
        ];
        return view('/pengaduan/index', $data);
    }

    public function dumas()
    {
        $data = [
            'active' => 'pengaduan',
        ];
        return view('/pengaduan/dumas', $data);
    }

    public function wbs()
    {
        $data = [
            'active' => 'pengaduan',
        ];
        return view('/pengaduan/wbs', $data);
    }
}
