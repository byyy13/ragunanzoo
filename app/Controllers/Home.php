<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\BeritaModel;
use App\Models\FaunaModel;
use App\Models\FloraModel;

class Home extends BaseController
{
    // public function index()
    // {
    //     return view('welcome_message');
    // }

    public function index()
    {
        $modelBerita = new BeritaModel();
        $berita = $modelBerita->getAllBerita();
        $modelFauna = new FaunaModel();
        $data = [
            'active' => 'home',
            'berita' => $berita,
            'mammalia' => $modelFauna->getAllAnimalByTipe('mamalia'),
            'aves' => $modelFauna->getAllAnimalByTipe('aves'),
            'pisces' => $modelFauna->getAllAnimalByTipe('pisces'),
            'reptilia' => $modelFauna->getAllAnimalByTipe('reptil'),
        ];

        // dd($data);

        return view('landing_page', $data);
    }

    public function cari()
    {
        $beritaModel = new BeritaModel();
        $faunaModel = new FaunaModel();
        $floraModel = new FloraModel();

        $berita = $beritaModel->getAllBerita();
        $mammalia = $faunaModel->getAllAnimalByTipe('mamalia');
        $flora = $floraModel->getAllFlora();
        $keyword = $this->request->getVar('search');
        $data = [
            'active' => 'home',
            'berita' => $berita,
            'keyword' => $keyword,
            'mammalia' => $mammalia,
            'flora' => $flora,
        ];
        return view('cari', $data);
    }
}