<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
// $routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.


// HOME PAGE
$routes->get('/', 'Home::index');
$routes->get('/cari', 'Home::cari');

// PENGADUAN
$routes->get('/pengaduan', 'Pengaduan::index');
$routes->get('/pengaduan/dumas', 'Pengaduan::dumas');
$routes->get('/pengaduan/wbs', 'Pengaduan::wbs');

// INFO PENGUJUNG
$routes->group('info-pengunjung', function ($routes) {
    $routes->get('berita-kegiatan', 'InfoPengunjung::beritaKegiatan');
    $routes->get('berita-kegiatan/cari', 'InfoPengunjung::cariBerita');
    $routes->get('berita-kegiatan/(:segment)', 'InfoPengunjung::beritaKegiatanDetail/$1');
    $routes->get('jadwal-makan-satwa', 'InfoPengunjung::jadwalMakanSatwa');
    $routes->get('tiket', 'InfoPengunjung::tiket');
    $routes->get('layanan', 'InfoPengunjung::layanan');
    $routes->get('peta', 'InfoPengunjung::peta');
    $routes->get('standar-pelayanan', 'InfoPengunjung::standarPelayanan');
});

// FLORA FAUNA
$routes->group('satwa-ragunan', function ($routes) {
    $routes->get('flora', 'FloraFaunaController::flora');
    $routes->get('flora/(:segment)', 'FloraFaunaController::floraDetail/$1');
    $routes->get('fauna', 'FloraFaunaController::fauna');
    $routes->get('fauna/(:segment)/(:segment)', 'FloraFaunaController::faunaDetail/$1/$2');
    $routes->get('fauna/(:segment)', 'FloraFaunaController::faunaTipe/$1');
});

// FASILITAS
$routes->group('fasilitas', function ($routes) {
    $routes->get('pusat-primata', 'Fasilitas::pusatPrimata');
    $routes->get('taman-refleksi', 'Fasilitas::tamanRefleksi');
    $routes->get('taman-satwa-anak', 'Fasilitas::satwaAnak');
    $routes->get('sarana-rekreasi', 'Fasilitas::rekreasi');
});

// TENTANG
$routes->get('/profil', 'Tentang::profil');
$routes->get('/kontak', 'Tentang::kontak');


/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}