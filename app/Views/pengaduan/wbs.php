<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<!-- banner start-->
<section class="promo-primary" data-aos="fade-up">
    <picture>
        <source srcset="/assets/img/banner-2.jpg" media="(min-width: 992px)" /><img class="img--bg" src="/assets/img/banner-2.jpg" alt="img" />
    </picture>
    <div class="container">
        <div class="row">
            <div class="col-auto">
                <div class="align-container">
                    <div class="align-container__item"><span class="promo-primary__pre-title">Taman Margasatwa Ragunan</span>
                        <h1 class="promo-primary__title"><span>Whistle Blowing</span> <span>System</span></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- banner end-->

<!-- section start-->
<section class="section" data-aos="zoom-in">
    <div class="container">
        <div class="row bottom-50">
            <div class="col-12">
                <div class="heading heading--primary heading--center">
                    <h2 class="heading__title no-margin-bottom"><span>Whistle Blowing</span> <span>System</span></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-10 offset-lg-1 col-xl-8 offset-xl-2">
                <form class="form" id="wbs-form" action="javascript:void(0);" method="post">
                    <?= csrf_field(); ?>
                    <div class="row">
                        <!-- Section Data Pelapor -->
                        <div class="col-12 my-3">
                            <h5>Data Pelapor</h5>
                        </div>
                        <div class=" form-group col-md-6">
                            <label for="name">Nama Pelapor</label>
                            <input class="form__field" type="text" name="name" id="name" placeholder="John Doe" />
                        </div>
                        <div class="form-group col-md-6">
                            <label for="email">Alamat Email</label>
                            <input class="form__field" type="email" name="email" id="email" placeholder="johndoe@gmail.com" />
                        </div>
                        <div class="form-group col-md-6">
                            <label for="telp">Nomor Telepon</label>
                            <input class="form__field" type="text" name="telp" id="telp" placeholder="0852xxxxxxx" />
                        </div>
                        <!--Section Isi Pengaduan -->
                        <div class="col-12 my-3">
                            <h5>Isi Pengaduan</h5>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="topik">Topik Pengaduan</label>
                            <input class="form__field" type="text" name="topik" id="topik" placeholder="" />
                        </div>
                        <div class="form-group col-md-6">
                            <label for="tanggal">Tanggal Kejadian</label>
                            <input class="form__field" type="date" name="tanggal" id="tanggal" placeholder="" />
                        </div>
                        <div class="form-group col-12">
                            <label for="deskripsi">Deskripsi Pengaduan</label>
                            <textarea class="form__field form__message" name="deskripsi" id="deskripsi" placeholder=""></textarea>
                        </div>
                        <div class="form-group col-md-8">
                            <label for="lampiran">Lampiran Pengaduan</label> <br>
                            <input class="" type="file" name="lampiran" id="lampiran" placeholder="" />
                            <small id="lampiran" class="form-text text-muted">Maksimal ukuran file 2MB. Jika ada banyak file/foto silahkan digabungkan jadi 1 file.</small>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="deskripsiLampiran">Deskripsi Lampiran</label>
                            <input class="form__field" type="text" name="deskripsiLampiran" id="deskripsiLampiran" placeholder="" />
                        </div>
                        <div class="col-12 text-center mt-5">
                            <input class="form__submit" type="submit" value="Kirim" />
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- section end-->


<?= $this->endSection(); ?>