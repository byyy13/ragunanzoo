<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<!-- banner start-->
<section class="promo-primary" data-aos="fade-up">
    <picture>
        <source srcset="/assets/img/banner-2.jpg" media="(min-width: 992px)" /><img class="img--bg"
            src="/assets/img/banner-2.jpg" alt="img" />
    </picture>
    <div class="container">
        <div class="row">
            <div class="col-auto">
                <div class="align-container">
                    <div class="align-container__item"><span class="promo-primary__pre-title">Taman Margasatwa
                            Ragunan</span>
                        <h1 class="promo-primary__title"><strong> Whistle Blowing System (WBS)</strong>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- banner end-->

<!-- info start-->
<section class="section pb-5" data-aos="fade-right">
    <div class="container">
        <div class="row flex-column-reverse flex-lg-row justify-content-center">
            <div class="col-lg-10 col-xl-10">
                <div class="heading heading--style-2 heading--primary heading--center">
                    <span class="heading__pre-title">UNIT PENGELOLA TAMAN MARGASATWA RAGUNAN</span>
                    <h2 class="heading__title"><span>Whistle Blowing System</span></h2>
                </div>
                <p class="text-center">WBS System adalah sarana informasi untuk menyampaikan adanya indikasi tindak
                    pidana korupsi di lingkungan TMR yang dilakukan oleh pegawai negeri pada TMR dengan memberikan
                    kronologis dan melampirkan bukti sebagai informasi awal dan hak pelapor (pemberi informasi)
                    mendapatkan perlindungan dengan identitas yang dirahasiakan.</p>

                <div class="heading heading--primary heading--center">
                    <a class="button button--primary button--radius" href="/pengaduan/dumas">Pengaduan Masyarakat</a>
                    <a class="button button--primary button--radius button--filled my-3 mx-3"
                        href="/pengaduan/wbs">Whistle Blowing System</a>
                </div>

                <!-- <div class="heading heading--primary heading--style-2 text-center">
                    <h2 class="heading__title text-center">
                        <span>
                            <h5>Identitas Anda Dijamin Kerahasiaanya</h5>
                        </span>
                    </h2>
                </div> -->
            </div>
        </div>
    </div>
</section>
<!-- info end-->

<!-- accordion start -->
<section class="section no-padding-top" data-aos="zoom-in">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-10">
                <div class="heading heading--style-2 heading--primary heading--center">
                    <span class="heading__pre-title">Ekstra</span>
                    <h2 class="heading__title"><span>Informasi</span></h2>
                </div>
                <div class="accordion accordion--primary ">
                    <div class="accordion__title-block">
                        <h6 class="accordion__title">Apa itu WBS?</h6><span class="accordion__close"></span>
                    </div>
                    <div class="accordion__text-block">
                        <p>Whistle Blowing System adalah sistem pelaporan pelanggaran yang memungkinkan setiap orang
                            untuk melaporkan adanya dugaan kecurangan, pelanggaran hukum dan etika serta misconduct
                            lainnya yang dilakukan di Lingkungan Unit Pengelola Taman Margasatwa Ragunan. Kami menjamin
                            kerahasian identitas serta memberikan perlindungan kepada pelapor.</p>
                    </div>
                </div>
                <div class="accordion accordion--primary">
                    <div class="accordion__title-block">
                        <h6 class="accordion__title">Kriteria Pelaporan</h6><span class="accordion__close"></span>
                    </div>
                    <div class="accordion__text-block">
                        <p>Laporkan segala kegiatan yang berindikasi pelanggaran di lingkungan Unit Pengelola Taman
                            Margasatwa Ragunan. Bentuk materi pelanggaran yang dilaporkan: </p>
                        <ol class="ordered-list">
                            <li>Pelanggaran Disiplin Pegawai</li>
                            <li>Penyalahgunaan Wewenang, Mal Administrasi dan Pemerasan/Penganiayaan</li>
                            <li>Perilaku Amoral/Perselingkuhan dan Kekerasan dalam Rumah Tangga</li>
                            <li>Korupsi</li>
                            <li>Pengadaan Barang dan Jasa/BAMA</li>
                            <li>Pungutan Liar, Percaloan, dan Pengurusan Dokumen</li>
                            <li>Narkoba</li>
                            <li>Pelanggaran Disiplin Pegawai</li>
                            <li>Pelayanan Publik</li>
                            <li>Laporan dan Klarifikasi</li>
                        </ol>
                    </div>
                </div>
                <div class="accordion accordion--primary">
                    <div class="accordion__title-block">
                        <h6 class="accordion__title">Dasar Hukum</h6><span class="accordion__close"></span>
                    </div>
                    <div class="accordion__text-block">
                        <p>Laporkan segala kegiatan yang berindikasi pelanggaran di lingkungan Unit Pengelola Taman
                            Margasatwa Ragunan. Bentuk materi pelanggaran yang dilaporkan:</p>
                        <ol class="ordered-list">
                            <li>Surat Edaran MenPAN-RB no. 1/2013 tentang Tindak Lanjut Koordinasi, Monitoring dan
                                Evaluasi Kebijakan Pemberantasan Korupsi dan Pembangungan Zona Integritas di Lingkungan
                                Kementerian / Lembaga dan Pemerintah Daerah</li>
                            <li>Surat Edaran Mahkamah Agung no. 4/2011 tentang Perlakuan bagi Pelapor Tindak Pidana dan
                                Saksi Pelaku yang Bekerjasama di dalam Perkara Tindak Pidana Tertentu</li>
                            <li>Undang-undang no 13/2006 tentang Perlindungan Saksi dan Korban</li>
                            <li>Undang-undang no. 30/2002 tentang Komisi Pemberantasan Tindak Pidana Korupsi</li>
                            <li>Undang-undang no. 20/2001 tentang Pemberantasan Tindak Pidana Korupsi</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- accordion end -->


<?= $this->endSection(); ?>