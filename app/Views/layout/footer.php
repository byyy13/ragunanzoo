<footer class="footer"><img class="footer__bg img--bg" src="/assets/img/background__layout.png" alt="bg" />
    <div class="container">
        <div class="row bottom-50">
            <!-- TO DO 1: LOGO WILD-WOLRD  -->
            <div class="col-md-5 col-xl-4 text-center text-md-left">
                <a class="logo logo--footer" href="#">
                    <img class="logo__img" src="/assets/img/extended/logo_ragunan.png" alt="logo" />
                </a>
                <div class="footer__details">
                    <p><span>Taman Margasatwa Ragunan</span></p>
                    <p><span>Jalan Harsono RM No. 1, Ragunan, Pasar Minggu,</span></p>
                    <p><span>Jakarta Selatan, DKI Jakarta, 12550, Indonesia</span></p>
                    <p><span>Tel. (021) 7884 7114 </span></p>
                    <p><span>Fax. (021) 780 5280</span></p>

                    <p>Email: <a href="mailto:ragunanzoo@jakarta.go.id">ragunanzoo@jakarta.go.id</a></p>
                </div>
                <ul class="socials">
                    <li class="socials__item">
                        <a class="socials__link" target="_blank" href="https://www.facebook.com/ragunanzoo">
                            <i class="fa-brands fa-facebook"></i>
                        </a>
                    </li>
                    <li class="socials__item">
                        <a class="socials__link" target="_blank" href="https://www.twitter.com/ragunanzoo?lang=id">
                            <i class="fa-brands fa-twitter" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li class="socials__item">
                        <a class="socials__link" target="_blank"
                            href="https://www.youtube.com/channel/UCiMlGb0PLS47E83wgT96WyA">
                            <i class="fa-brands fa-youtube-play" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li class="socials__item">
                        <a class="socials__link" target="_blank" href="https://www.instagram.com/ragunanzoo/">
                            <i class="fa-brands fa-instagram" aria-hidden="true"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-7 col-xl-5 d-none d-md-block">
                <h6 class="footer__title">Informasi Lainnya</h6>
                <ul class="footer-menu">
                    <li class="footer-menu__item"><a class="footer-menu__link" href="/">Beranda</a></li>
                    <li class="footer-menu__item"><a class="footer-menu__link" href="/pengaduan">Pengaduan </a></li>
                    <li class="footer-menu__item"><a class="footer-menu__link"
                            href="/info-pengunjung/berita-kegiatan">Berita & Kegiatan</a></li>
                    <li class="footer-menu__item"><a class="footer-menu__link" href="/info-pengunjung/peta">Peta &
                            Petunjuk Arah</a></li>
                    <li class="footer-menu__item"><a class="footer-menu__link" href="/info-pengunjung/tiket">Tiket</a>
                    </li>
                    <li class="footer-menu__item"><a class="footer-menu__link"
                            href="/info-pengunjung/jadwal-makan-satwa">Jadwal Makan Satwa</a></li>
                    <li class="footer-menu__item"><a class="footer-menu__link"
                            href="/info-pengunjung/standar-pelayanan">Standar Pelayanan</a></li>
                    <li class="footer-menu__item"><a class="footer-menu__link"
                            href="/info-pengunjung/layanan">Layanan</a></li>
                    <li class="footer-menu__item"><a class="footer-menu__link" href="/satwa-ragunan/flora">Flora</a>
                    </li>
                    <li class="footer-menu__item"><a class="footer-menu__link" href="/satwa-ragunan/fauna">Fauna</a>
                    </li>
                    <li class="footer-menu__item"><a class="footer-menu__link" href="/profil">Profil</a></li>
                    <li class="footer-menu__item"><a class="footer-menu__link" href="/kontak">Kontak</a></li>
                </ul>
            </div>
            <div class="col-lg-3 d-none d-xl-block">
                <h6 class="footer__title"><span>Gallery</span>
                    <!-- LOGO INSTAGRAM (DISABLE) -->
                    <!-- <i class="fa fa-instagram" aria-hidden="true"></i> -->
                </h6>
                <div class="footer-instagram"><a class="footer-instagram__item" href="#">
                        <div class="footer-instagram__img"><img class="img--bg" src="/assets/img/f_ig-1.jpg" alt="ig" />
                        </div>
                    </a><a class="footer-instagram__item" href="#">
                        <div class="footer-instagram__img"><img class="img--bg" src="/assets/img/f_ig-2.jpg" alt="ig" />
                        </div>
                    </a><a class="footer-instagram__item" href="#">
                        <div class="footer-instagram__img"><img class="img--bg" src="/assets/img/f_ig-3.jpg" alt="ig" />
                        </div>
                    </a><a class="footer-instagram__item" href="#">
                        <div class="footer-instagram__img"><img class="img--bg" src="/assets/img/f_ig-4.jpg" alt="ig" />
                        </div>
                    </a><a class="footer-instagram__item" href="#">
                        <div class="footer-instagram__img"><img class="img--bg" src="/assets/img/f_ig-5.jpg" alt="ig" />
                        </div>
                    </a><a class="footer-instagram__item" href="#">
                        <div class="footer-instagram__img"><img class="img--bg" src="/assets/img/f_ig-6.jpg" alt="ig" />
                        </div>
                    </a></div>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-sm-6 text-center text-sm-left">
                <div class="footer-privacy">
                    <a class="footer-privacy__link" href="#">Copyright &copy <?= date("Y"); ?> Kelompok 8 IMK 3SI3</a>
                </div>
            </div>
        </div>
    </div>
</footer>