        <!-- menu dropdown start-->
        <div class="menu-dropdown menu-dropdown--front-2">
            <div class="menu-dropdown__inner" data-value="start">
                <div class="screen screen--start">
                    <div class="screen__item">
                        <form class="search-form" action="/cari" method="GET">
                            <input style="color: #f8f9fa;" class="search-form__input" type="search" name="search"
                                placeholder="Cari di sini" autocomplete="off" />
                            <button class="search-form__submit" type="submit">
                                <i class="fa-solid fa-magnifying-glass"></i>
                            </button>
                        </form>
                    </div>
                    <div class="screen__item <?= ($active == 'home') ? 'item--active' : '' ?>"><a class="screen__link"
                            href="/">Beranda</a></div>
                    <div class="screen__item <?= ($active == 'pengaduan') ? 'item--active' : '' ?>"><a
                            class="screen__link" href="/pengaduan">Pengaduan</a></div>
                    <div class="screen__item screen--trigger <?= ($active == 'berita_kegiatan' || $active == 'peta' || $active == 'tiket' || $active == 'jadwal_makan' || $active == 'layanan') ? 'item--active' : '' ?>"
                        data-category="info-pengunjung">
                        <span>Info Pengunjung</span>
                        <span>
                            <span class="icon"><i class="fa-solid fa-angle-right" aria-hidden="true"></i></span>
                        </span>
                    </div>
                    <div class="screen__item screen--trigger <?= ($active == 'flora' || $active == 'fauna') ? 'item--active' : '' ?>"
                        data-category="flora-fauna"><span>Flora & Fauna</span>
                        <span>
                            <span class="icon"><i class="fa-solid fa-angle-right" aria-hidden="true"></i>
                            </span>
                        </span>
                    </div>
                    <div class="screen__item screen--trigger <?= ($active == 'pusat_primata' || $active == 'taman_refleksi' || $active == 'satwa_anak' || $active == 'sarana_rekreasi') ? 'item--active' : '' ?>"
                        data-category="fasilitas"><span>Fasilitas</span><span>
                            <span class="icon"><i class="fa-solid fa-angle-right" aria-hidden="true"></i>
                            </span>
                        </span>
                    </div>
                    <div class="screen__item screen--trigger <?= ($active == 'profil' || $active == 'kontak') ? 'item--active' : '' ?>"
                        data-category="tentang"><span>Tentang</span><span>
                            <span class="icon"><i class="fa-solid fa-angle-right" aria-hidden="true"></i>
                            </span>
                        </span>
                    </div>
                </div>
            </div>

            <div class="menu-dropdown__inner" data-value="info-pengunjung">
                <div class="screen screen--sub">
                    <div class="screen__heading">
                        <h6 class="screen__back">
                            <span>
                                <span class="icon"><i class="fa-solid fa-angle-left" aria-hidden="true"></i></span>
                            </span>
                            <span>Info Pengunjung</span>
                        </h6>
                    </div>
                    <div class="screen__item <?= ($active == 'berita_kegiatan') ? 'item--active' : '' ?>"><a
                            class="screen__link" href="/info-pengunjung/berita-kegiatan">Berita & Kegiatan</a></div>
                    <div class="screen__item <?= ($active == 'peta') ? 'item--active' : '' ?>">
                        <a class="screen__link" href="/info-pengunjung/peta">Peta & Petunjuk Arah</a>
                    </div>
                    <div class="screen__item <?= ($active == 'tiket') ? 'item--active' : '' ?>">
                        <a class="screen__link" href="/info-pengunjung/tiket">Tiket</a>
                    </div>
                    <div class="screen__item <?= ($active == 'jadwal_makan') ? 'item--active' : '' ?>">
                        <a class="screen__link" href="/info-pengunjung/jadwal-makan-satwa">Jadwal Makan Satwa</a>
                    </div>
                    <div class="screen__item <?= ($active == 'standar_pelayanan') ? 'item--active' : '' ?>">
                        <a class="screen__link" href="/info-pengunjung/standar-pelayanan">Standar Pelayanan</a>
                    </div>
                    <div class="screen__item <?= ($active == 'layanan') ? 'item--active' : '' ?>">
                        <a class="screen__link" href="/info-pengunjung/layanan">Layanan</a>
                    </div>
                    <!-- <div class="screen__item">
                        <a class="screen__link" href="">Pendaftaran Kunjungan</a>
                    </div> -->
                </div>
            </div>

            <div class="menu-dropdown__inner" data-value="flora-fauna">
                <div class="screen screen--sub">
                    <div class="screen__heading">
                        <h6 class="screen__back">
                            <span>
                                <span class="icon"><i class="fa-solid fa-angle-left" aria-hidden="true"></i></span>
                            </span>
                            <span>Flora & Fauna</span>
                        </h6>
                    </div>
                    <div class="screen__item <?= ($active == 'flora') ? 'item--active' : '' ?>"><a class="screen__link"
                            href="/satwa-ragunan/flora">Flora</a></div>
                    <div class="screen__item <?= ($active == 'fauna') ? 'item--active' : '' ?>"><a class="screen__link"
                            href="/satwa-ragunan/fauna">Fauna</a></div>
                </div>
            </div>

            <div class="menu-dropdown__inner" data-value="fasilitas">
                <div class="screen screen--sub">
                    <div class="screen__heading">
                        <h6 class="screen__back">
                            <span>
                                <span class="icon"><i class="fa-solid fa-angle-left" aria-hidden="true"></i></span>
                            </span>
                            <span>Fasilitas</span>
                        </h6>
                    </div>
                    <div class="screen__item <?= ($active == 'pusat_primata') ? 'item--active' : '' ?>"><a
                            class="screen__link" href="/fasilitas/pusat-primata">Pusat Primata Schmutzer</a></div>
                    <div class="screen__item <?= ($active == 'taman_refleksi') ? 'item--active' : '' ?>"><a
                            class="screen__link" href="/fasilitas/taman-refleksi">Taman Refleksi</a></div>
                    <div class="screen__item <?= ($active == 'satwa_anak') ? 'item--active' : '' ?>"><a
                            class="screen__link" href="/fasilitas/taman-satwa-anak">Taman Satwa Anak</a></div>
                    <div class="screen__item <?= ($active == 'sarana_rekreasi') ? 'item--active' : '' ?>"><a
                            class="screen__link" href="/fasilitas/sarana-rekreasi">Sarana Rekreasi</a></div>
                </div>
            </div>

            <div class="menu-dropdown__inner" data-value="tentang">
                <div class="screen screen--sub">
                    <div class="screen__heading">
                        <h6 class="screen__back">
                            <span>
                                <span class="icon"><i class="fa-solid fa-angle-left" aria-hidden="true"></i></span>
                            </span>
                            <span>Tentang</span>
                        </h6>
                    </div>
                    <div class="screen__item <?= ($active == 'profil') ? 'item--active' : '' ?>"><a class="screen__link"
                            href="/profil">Profil</a></div>
                    <div class="screen__item <?= ($active == 'kontak') ? 'item--active' : '' ?>"><a class="screen__link"
                            href="/kontak">Kontak</a></div>
                </div>
            </div>
        </div>
        <!-- menu dropdown end-->

        <!-- header start-->
        <header class="header shop-header header-f2">
            <div class="header__top">
                <div class="row align-items-center">
                    <!-- LOGO RAGUNAN -->
                    <div class="col-8 col-lg-2">
                        <a class="logo" href="/">
                            <img class="logo--white logo__img" src="/assets/img/extended/logo_ragunan.png" alt="logo" />
                            <img class="logo--dark logo__img" src="/assets/img/extended/logo_ragunan.png" alt="logo" />
                        </a>
                    </div>
                    <div class="col-4 col-lg-10 d-flex justify-content-end align-items-center">
                        <!-- main menu start-->
                        <ul class="main-menu">
                            <!-- beranda start -->
                            <li class="main-menu__item <?= ($active == 'home') ? 'main-menu__item--active' : '' ?>"><a
                                    class="main-menu__link" href="/"><span>Beranda</span></a></li>
                            <!-- beranda end -->

                            <!-- pengaduan start -->
                            <li
                                class="main-menu__item <?= ($active == 'pengaduan') ? 'main-menu__item--active' : '' ?>">
                                <a class="main-menu__link" href="/pengaduan"><span>Pengaduan</span></a>
                            </li>
                            <!-- pengaduan end -->

                            <!-- info pengunjung start -->
                            <li
                                class="main-menu__item main-menu__item--has-child <?= ($active == 'berita_kegiatan' || $active == 'peta' || $active == 'tiket' || $active == 'jadwal_makan' || $active == 'layanan' || $active == 'standar_pelayanan') ? 'main-menu__item--active' : '' ?>">
                                <a class="main-menu__link" href="javascript:void(0);"><span>Info Pengunjung</span></a>
                                <!-- sub menu start-->
                                <ul class="main-menu__sub-list">
                                    <li class="<?= ($active == 'berita_kegiatan') ? 'item--active' : '' ?>">
                                        <a href="/info-pengunjung/berita-kegiatan">
                                            <span>Berita & Kegiatan</span>
                                        </a>
                                    </li>
                                    <li class="<?= ($active == 'peta') ? 'item--active' : '' ?>">
                                        <a href="/info-pengunjung/peta"><span>Peta & Petunjuk Arah</span></a>
                                    </li>
                                    <li class="<?= ($active == 'tiket') ? 'item--active' : '' ?>">
                                        <a href="/info-pengunjung/tiket"><span>Tiket</span></a>
                                    </li>
                                    <li class="<?= ($active == 'jadwal_makan') ? 'item--active' : '' ?>">
                                        <a href="/info-pengunjung/jadwal-makan-satwa">
                                            <span>Jadwal Makan Satwa</span>
                                        </a>
                                    </li>
                                    <li class="<?= ($active == 'standar_pelayanan') ? 'item--active' : '' ?>"><a
                                            href="/info-pengunjung/standar-pelayanan"><span>Standar Pelayanan</span></a>
                                    </li>
                                    <li class="<?= ($active == 'layanan') ? 'item--active' : '' ?>"><a
                                            href="/info-pengunjung/layanan"><span>Layanan</span></a></li>
                                    <!-- <li><a href="#"><span>Pendaftaran Kunjungan</span></a></li> -->
                                </ul>
                                <!-- sub menu end-->
                            </li>
                            <!-- info pengunjung end -->

                            <!-- flora & fauna start -->
                            <li
                                class="main-menu__item main-menu__item--has-child <?= ($active == 'flora' || $active == 'fauna') ? 'main-menu__item--active' : '' ?>">
                                <a class="main-menu__link" href="javascript:void(0);"><span>Flora & Fauna</span></a>
                                <!-- sub menu start-->
                                <ul class="main-menu__sub-list">
                                    <li class="<?= ($active == 'flora') ? 'item--active' : '' ?>"><a
                                            href="/satwa-ragunan/flora"><span>Flora</span></a></li>
                                    <li class="<?= ($active == 'fauna') ? 'item--active' : '' ?>"><a
                                            href="/satwa-ragunan/fauna"><span>Fauna</span></a></li>
                                </ul>
                                <!-- sub menu end-->
                            </li>
                            <!-- flora & fauna end -->

                            <!-- fasilitas start -->
                            <li
                                class="main-menu__item main-menu__item--has-child <?= ($active == 'pusat_primata' || $active == 'taman_refleksi' || $active == 'satwa_anak' || $active == 'sarana_rekreasi') ? 'main-menu__item--active' : '' ?>">
                                <a class="main-menu__link" href="javascript:void(0);"><span>Fasilitas</span></a>
                                <!-- sub menu start-->
                                <ul class="main-menu__sub-list">
                                    <li class="<?= ($active == 'pusat_primata') ? 'item--active' : '' ?>"><a
                                            href="/fasilitas/pusat-primata"><span>Pusat Primata Schmutzer</span></a>
                                    </li>
                                    <li class="<?= ($active == 'taman_refleksi') ? 'item--active' : '' ?>"><a
                                            href="/fasilitas/taman-refleksi"><span>Taman Refleksi</span></a>
                                    </li>
                                    <li class="<?= ($active == 'satwa_anak') ? 'item--active' : '' ?>"><a
                                            href="/fasilitas/taman-satwa-anak"><span>Taman Satwa Anak</span></a>
                                    </li>
                                    <li class="<?= ($active == 'sarana_rekreasi') ? 'item--active' : '' ?>"><a
                                            href="/fasilitas/sarana-rekreasi"><span>Sarana Rekreasi</span></a>
                                    </li>
                                </ul>
                                <!-- sub menu end-->
                            </li>
                            <!-- fasiltias end -->

                            <!-- tentang start -->
                            <li
                                class="main-menu__item main-menu__item--has-child <?= ($active == 'profil' || $active == 'kontak') ? 'main-menu__item--active' : '' ?>">
                                <a class="main-menu__link" href="javascript:void(0);"><span>Tentang</span></a>
                                <!-- sub menu start-->
                                <ul class="main-menu__sub-list">
                                    <li class="<?= ($active == 'profil') ? 'item--active' : '' ?>"><a
                                            href="/profil"><span>Profil</span></a></li>
                                    <li class="<?= ($active == 'kontak') ? 'item--active' : '' ?>"><a
                                            href="/kontak"><span>Kontak</span></a></li>
                                </ul>
                                <!-- sub menu end-->
                            </li>
                            <!-- tentang end -->

                        </ul>
                        <!-- main menu end-->

                        <!-- search box start -->
                        <div class="col-3 d-none d-lg-flex">
                            <form class="search-form" action="/cari" method="GET">
                                <input style="background-color:#f8f9fa" class="search-form__input py-2 " type="search"
                                    autocomplete="off" name="search" placeholder="Cari di sini" />
                                <button style="height: 50px; width: 50px;" class="search-form__submit" type="submit">
                                    <i class="fa-solid fa-magnifying-glass"></i>
                                </button>
                            </form>
                        </div>
                        <!-- search box end -->

                        <!-- menu-trigger start-->
                        <div class="hamburger">
                            <div class="hamburger-box">
                                <div class="hamburger-inner"></div>
                            </div>
                        </div>
                        <!-- menu-trigger end-->
                    </div>
                </div>
            </div>
        </header>
        <!-- header end-->