<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<!-- banner start-->
<section class="promo-primary" data-aos="fade-up">
    <picture>
        <source srcset="/assets/img/banner-4.jpg" media="(min-width: 992px)" /><img class="img--bg" src="/assets/img/banner-4.jpg" alt="img" />
    </picture>
    <div class="container">
        <div class="row">
            <div class="col-auto">
                <div class="align-container">
                    <div class="align-container__item"><span class="promo-primary__pre-title">Fasilitas</span>
                        <h1 class="promo-primary__title"><span></span> <span>Sarana Rekreasi</span></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- banner end-->

<!-- gajah tunggang start-->
<section class="section" data-aos="fade-left">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-xl-6 offset-xl-1 align-self-center">
                <div class="heading heading--primary heading--style-2"><span class="heading__pre-title">Tentang</span>
                    <h4>Gajah Tunggang</h4>
                    </span></h2>
                </div>
                <p>Menunggang gajah bisa menjadi suatu tantangan tersendiri bagi anda. Satwa yang berukuran besar dan tinggi ini bisa menjadi sangat jinak bersama perawat sehingga pengunjung dapat merasakan berkeliling dengan nyaman dan aman.</p>
                <p>Atraksi ini hanya akan anda jumpai pada hari Minggu dan Libur Nasional saja. Coba dan rasakan sensasi menunggang gajah hanya dengan tiket Rp. 7.500 per orang.</p>
            </div>
            <div class="col-lg-6 col-xl-5 align-self-center">
                <div class="img-box">
                    <div class="img-box__img"><img class="img--bg" src="/assets/img/extended/gajah-tunggang-2.jpg" alt="img" /></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- gajah tunggang end-->

<!-- onta tunggang start-->
<section class="section pt-5 pb-5 background--gray" data-aos="fade-right">
    <div class="container">
        <div class="row flex-column-reverse flex-lg-row">
            <div class="col-lg-6 col-xl-5 align-self-center">
                <div class="img-box">
                    <div class="img-box__img"><img class="img--bg" src="/assets/img/extended/onta-tunggang.jpg" alt="img" /></div>
                </div>
            </div>
            <div class="col-lg-6 col-xl-6 offset-xl-1 align-self-center">
                <div class="heading heading--primary heading--style-2"><span class="heading__pre-title">Tentang</span>
                    <h4>Onta Tunggang</h4>
                    </span></h2>
                </div>
                <p>Tidak perlu pergi ke Arab Saudi atau ke Mesir untuk bisa merasakan nikmatnya naik Onta. Cukuplah di Taman Margasatwa Ragunan anda bisa menunggang Onta. Terbayang hamparan pasir yang luas saat anda mengendarai Onta punuk satu ini. Untuk harga tiket naik Onta Rp. 7.500,-</p>
            </div>
        </div>
    </div>
</section>
<!-- onta tunggang end-->

<!-- kereta keliling start-->
<section class="section pt-5 pb-5" data-aos="fade-left">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-xl-6 offset-xl-1 align-self-center">
                <div class="heading heading--primary heading--style-2"><span class="heading__pre-title">Tentang</span>
                    <h4>Kereta Keliling</h4>
                    </span></h2>
                </div>
                <p>Mengelilingi Taman Margasatwa Ragunan seluas 147 hektar akan membuat lelah. Kereta keliling akan mengantar anda mengelilingi luas Taman Margasatwa Ragunan dan menjumpai satwa di sepanjang rute kereta keliling dengan harga tiket kereta Rp. 7.500,-</p>
            </div>
            <div class="col-lg-6 col-xl-5 align-self-center">
                <div class="img-box">
                    <div class="img-box__img"><img class="img--bg" src="/assets/img/extended/kereta-keliling.jpg" alt="img" /></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- kereta keliling end-->

<!-- penyewaan sepeda start-->
<section class="section pt-5 pb-5 background--gray" data-aos="fade-right">
    <div class="container">
        <div class="row flex-column-reverse flex-lg-row align-self-center">
            <div class="col-lg-6 col-xl-5 align-self-center">
                <div class="img-box">
                    <div class="img-box__img"><img class="img--bg" src="/assets/img/extended/penyewaan-sepeda.jpg" alt="img" /></div>
                </div>
            </div>
            <div class="col-lg-6 col-xl-6 offset-xl-1 align-self-center">
                <div class="heading heading--primary heading--style-2"><span class="heading__pre-title">Tentang</span>
                    <h4>Penyewaan Sepeda</h4>
                    </span></h2>
                </div>
                <p>Bersepeda keliling Taman Margasatwa Ragunan akan lebih menyenangkan dibandingkan hanya jalan kaki saja. Tersedia penyewaan sepeda dengan dua model sepeda yaitu sepeda tunggal ( satu kayuh ) dan sepeda ganda ( dua kayuh ). Untuk menyewa sepeda Rp. 10.000 (sepeda tunggal) per jam dan Rp. 15.000 ( sepeda ganda).</p>
            </div>
        </div>
    </div>
</section>
<!-- penyewaan sepeda end-->

<!-- perahu angsa start-->
<section class="section pt-5 pb-5" data-aos="fade-left">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-xl-6 offset-xl-1 align-self-center">
                <div class="heading heading--primary heading--style-2"><span class="heading__pre-title">Tentang</span>
                    <h4>Taman Perahu Angsa</h4>
                    </span></h2>
                </div>
                <p>Taman perahu merupakan kolam besar, membentang dari utara ke selatan seluas 2.000 m2 , terdapat beberapa pulau di tengahnya. Pulau-pulau buatan ini menambah indahnya perjalanan anda mengelilingi dengan menggunakan perahu angsa. Untuk menikmati perahu angsa anda dikenakan tiket Rp. 15.000,-</p>
            </div>
            <div class="col-lg-6 col-xl-5 align-self-center">
                <div class="img-box">
                    <div class="img-box__img"><img class="img--bg" src="/assets/img/extended/taman-perahu-angsa.jpg" alt="img" /></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- perahu angsa end-->

<!-- pentas satwa start-->
<section class="section pt-5 pb-5 background--gray" data-aos="fade-right">
    <div class="container">
        <div class="row flex-column-reverse flex-lg-row">
            <div class="col-lg-6 col-xl-5 align-self-center">
                <div class="img-box">
                    <div class="img-box__img"><img class="img--bg" src="/assets/img/extended/pentas-satwa.jpg" alt="img" /></div>
                </div>
            </div>
            <div class="col-lg-6 col-xl-6 offset-xl-1 align-self-center">
                <div class="heading heading--primary heading--style-2"><span class="heading__pre-title">Tentang</span>
                    <h4>Pentas Satwa</h4>
                    </span></h2>
                </div>
                <p>Atraksi satwa seperti kakatua bermain gelang warna warni, beruang bersepeda, dan tentu saja dikemas dalam atraksi yang lucu dan akan membuat anda tertawa dengan tingkah satwa-satwa ini. Loket dibuka pukul 10.00 – 15.00 WIB. Tiket masuk yang berlaku Rp 4000 perorang.</p>
            </div>
        </div>
    </div>
</section>
<!-- pentas satwa end-->

<!-- kuda bendi start-->
<section class="section pt-5 pb-5" data-aos="fade-left">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-xl-6 offset-xl-1 align-self-center">
                <div class="heading heading--primary heading--style-2"><span class="heading__pre-title">Tentang</span>
                    <h4>Kuda Bendi</h4>
                    </span></h2>
                </div>
                <p>Kendaraan tradisional ini menjadi alat angkutan yang unik dan sudah langka. Namun masih akan anda jumpai di Taman Margasatwa Ragunan pada saat liburan .Loket buka pukul 10.00 – 15.00 WIB. Harga tiket Rp. 10.000 per bendi dengan penumpang maksimal 4 orang.</p>
            </div>
            <div class="col-lg-6 col-xl-5 align-self-center">
                <div class="img-box">
                    <div class="img-box__img"><img class="img--bg" src="/assets/img/extended/kuda-bendi.jpg" alt="img" /></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- kuda bendi end-->

<!-- kuda tunggang start-->
<section class="section pt-5 pb-5 background--gray" data-aos="fade-right">
    <div class="container">
        <div class="row flex-column-reverse flex-lg-row">
            <div class="col-lg-6 col-xl-5 align-self-center">
                <div class="img-box">
                    <div class="img-box__img"><img class="img--bg img-fluid" src="/assets/img/extended/kuda-tunggang.jpg" alt="img" /></div>
                </div>
            </div>
            <div class="col-lg-6 col-xl-6 offset-xl-1 align-self-center">
                <div class="heading heading--primary heading--style-2"><span class="heading__pre-title">Tentang</span>
                    <h4>Kuda Tunggang</h4>
                    </span></h2>
                </div>
                <p>Tidak seperti kuda pacu. Kuda di sini adalah kuda pony yang tidak terlalu besar. Anda akan didampingi perawat kuda untuk mengelilingi area taman satwa anak. Tiket naik kuda hanya Rp. 5000,- per orang untuk satu kali putaran.</p>
            </div>
        </div>
    </div>
</section>
<!-- kuda tunggang end-->

<!-- section start-->
<section class="section pt-5" data-aos="zoom-in">
    <div class="heading heading--primary heading--center"><span class="heading__pre-title">Ekstra</span>
        <h2 class="heading__title"><span>Fasilitas Lainnya</span></h2>
    </div>
    <div class="row no-gutters">
        <div class="col-xl-4"><a class="action-block" href="/fasilitas/pusat-primata">
                <div class="action-block__inner"><img class="img--bg" src="/assets/img/extended/pusat-primata-1.jpg" alt="img" />
                    <h3 class="action-block__title"><span>Pusat Primata</span><br /> <span>Schmutzer</span></h3>
                </div>
            </a></div>
        <div class="col-xl-4"><a class="action-block" href="/fasilitas/taman-refleksi">
                <div class="action-block__inner"><img class="img--bg" src="/assets/img/extended/taman-refleksi.jpg" alt="img" />
                    <h3 class="action-block__title"><span>Taman</span><br /> <span>Refleksi</span></h3>
                </div>
            </a></div>
        <div class="col-xl-4"><a class="action-block" href=/fasilitas/taman-satwa-anak#">
                <div class="action-block__inner"><img class="img--bg" src="/assets/img/extended/aquarium-arapaima.jpg" alt="img" />
                    <h3 class="action-block__title"><span>Taman</span><br /> <span>Satwa Anak</span></h3>
                </div>
            </a></div>
    </div>
</section>
<!-- section end-->

<?= $this->endSection(); ?>