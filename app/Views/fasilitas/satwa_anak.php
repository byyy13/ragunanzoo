<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<!-- banner start-->
<section class="promo-primary" data-aos="fade-up">
    <picture>
        <source srcset="/assets/img/banner-4.jpg" media="(min-width: 992px)" /><img class="img--bg" src="/assets/img/banner-4.jpg" alt="img" />
    </picture>
    <div class="container">
        <div class="row">
            <div class="col-auto">
                <div class="align-container">
                    <div class="align-container__item"><span class="promo-primary__pre-title">Fasilitas</span>
                        <h1 class="promo-primary__title"><span></span> <span>Taman Satwa Anak</span></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- banner end-->

<!-- info start-->
<section class="section" data-aos="fade-left">
    <img class="section__bg t50 r0" src="/assets/img/about_bg.png" alt="img" />
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-xl-6 offset-xl-1">
                <div class="heading heading--primary heading--style-2"><span class="heading__pre-title">Tentang</span>
                    <h4>Taman Satwa Anak</h4>
                    </span></h2>
                </div>
                <p>Taman Satwa Anak merupakan taman satwa yang menampilkan beberapa satwa yang disukai anak-anak seperti: ikan raksasa arapaima gigas yaitu ikan dengan berat mencapai lebih dari 100 kg, burung kakatua yang pandai berbicara, mini terrarium yang terdapat ular condrophyton yang indah warna-warninya, kapibara yaitu jenis rodentia raksasa (satwa pengerat) dilengkapi dengan sarana permainan anak-anak seperti ayunan, jaring laba-laba dan sebagainya. </p>
                <p>Ikan arapaima raksasa dari sungai Amazon ini dapat dilihat di Kolam Arapaima yang berada di area Taman Satwa Anak Ragunan. Aquarium Arapaima menjadi daya tarik tersendiri karena keunikannya dan menjadi salah satu edukasi tentang akan keberadaan ikan air tawar tersebut.</p>
                <p>Untuk tiket masuknya adalah Rp. 2.500.</p>
            </div>
            <div class="col-lg-6 col-xl-5 align-self-center">
                <div class="img-box">
                    <div class="img-box__img"><img class="img--bg" src="/assets/img/extended/aquarium-arapaima.jpg" alt="img" /></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- info end-->
<!-- section start-->
<section class="section no-padding-top" data-aos="zoom-in">
    <div class="heading heading--primary heading--center"><span class="heading__pre-title">Ekstra</span>
        <h2 class="heading__title"><span>Fasilitas Lainnya</span></h2>
    </div>
    <div class="row no-gutters">
        <div class="col-xl-4"><a class="action-block" href="/fasilitas/pusat-primata">
                <div class="action-block__inner"><img class="img--bg" src="/assets/img/extended/pusat-primata-1.jpg" alt="img" />
                    <h3 class="action-block__title"><span>Pusat Primata</span><br /> <span>Schmutzer</span></h3>
                </div>
            </a></div>
        <div class="col-xl-4"><a class="action-block" href="/fasilitas/taman-refleksi">
                <div class="action-block__inner"><img class="img--bg" src="/assets/img/extended/taman-refleksi.jpg" alt="img" />
                    <h3 class="action-block__title"><span>Taman</span><br /> <span>Refleksi</span></h3>
                </div>
            </a></div>
        <div class="col-xl-4"><a class="action-block" href="/fasilitas/sarana-rekreasi">
                <div class="action-block__inner"><img class="img--bg" src="/assets/img/extended/onta-tunggang.jpg" alt="img" />
                    <h3 class="action-block__title"><span>Sarana</span><br /> <span>Rekreasi</span></h3>
                </div>
            </a></div>
    </div>
</section>
<!-- section end-->

<?= $this->endSection(); ?>