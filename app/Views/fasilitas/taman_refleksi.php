<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<!-- banner start-->
<section class="promo-primary" data-aos="fade-up">
    <picture>
        <source srcset="/assets/img/banner-4.jpg" media="(min-width: 992px)" /><img class="img--bg" src="/assets/img/banner-4.jpg" alt="img" />
    </picture>
    <div class="container">
        <div class="row">
            <div class="col-auto">
                <div class="align-container">
                    <div class="align-container__item"><span class="promo-primary__pre-title">Fasilitas</span>
                        <h1 class="promo-primary__title"><span></span> <span>Taman Refleksi</span></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- banner end-->

<!-- info start-->
<section class="section" data-aos="fade-left">
    <img class="section__bg t50 r0" src="/assets/img/about_bg.png" alt="img" />
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-xl-6 offset-xl-1">
                <div class="heading heading--primary heading--style-2"><span class="heading__pre-title">Tentang</span>
                    <h4>Taman Refleksi</h4>
                    </span></h2>
                </div>
                <p>Mungkin tidak banyak yang tahu bahwa Taman Margasatwa Ragunan memiliki areal khusus untuk refleksi. TempatIMG_3563 itu berupa taman yang dilengkapi dengan batu-batu yang didesain sebagai taman refleksi. Taman Refleksi berada di Central Area Pusat Primata Schmutzer.</p>
                <p>Jika Anda beranjak ke sana, akan terasa lebih nikmat suasana di pagi dan sore hari. Pengunjung biasanya memanfaatkan tempat ini untuk melakukan relaksasi dan olahraga ataupun sekadar berfoto di taman tersebut. Selain itu, dengan icon Orang Utan yang berada tepat di tangah kolam Taman Refleksi, saat ini dilengkapi pula dengan patung Komodo dan patung Gajah yang berukuran sangat besar. Bagi pengunjung yang memiliki disabilitas, terutama yang menggunakan kursi roda, dapat menikmati taman tersebut, sebab akses menuju taman ini juga disiapkan untuk pengguna kursi roda.</p>
                <p>Anda penasaran? Datang dan nikmati Taman Refleksi Taman Margasatwa Ragunan. Tidak dikenakan biaya tambahan untuk menikmati Taman Refleksi ini.</p>
            </div>
            <div class="col-lg-6 col-xl-5 align-self-center">
                <div class="img-box">
                    <div class="img-box__img"><img class="img--bg" src="/assets/img/extended/taman-refleksi.jpg" alt="img" /></div>
                </div>
            </div>
        </div>
        <!-- <div class="row flex-column-reverse flex-lg-row mt-3">
            <div class="col-lg-6 col-xl-6">
                <div class="heading heading--primary heading--style-2"><span class="heading__pre-title">Tentang</span>
                    <h2 class="heading__title"><span></span><br /></h2>
                </div>
                <p>Selanjutnya apabila pengunjung ingin melihat orangutan akan dapat melihatnya melalui terowongan orangutan ( orangutan tunnel ) di dalam terowongan. Selain melihat satwa primata, anda dapat mempelajari kehidupan primata melalui beberapa fasilitas yang ada di dalam pusat primata schmutzer antara lain : dapur makanan satwa, fasilitas pendidikan dengan adanya ruang teater pemutaran film dokumenter, diorama satwa serta perpustakaan.</p>
                <p>Tiket masuk Pusat Primata Schmutzer adalah (selasa-jumat : Rp.6.000,-) dan (sabtu-minggu dan hari libur nasional : Rp.7500,-). Apabila anda ingin menyaksikan film dokumenter Primata anda dikenakan biaya Rp. 150.000 untuk sekali putar dengan kapasitas tempat duduk 85 kursi.</p>
            </div>
            <div class="col-lg-6 col-xl-5">
                <div class="img-box">
                    <div class="img-box__img"><img class="img--contain" src="/assets/img/extended/pusat-primata-2.jpg" alt="img" /></div>
                </div>
            </div>
        </div> -->
    </div>
</section>
<!-- info end-->
<!-- section start-->
<section class="section no-padding-top" data-aos="zoom-in">
    <div class="heading heading--primary heading--center"><span class="heading__pre-title">Ekstra</span>
        <h2 class="heading__title"><span>Fasilitas Lainnya</span></h2>
    </div>
    <div class="row no-gutters">
        <div class="col-xl-4"><a class="action-block" href="/fasilitas/pusat-primata">
                <div class="action-block__inner"><img class="img--bg" src="/assets/img/extended/pusat-primata-1.jpg" alt="img" />
                    <h3 class="action-block__title"><span>Pusat Primata</span><br /> <span>Schmutzer</span></h3>
                </div>
            </a></div>
        <div class="col-xl-4"><a class="action-block" href="/fasilitas/taman-satwa-anak">
                <div class="action-block__inner"><img class="img--bg" src="/assets/img/extended/aquarium-arapaima.jpg" alt="img" />
                    <h3 class="action-block__title"><span>Taman</span><br /> <span>Satwa Anak</span></h3>
                </div>
            </a></div>
        <div class="col-xl-4"><a class="action-block" href="/fasilitas/sarana-rekreasi">
                <div class="action-block__inner"><img class="img--bg" src="/assets/img/extended/onta-tunggang.jpg" alt="img" />
                    <h3 class="action-block__title"><span>Sarana</span><br /> <span>Rekreasi</span></h3>
                </div>
            </a></div>
    </div>
</section>
<!-- section end-->

<?= $this->endSection(); ?>