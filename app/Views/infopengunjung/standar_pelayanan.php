<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>

<section class="promo-primary" data-aos="fade-up">
    <picture>
        <source srcset="/assets/img/extended/standar_pelayanan.jpg" media="(min-width: 992px)" />
        <img class="img--bg" src="/assets/img/extended/standar_pelayanan.jpg" alt="img" />
    </picture>
    <div class="container">
        <div class="row">
            <div class="col-auto">
                <div class="align-container">
                    <div class="align-container__item"><span class="promo-primary__pre-title">Info Pengunjung</span>
                        <h1 class="promo-primary__title"><span style="font-weight: bold;">Standar Pelayanan</span>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section benefits"><img class="benefits__img" src="/assets/img/benefits-bg.png" alt="img" />
    <div class="container">
        <div class="row bottom-50">
            <div class="col-12">
                <div class="heading heading--primary heading--style-2"><span class="heading__pre-title">Layanan</span>
                    <h4 class="heading__title" style="font-size: 1.7rem;">
                        <span>Standar pelayanan pada unit pengelola Taman Margasatwa Ragunan</span>
                    </h4>
                </div>
            </div>
        </div>
        <div class="row offset-margin">
            <?php foreach ($sp as $sp) : ?>
            <div class="col-sm-6 col-lg-3 hover-pointer" data-aos="fade-left" data-toggle="modal"
                data-target="#modal<?= $sp['slug']; ?>">
                <div class="icon-item">
                    <div class="icon-item__icon">
                        <div class="icon-item__img">
                            <i style="color: #fdd340;" class="<?= $sp['icon']; ?>"></i>
                        </div>
                    </div>
                    <h6 class="icon-item__title"><?= $sp['nama']; ?></h6>
                    <p>
                        <?= $sp['deskripsi']; ?>
                        <br><b>Klik untuk detail lebih lanjut</b>
                    </p>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>

<!-- MODAL -->
<?php foreach ($sp2 as $s) : ?>
<div class="modal fade" id="modal<?= $s['slug']; ?>" tabindex=" -1" role="dialog"
    aria-labelledby="modalTitle<?= $s['slug']; ?>" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalTitle<?= $s['slug']; ?>"><?= $s['deskripsi']; ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive py-4">
                    <table class="table table-borderless" style="color: #333;">
                        <thead class="table__header">
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Komponen</th>
                                <th scope="col">Uraian</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            <?php foreach ($s['komponen'] as $key => $value) : ?>
                            <tr>
                                <th scope="row"><?= $i; ?></th>
                                <td><b><?= $key; ?></b></td>
                                <td style="color: #777;">
                                    <?php if ($key == 'Biaya/Tarif') { ?>
                                    Berdasarkan Peraturan Gubernur Nomor 96 Tahun 2011, tarif yang dikenakan sebagai
                                    berikut:
                                    <?php } ?>
                                    <ol class="ordered-list">
                                        <?php foreach ($value as $v) : ?>
                                        <li><?= $v; ?></li>
                                        <?php endforeach; ?>
                                    </ol>
                                </td>
                            </tr>
                            <?php $i++; ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="button button--primary button--filled" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<?php endforeach; ?>

<?= $this->endSection(); ?>