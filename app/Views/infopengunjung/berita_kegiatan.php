<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>

<section class="promo-primary" data-aos="fade-up">
    <picture>
        <source srcset="/assets/img/event-details.jpg" media="(min-width: 992px)" /><img class="img--bg"
            src="/assets/img/event-details.jpg" alt="img" />
    </picture>
    <div class="container">
        <div class="row">
            <div class="col-auto">
                <div class="align-container">
                    <div class="align-container__item"><span class="promo-primary__pre-title">Info Pengunjung</span>
                        <h1 class="promo-primary__title"><span style="font-weight: bold;">Berita & Kegiatan</span>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section blog pt-5" data-aos="zoom-in">
    <div class="container">
        <div class="row bottom-50">
            <div class="col-12">
                <div class="heading heading--style-2 text-center"><span class="heading__pre-title">Seputar Ragunan
                    </span>
                    <h2 class="heading__title no-margin-bottom"><span style="font-weight: normal;">Cari Berita
                            Terbaru</span> <span style="font-weight: bold;">DI SINI</span>
                    </h2>
                    <div class="row justify-content-center pt-4">
                        <div class="col-12">
                            <div class="blog__inner-block">
                                <form class="search-form" action="/info-pengunjung/berita-kegiatan/cari" method="GET">
                                    <input class="search-form__input" type="search" name="search"
                                        placeholder="Cari di sini" />
                                    <button class="search-form__submit" type="submit">
                                        <i class="fa-solid fa-magnifying-glass"></i>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-xl-9">
                <div class="align-items-baseline">
                    <div><span class="shop__aside-trigger">Filter</span></div>
                    <div class="text-right">
                        <ul class="shop-filter">
                            <li class="shop-filter__item shop-filter__item--active"><span>Urutkan terbaru</span>
                                <ul class="shop-filter__sub-list">
                                    <li><a href="javascript:void(0);">Urutkan terlama</a></li>
                                    <li><a href="javascript:void(0);">Urutkan popularitas</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <?php foreach ($berita as $b) : ?>
                        <div class="blog-item" data-aos="fade-left">
                            <div class="blog-item__img"><img class="img--bg" src="<?= $b['gambar']; ?>" alt="blog" />
                                <div class="blog-item__date"><span
                                        style="font-size: 2em;"><?= $b['tanggal']; ?></span><span><?= $b['bulan']; ?>
                                        <?= $b['tahun']; ?></span></div>
                            </div>
                            <h4 class="blog-item__title"><a
                                    href="/info-pengunjung/berita-kegiatan/<?= trim($b['slug']); ?>"><?= $b['judul_berita']; ?></a>
                            </h4>
                            <p><?= $b['deskripsi_singkat']; ?>
                            </p>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="col-12 text-center">
                        <ul class="pagination blog__pagination pagination--rounded">
                            <!-- PREV PAJANGAN -->
                            <li class="pagination__item pagination__item--prev"><i class="fa fa-angle-left"
                                    aria-hidden="true"></i><span></span>
                            </li>
                            <?php for ($i = 1; $i <= $totalPages; $i++) : ?>
                            <?php if ($i == $currentPage) : ?>
                            <li class="pagination__item pagination__item--active"><span><?= $i ?></span></li>
                            <?php else : ?>
                            <a href="<?= site_url('info-pengunjung/berita-kegiatan?page=' . $i) ?>">
                                <li class="pagination__item "><span><?= $i ?></span></li>
                            </a>
                            <?php endif; ?>
                            <?php endfor; ?>
                            <!-- NEXT PAJANGAN -->
                            <li class="pagination__item pagination__item--next"><span></span><i
                                    class="fa fa-angle-right" aria-hidden="true"></i>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-8 offset-md-2 col-lg-4 offset-lg-0 col-xl-3">
                <div>
                    <br><br>
                </div>
                <div class="blog__inner-block bottom-50">
                    <h5 class="blog__title">Tags</h5>
                    <div class="tags tags--style-2">
                        <a class="tags__item" href="javascript:void(0);">Kebun Binatang</a>
                        <a class="tags__item" href="javascript:void(0);">Flora</a>
                        <a class="tags__item" href="javascript:void(0);">Fauna</a>
                        <a class="tags__item" href="javascript:void(0);">Ragunan</a>
                    </div>
                </div>

                <div class="blog__inner-block bottom-50">
                    <h5 class="blog__title">Berita Terbaru</h5>
                    <?php for ($i = 0; $i < 3; $i++) { ?>
                    <div class="latest-item">
                        <div class="row align-items-center">
                            <div class="col-4">
                                <div class="latest-item__img"><img class="img--bg"
                                        src="<?= $berita_terbaru[$i]['gambar']; ?>" alt="img" />
                                </div>
                            </div>
                            <div class="col-8">
                                <h6 class="latest-item__title">
                                    <a href="/info-pengunjung/berita-kegiatan/<?= $berita_terbaru[$i]['slug']; ?>"><?= $berita_terbaru[$i]['judul_berita']; ?>
                                    </a>
                                </h6>
                                <div class="latest-item__details">
                                    <div class="row">
                                        <div class="col-7">
                                            <div class="latest-item__date"><?= $berita_terbaru[$i]['tanggal']; ?>
                                                <?= $berita_terbaru[$i]['bulan']; ?>
                                                <?= $berita_terbaru[$i]['tahun']; ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?= $this->endSection(); ?>