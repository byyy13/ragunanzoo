<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>

<section class="promo-primary" data-aos="fade-up">
    <picture>
        <source srcset="/assets/img/jadwal_makan.jpg" media="(min-width: 992px)" /><img class="img--bg"
            src="/assets/img/jadwal_makan.jpg" alt="img" />
    </picture>
    <div class="container">
        <div class="row">
            <div class="col-auto">
                <div class="align-container">
                    <div class="align-container__item"><span class="promo-primary__pre-title">Info Pengunjung</span>
                        <h1 class="promo-primary__title"><span style="font-weight: bold;">Jadwal Makan Satwa</span>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section calendar pt-5" data-aos="zoom-in">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="heading heading--style-2 text-center pb-4"><span class="heading__pre-title">Jadwal</span>
                    <h2 class="heading__title"><span>Jadwal Makan Satwa</span>
                </div>

                <div class="calendar__block">
                    <div class="row">
                        <div class="col-12 col-lg-12">
                            <div class="calendar-item white-hover">
                                <div class="row align-items-center">
                                    <div class="col-lg-4 col-xl-3">
                                        <div class="calendar-item__img"><img class="img--bg"
                                                src="/assets/img/extended/gorilla_makan.jpg" alt="img" /></div>
                                    </div>
                                    <div class="col-lg-4 col-xl-6">
                                        <div class="calendar-item__details ">
                                            <h6 class="calendar-item__title">
                                                <a href="#">Jadwal Makan Siang Gorilla</a>
                                            </h6>
                                            <span class="calendar-item__category" style="font-weight: 700;">Makan
                                                Siang</span>
                                            <span class="jadwal-satwa-makan"> (12 PM - 12.30 PM)</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-xl-3 text-center d-none d-lg-block">
                                        <div class="calendar-item__time">12 PM - 12.30 PM</div>
                                        <a class="calendar-item__link" href="#">12 PM - 12.30 PM</a>
                                    </div>
                                </div>
                            </div>
                            <div class="calendar-item white-hover">
                                <div class="row align-items-center">
                                    <div class="col-lg-4 col-xl-3">
                                        <div class="calendar-item__img"><img class="img--bg"
                                                src="/assets/img/extended/gajah_makan.jpg" alt="img" /></div>
                                    </div>
                                    <div class="col-lg-4 col-xl-6">
                                        <div class="calendar-item__details ">
                                            <h6 class="calendar-item__title">
                                                <a href="#">Jadwal Makan Siang Gajah Sumatera</a>
                                            </h6>
                                            <span class="calendar-item__category" style="font-weight: 700;">Makan
                                                Siang</span>
                                            <span class="jadwal-satwa-makan"> (2.30 PM - 2.45 PM)</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-xl-3 text-center d-none d-lg-block">
                                        <div class="calendar-item__time">2.30 PM - 2.45 PM</div>
                                        <a class="calendar-item__link" href="#">2.30 PM - 2.45 PM</a>
                                    </div>
                                </div>
                            </div>
                            <div class="calendar-item white-hover">
                                <div class="row align-items-center">
                                    <div class="col-lg-4 col-xl-3">
                                        <div class="calendar-item__img"><img class="img--bg"
                                                src="/assets/img/extended/komodo_makan.webp" alt="img" /></div>
                                    </div>
                                    <div class="col-lg-4 col-xl-6">
                                        <div class="calendar-item__details ">
                                            <h6 class="calendar-item__title">
                                                <a href="#">Jadwal Makan Sore Komodo</a>
                                            </h6>
                                            <span class="calendar-item__category" style="font-weight: 700;">Makan
                                                Sore</span>
                                            <span class="jadwal-satwa-makan"> (3 PM - 3.15 PM)</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-xl-3 text-center d-none d-lg-block">
                                        <div class="calendar-item__time">3 PM - 3.15 PM</div>
                                        <a class="calendar-item__link" href="#">3 PM - 3.15 PM</a>
                                    </div>
                                </div>
                            </div>
                            <div class="calendar-item white-hover">
                                <div class="row align-items-center">
                                    <div class="col-lg-4 col-xl-3">
                                        <div class="calendar-item__img"><img class="img--bg"
                                                src="/assets/img/calendar_1.jpg" alt="img" /></div>
                                    </div>
                                    <div class="col-lg-4 col-xl-6">
                                        <div class="calendar-item__details ">
                                            <h6 class="calendar-item__title">
                                                <a href="#">Jadwal Makan Sore Gajah Sumatera</a>
                                            </h6>
                                            <span class="calendar-item__category" style="font-weight: 700;">Makan
                                                Sore</span>
                                            <span class="jadwal-satwa-makan"> (3 PM - 3.15 PM)</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-xl-3 text-center d-none d-lg-block">
                                        <div class="calendar-item__time">3 PM - 3.15 PM</div>
                                        <a class="calendar-item__link" href="#">3 PM - 3.15 PM</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?= $this->endSection(); ?>