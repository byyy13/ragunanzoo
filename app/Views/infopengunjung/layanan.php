<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>

<section class="promo-primary" data-aos="fade-up">
    <picture>
        <source srcset="/assets/img/extended/layanan.jpg" media="(min-width: 992px)" /><img class="img--bg"
            src="/assets/img/extended/layanan.jpg" alt="img" />
    </picture>
    <div class="container">
        <div class="row">
            <div class="col-auto">
                <div class="align-container">
                    <div class="align-container__item"><span class="promo-primary__pre-title">Info Pengunjung</span>
                        <h1 class="promo-primary__title"><span style="font-weight: bold;">Layanan</span>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- praktek kerja lapangan start-->
<section class="section pt-5 pb-5" data-aos="fade-right">
    <div class="container">
        <div class="row flex-column-reverse flex-lg-row">
            <div class="col-lg-6 col-xl-5 align-self-center">
                <div class="img-box">
                    <div class="img-box__img"><img class="img--bg" src="/assets/img/extended/pkl.jpg" alt="img" /></div>
                </div>
            </div>
            <div class="col-lg-6 col-xl-6 offset-xl-1 align-self-center">
                <div class="heading heading--primary heading--style-2"><span class="heading__pre-title">Layanan</span>
                    <h4>Praktek Kerja Lapangan</h4>
                    </span></h2>
                </div>
                <p>Taman Margasatwa Ragunan memberikan kesempatan kepada siswa SMP/SMU/SMK, mahasiswa biologi, mahasiswa
                    kedokteran hewan dan mahasiswa umum dapat melaksanakan praktek kerja lapangan.</p>

                <p>Pelaksanaan praktek kerja lapangan di Taman Margasatwa Ragunan bagi siswa SMP/SMU/SMK, mahasiswa
                    biologi, mahasiswa kedokteran hewan serta mahasiswa umum akan ditempatkan sesuai jurusan dan tema
                    yang dipilih dalam kegiatan praktek kerja lapangan.</p>
            </div>
        </div>
    </div>
</section>
<!-- praktek kerja lapangan end-->

<!-- penelitian start-->
<section class="section pt-5 pb-5 background--gray" data-aos="fade-left">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-xl-6 offset-xl-1 align-self-center">
                <div class="heading heading--primary heading--style-2"><span class="heading__pre-title">Layanan</span>
                    <h4>Penelitian</h4>
                    </span></h2>
                </div>
                <p>Taman Margasatwa Ragunan merupakan contoh lembaga konservasi exsitu sebagai laboratorium alam dengan
                    koleksi 67 jenis satwa Mamalia, 101 jenis satwa Aves, 34 jenis satwa Reptil, 16 jenis satwa Pisces
                    dan 171 jenis tumbuh-tumbuhan.
                </p>

                <p>
                    Taman Margasatwa Ragunan juga menjadi pusat penelitian satwa-satwa langka yang ada di Indonesia.
                    Para peneliti, mahasiswa dari pendidikan dalam/luar negeri dapat melaksanakan penelitian yang
                    berkaitan dengan satwa, tumbuhan, lingkungan sebagai bahan untuk kajian ilmiah. Selain itu, Taman
                    Margasatwa Ragunan merupakan tempat yang tepat bagi pelajar dari TK sampai Perguruan Tinggi untuk
                    mengenal/mengetahui dan mengamati satwa dari dekat, dibandingkan harus di hutan/habitat aslinya.
                </p>
            </div>
            <div class="col-lg-6 col-xl-5 align-self-center">
                <div class="img-box">
                    <div class="img-box__img"><img class="img--bg" src="/assets/img/extended/penelitian.jpg"
                            alt="img" /></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- penelitian end-->

<!-- pemandu wisata start-->
<section class="section pt-5 pb-5" data-aos="fade-right">
    <div class="container">
        <div class="row flex-column-reverse flex-lg-row">
            <div class="col-lg-6 col-xl-5 align-self-center">
                <div class="img-box">
                    <div class="img-box__img"><img class="img--bg" src="/assets/img/extended/pemandu_wisata.jpg"
                            alt="img" /></div>
                </div>
            </div>
            <div class="col-lg-6 col-xl-6 offset-xl-1 align-self-center">
                <div class="heading heading--primary heading--style-2"><span class="heading__pre-title">Layanan</span>
                    <h4>Pemandu Wisata</h4>
                    </span></h2>
                </div>
                <p>Taman Margasatwa Ragunan menyediakan pemandu wisata bagi siswa TK, SD, SMP dan SMU.</p>
                <p>
                    Pemandu akan berkeliling selama 1 - 2 jam mendampingi siswa sambil menerangkan satwa-satwa di Taman
                    Margasatwa Ragunan. Kegiatan siswa ditemani pemandu akan membuat kunjungan lebih terarah dan kental
                    dengan nuansa belajar di alam. Pelayanan pemandu dilaksanakan pada hari Selasa - Jumat pukul 09.00 -
                    14.00 wib melalui konfirmasi sebelumnya.
                </p>
            </div>
        </div>
    </div>
</section>
<!-- pemandu wisata end-->


<?= $this->endSection(); ?>