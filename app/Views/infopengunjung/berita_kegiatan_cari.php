<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>

<section class="promo-primary" data-aos="fade-up">
    <picture>
        <source srcset="/assets/img/event-details.jpg" media="(min-width: 992px)" /><img class="img--bg"
            src="/assets/img/event-details.jpg" alt="img" />
    </picture>
    <div class="container">
        <div class="row">
            <div class="col-auto">
                <div class="align-container">
                    <div class="align-container__item"><span class="promo-primary__pre-title">Info Pengunjung</span>
                        <h1 class="promo-primary__title"><span style="font-weight: bold;">Berita & Kegiatan</span>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section pt-5" data-aos="zoom-in">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="heading heading--primary heading--center"><span class="heading__pre-title">Pencarian</span>
                    <h2 class="heading__title"><span>Hasil Pencarian untuk "<?= $keyword; ?>"</span></h2>
                    <p class="no-margin-bottom">
                        <b>Disclaimer</b>: Hasil pencarian yang ditampilkan adalah <b>hasil dummy</b>
                    </p>
                </div>
            </div>
            <div class="row mt-5 px-3">
                <?php for ($i = 0; $i < 3; $i++) { ?>
                <div class="col-md-6 col-xl-4" data-aos="fade-left">
                    <div class="events-item">
                        <div class="events-item__img">
                            <div class="events-item__date">
                                <span>
                                    <?= $berita[$i]['tanggal']; ?></span><span><?= $berita[$i]['bulan']; ?>
                                    <?= $berita[$i]['tahun']; ?>
                                </span>
                            </div>
                            <img class="img--bg" src="<?= $berita[$i]['gambar']; ?>" alt="img" />
                        </div>
                        <h6 class="events-item__title"><a class="events-item__link"
                                href="/info-pengunjung/berita-kegiatan/<?= $berita[$i]['slug']; ?>"><?= $berita[$i]['judul_berita']; ?></a>
                        </h6>
                        <p>
                            <?= $berita[$i]['deskripsi_singkat']; ?>
                        </p>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>

<?= $this->endSection(); ?>