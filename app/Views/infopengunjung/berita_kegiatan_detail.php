<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>

<section class="promo-primary" data-aos="fade-up">
    <picture>
        <source srcset="/assets/img/event-details.jpg" media="(min-width: 992px)" /><img class="img--bg"
            src="/assets/img/event-details.jpg" alt="img" />
    </picture>
    <div class="container">
        <div class="row">
            <div class="col-auto">
                <div class="align-container">
                    <div class="align-container__item"><span class="promo-primary__pre-title">Info Pengunjung</span>
                        <h1 class="promo-primary__title"><span style="font-weight: bold;">Berita & Kegiatan</span>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section donation-details" data-aos="zoom-in">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="row ">
                    <div class="col-12">
                        <h3 class="donation-details__title mt-0"><strong><?= $berita['judul_berita']; ?></strong></h3>
                        <span><strong><?= $berita['tanggal_lengkap']; ?></strong></span>

                        <?php if ($berita['link_youtube'] === '') { ?>
                        <div class="donation-details__img">
                            <img class="img--bg" src="<?= $berita['gambar']; ?>" alt="img" />
                        </div>
                        <?php } else { ?>
                        <div class="row">
                            <div class="col-12">
                                <div class="video-frame">
                                    <img class="img--bg" src="<?= $berita['gambar']; ?>" alt="frame" />
                                    <a class="video-trigger video-frame__trigger"
                                        href="<?= $berita['link_youtube']; ?>">
                                        <span class="video-frame__icon">
                                            <i class="fa fa-play" aria-hidden="true"></i>
                                        </span>
                                        <span class="video-frame__text">
                                            <?= $berita['judul_berita']; ?>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        <br>
                        <!-- DESKRIPSI LENGKAP -->
                        <?php foreach ($berita['deskripsi_lengkap'] as $dl) : ?>
                        <p><?= $dl; ?></p>
                        <?php endforeach; ?>
                    </div>
                    <!-- GALLERY FOTO -->
                    <?php if ($berita['gambar_lengkap'] != null) { ?>
                    <div class="col-12">
                        <div class="row bottom-30">
                            <div class="col-md-8">
                                <h5 class="tour-details__title no-margin-bottom">Galeri Foto</h5>
                            </div>
                            <div class="col-md-4 text-left text-md-right">
                                <div class="tours-slider__dots"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="tours-slider">
                                    <?php foreach ($berita['gambar_lengkap'] as $gl) : ?>
                                    <a class="tours-slider__item photo-trigger" href="<?= $gl; ?>"
                                        data-fancybox="gallery">
                                        <img class="img--bg" src="<?= $gl; ?>" alt="img" />
                                    </a>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <h6 class="comments__trigger top-30 bottom-30">1 Komentar</h6>
                <div class="comments">
                    <div class="comments__item">
                        <div class="comments__item-img"><img class="img--bg" src="/assets/img/author_2.png" alt="img" />
                        </div>
                        <div class="comments__item-description">
                            <div class="row align-items-center">
                                <div class="col-12 col-sm-4 col-md-3">
                                    <span class="comments__item-name">Roby Awaludin Fajar
                                    </span>
                                </div>
                                <div class="col-10 col-md-7"><span class="comments__item-date">
                                        17 Juni 2023
                                    </span></div>
                                <div class="col-2 text-right"><span class="comments__item-action">
                                        <svg class="icon">
                                            <use xlink:href="#previous"></use>
                                        </svg></span></div>
                                <div class="col-12">
                                    <div class="comments__item-text">
                                        <p>Keren sekali, sangat bermanfaat.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <h6 class="blog-post__title bottom-30">Berikan komentar</h6>
                <form id="komentar-form" class="form " action="javascript:void(0);" method="POST">
                    <?= csrf_field(); ?>
                    <div class="row">
                        <div class="form-group col-12">
                            <label for="komentarMessage">Isi Komentar</label>
                            <textarea class="form__field form__message" name="komentarMessage" id="komentarMessage"
                                placeholder="Komentar"></textarea>
                        </div>
                        <div class=" form-group col-md-6">
                            <label for="komentarFirstName">Nama</label>
                            <input class="form__field" type="text" name="komentarFirstName" id="komentarFirstName"
                                placeholder="Robeeh" />
                        </div>
                        <div class="form-group col-md-6">
                            <label for="komentarEmail">Alamat Email</label>
                            <input class="form__field" type="email" name="komentarEmail" id="komentarEmail"
                                placeholder="robeeh@gmail.com" />
                        </div>

                        <div class="col-12 mt-2 mb-5">
                            <input class="form__submit" type="submit" value="Kirim Komentar" />
                        </div>

                    </div>
                </form>
            </div>
            <div class="col-md-8 offset-md-2 col-lg-3 offset-lg-0">
                <div class="blog__inner-block bottom-50">
                    <h5 class="blog__title">Berita Terkait</h5>
                    <?php for ($i = 0; $i < 3; $i++) { ?>
                    <div class="latest-item">
                        <div class="row align-items-center">
                            <div class="col-4">
                                <div class="latest-item__img"><img class="img--bg"
                                        src="<?= $berita_terbaru[$i]['gambar']; ?>" alt="img" />
                                </div>
                            </div>
                            <div class="col-8">
                                <h6 class="latest-item__title">
                                    <a href="/info-pengunjung/berita-kegiatan/<?= $berita_terbaru[$i]['slug']; ?>"><?= $berita_terbaru[$i]['judul_berita']; ?>
                                    </a>
                                </h6>
                                <div class="latest-item__details">
                                    <div class="row">
                                        <div class="col-7">
                                            <div class="latest-item__date">
                                                <?= $berita_terbaru[$i]['tanggal']; ?>
                                                <?= $berita_terbaru[$i]['bulan']; ?>
                                                <?= $berita_terbaru[$i]['tahun']; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?= $this->endSection(); ?>