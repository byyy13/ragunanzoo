<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<!-- banner start-->
<section class="promo-primary" data-aos="fade-up">
    <picture>
        <source srcset="/assets/img/banner-5.jpg" media="(min-width: 992px)" /><img class="img--bg" src="/assets/img/banner-5.jpg" alt="img" />
    </picture>
    <div class="container">
        <div class="row">
            <div class="col-auto">
                <div class="align-container">
                    <div class="align-container__item"><span class="promo-primary__pre-title">Info Pengunjung</span>
                        <h1 class="promo-primary__title"><span></span> <span>Peta & Petunjuk Arah</span></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- banner end-->

<!-- lokasi start-->
<section class="section" data-aos="fade-left">
    <div class="container">
        <div class="row flex-column-reverse flex-lg-row">
            <div class="col-lg-12 col-xl-12">
                <div class="heading heading--primary heading--style-2 align-items-center"><span class="heading__pre-title">LOKASI</span>
                    <h2 class="heading__title"><span>Peta Ragunan</span><br /></h2>
                </div>
                <div class="map-responsive">
                    <iframe src="https://www.google.com/maps/d/embed?mid=1WxhE_KdpD1pO36C0ow-q9HCQHuDMuGJL&ehbc=2E312F" width="640" height="480"></iframe>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- lokasi end-->
<?= $this->endSection(); ?>