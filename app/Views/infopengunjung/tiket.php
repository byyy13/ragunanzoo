<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<!-- banner start-->
<section class="promo-primary" data-aos="fade-up">
    <picture>
        <source srcset="/assets/img/banner-7.jpg" media="(min-width: 992px)" /><img class="img--bg"
            src="/assets/img/banner-7.jpg" alt="img" />
    </picture>
    <div class="container">
        <div class="row">
            <div class="col-auto">
                <div class="align-container">
                    <div class="align-container__item"><span class="promo-primary__pre-title">Info Pengunjung</span>
                        <h1 class="promo-primary__title"><span></span> <span>Tiket</span></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- banner end-->
<!-- cta start-->
<div class="cta-block" data-aos="fade-down">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12">
                <p class="cta-block__text">
                    <strong>Halo Sahabat Ragunan, akses masuk ke Taman Margasatwa Ragunan sekarang sudah bisa online dan
                        offline loh! Untuk pembelian tiket online kamu bisa download aplikasi “Taman Margasatwa Ragunan”
                        melalui
                        <a style="color: #333333;" target="_blank"
                            href="https://play.google.com/store/apps/details?id=com.didik.ragunanzooapp&hl=id-ID"><i
                                class="fab fa-google-play"></i> Play Store
                        </a>
                    </strong>
                </p>
            </div>
        </div>
    </div>
</div>
<!-- cta end-->

<!-- info start-->
<section class="section pt-5 background--gray pb-5" data-aos="fade-right">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="heading heading--style-2 heading--primary heading--center"><span class="heading__pre-title">Tarif</span>
                    <h2 class="heading__title no-margin-bottom"><span>Tiket</span> <span>Masuk</span></h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-sm-6 col-md-4 col-xl-3">
                <div class="animal-item pricing-item pricing-item--style-1 text-center pricing-item--white mb-0 pb-0">
                    <div class="pricing-item__top">
                        <div class="pricing-item__icon">
                            <i class="fa-solid fa-person"></i>
                        </div>
                        <div class="pricing-item__subject">Dewasa</div>
                    </div>
                    <div class="pricing-item__lower">
                        <div class="pricing-item__price"><span>Rp</span><span>4.000</span></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-xl-3">
                <div class="animal-item pricing-item pricing-item--style-1 text-center pricing-item--white mb-0">
                    <div class="pricing-item__top">
                        <div class="pricing-item__icon">
                            <i class="fa-sharp fa-solid fa-child-reaching"></i>
                        </div>
                        <div class="pricing-item__subject">Anak-anak</div>
                    </div>
                    <div class="pricing-item__lower">
                        <div class="pricing-item__price"><span>Rp</span><span>3.000</span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="heading heading--style-2 heading--primary heading--center"><span class="heading__pre-title">Tarif</span>
                    <h2 class="heading__title no-margin-bottom"><span>Pusat Primata</span> <span>Schmutzer</span></h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-sm-6 col-md-4 col-xl-3">
                <div class="animal-item pricing-item pricing-item--style-1 text-center pricing-item--white mb-0 pb-0">
                    <div class="pricing-item__top">
                        <div class="pricing-item__icon">
                            <div class="icon-item__img"><img src="/assets/img/svg/forest-animals/monkey.svg"
                                    alt="icon" /></div>
                        </div>
                        <div class="pricing-item__subject">Hari Selasa-Jumat <br> (usia 3 tahun ke atas)</div>
                    </div>
                    <div class="pricing-item__lower">
                        <div class="pricing-item__price"><span>Rp</span><span>6.000</span></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-xl-3">
                <div class="animal-item pricing-item pricing-item--style-1 text-center pricing-item--white mb-0">
                    <div class="pricing-item__top">
                        <div class="pricing-item__icon">
                            <div class="icon-item__img"><img src="/assets/img/svg/forest-animals/monkey.svg"
                                    alt="icon" /></div>
                        </div>
                        <div class="pricing-item__subject">Hari Sabtu-Minggu / libur nasional <br> (usia 3 tahun ke
                            atas)</div>
                    </div>
                    <div class="pricing-item__lower">
                        <div class="pricing-item__price"><span>Rp</span><span>7.500</span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- info end-->

<!-- accordion start -->
<section class="section pt-5" data-aos="zoom-in">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-10">
                <div class="heading heading--style-2 heading--primary heading--center">
                    <span class="heading__pre-title">Ekstra</span>
                    <h2 class="heading__title"><span>Tarif Lainnya</span></h2>
                </div>
                <div class="accordion accordion--primary ">
                    <div class="accordion__title-block">
                        <h6 class="accordion__title">Tarif Penitipan Kendaraan</h6><span
                            class="accordion__close"></span>
                    </div>
                    <div class="accordion__text-block">
                        <ul class="unordered-list">
                            <li>Bus besar, truk besar, dan mobil box besar <b>(Rp. 15.000,-/hari)</b></li>
                            <li>Bus kecil, Truk kecil, mobil box kecil, dan pick up besar <b>(Rp. 15.000,-/hari)</b>
                            </li>
                            <li>Mobil sedan, minibus/sejenis, termasuk dalam bentuk pickup kecil <b>(Rp.
                                    15.000,-/hari)</b></li>
                            <li>Sepeda motor, dan kendaraan roda tiga <b>(Rp. 15.000,-/hari)</b></li>
                            <li>Sepeda <b>(Rp. 15.000,-/hari)</b></li>
                        </ul>
                    </div>
                </div>

                <div class="accordion accordion--primary">
                    <div class="accordion__title-block">
                        <h6 class="accordion__title">Tarif Pemakaian Fasilitas Wahana</h6><span
                            class="accordion__close"></span>
                    </div>
                    <div class="accordion__text-block">
                        <ul class="unordered-list">
                            <li>Kuda tunggang <b>Rp. 5.000,-/satu kali keliling</b></li>
                            <li>Onta tunggang <b>Rp. 7.500,-/satu kali keliling</b></li>
                            <li>Taman satwa anak <b>Rp. 2.500,-/satu kali keliling</b></li>
                            <li>Perahu Angsa <b>Rp. 18.000,-/satu kali keliling</b></li>
                            <li>Kereta keliling <b>Rp. 10.000,-/satu kali keliling</b></li>
                            <li>Sepeda tunggal <b>Rp. 10.000,-/orang</b></li>
                            <li>Sepeda ganda <b>Rp. 15.000,-/orang</b></li>
                            <li>Kuda bendi <b>Rp. 15.000,-/satu kali keliling</b></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- accordion end -->

<section class="section pt-5 background--gray" data-aos="fade-up">
    <div class="container">
        <div class="row bottom-50">
            <div class="col-12">
                <div class="heading heading--style-2 heading--primary heading--center"><span class="heading__pre-title">Ekstra</span>
                    <h2 class="heading__title no-margin-bottom"><span>Metode</span> <span>Pembayaran</span></h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-10 bottom-30">
                <div class="education-item">
                    <div class="row flex-column-reverse flex-md-row">
                        <div class="col-md-6 align-self-center">
                            <p><b>Harga Rp. 45,000</b> dengan Saldo Rp. 20,000
                                ( Rincian Harga Kartu Rp. 25,000 dan saldo di kartu Rp. 20,000 )</p>
                        </div>
                        <div class="col-md-6">
                            <div class="education-item__img"><img class="img--bg"
                                    src="/assets/img/extended/jakcard-1.png" alt="img" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-10 bottom-30">
                <div class="education-item">
                    <div class="row flex-column-reverse flex-md-row">
                        <div class="col-md-6 align-self-center">
                            <p><b>Harga Rp. 75,000</b> dengan Saldo Rp. 50,000
                                ( Rincian Harga Kartu Rp. 25,000 dan saldo di kartu Rp. 50,000 )</p>
                        </div>
                        <div class="col-md-6">
                            <div class="education-item__img"><img class="img--bg"
                                    src="/assets/img/extended/jakcard-2.png" alt="img" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-10">
                <p><strong>Informasi :</strong></p>
                <ul class="unordered-list">
                    <li>Kartu Jakcard bisa dibeli di semua loket Taman Margasatwa Ragunan</li>
                    <li>Topup Saldo minimum Rp. 10,000 ( Maksimal Saldo Kartu Rp. 1,000,000 )</li>
                    <li>Topup bisa dilakukan di semua Loket Taman Margasatwa Ragunan, Halte Busway</li>
                    <li> Kartu Jakcard bisa digunakan di Taman Margasatwa Ragunan, Monumen Nasional, Museum Kota Tua dan
                        Transjakarta.</li>
                </ul>
            </div>
        </div>
    </div>
</section>


<?= $this->endSection(); ?>