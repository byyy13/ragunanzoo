<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<!-- banner start-->
<section class="promo promo--front-5" data-aos="fade-up">
    <div class="promo-slider">
        <div class="promo-slider__item promo-slider__item--style-2">
            <picture>
                <!-- <source srcset="/assets/img/extended/homeragunan.jpg" media="(min-width: 992px)" />
                <img class="img--bg" src="/assets/img/extended/homeragunan.jpg" alt="img" /> -->
                <source srcset="/assets/img/front_5-promo-1.jpg" media="(min-width: 992px)" />
                <img class="img--bg" src="/assets/img/front_5-promo-1.jpg" alt="img" />
            </picture>
            <div class="container">
                <div class="row">
                    <div class="col-xl-10 offset-xl-1">
                        <div class="align-container">
                            <div class="align-container__item text-center">
                                <div class="promo-slider__wrapper-1">
                                    <h2 class="promo-slider__title"><span>Taman Margasatwa</span><br />
                                        <span>Ragunan</span>
                                    </h2>
                                </div>
                                <div class="promo-slider__wrapper-2">
                                    <p class="promo-slider__subtitle" style="font-size: 1.25rem;">
                                        Selamat datang di Taman Margasatwa Ragunan. Sebuah
                                        taman seluas 147 hektar dan berpenghuni lebih dari 2.009 ekor satwa</p>
                                </div>
                                <div class="promo-slider__wrapper-3 pt-3">
                                    <a class="button button--primary button--radius button--filled" href="/profil"
                                        style="font-size: 700;">Selengkapnya</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="promo-slider__item promo-slider__item--style-3">
            <picture>
                <source srcset="/assets/img/extended/homeslider3.webp" media="(min-width: 992px)" /><img class="img--bg"
                    src="/assets/img/extended/homeslider3.webp" alt="img" />
            </picture>
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 offset-xl-2">
                        <div class="align-container">
                            <div class="align-container__item">
                                <div class="promo-slider__wrapper-1">
                                    <h2 class="promo-slider__title"><span style="font-weight: normal;">Senin sebagai
                                        </span><br /><span style="font-weight: bold;">Hari
                                            libur satwa</span></h2>
                                </div>
                                <div class="promo-slider__wrapper-2">
                                    <p class="promo-slider__subtitle" style="font-size: 1.25rem;">
                                        Hari libur bagi satwa bertujuan agar satwa dapat hidup sehat, tumbuh dan
                                        berkembang secara baik
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="promo-slider__item promo-slider__item--style-2">
            <picture>
                <!-- <source srcset="/assets/img/extended/homeragunan.jpg" media="(min-width: 992px)" />
                <img class="img--bg" src="/assets/img/extended/homeragunan.jpg" alt="img" /> -->
                <source srcset="/assets/img/extended/homeragunan.jpg" media="(min-width: 992px)" />
                <img class="img--bg" src="/assets/img/extended/homeragunan.jpg" alt="img" />
            </picture>
            <div class="container">
                <div class="row">
                    <div class="col-xl-10 offset-xl-1">
                        <div class="align-container">
                            <div class="align-container__item text-center">
                                <div class="promo-slider__wrapper-1">
                                    <h2 class="promo-slider__title"><span>Informasi Pengunjung</span><br />
                                        <span>Ragunan</span>
                                    </h2>
                                </div>
                                <div class="promo-slider__wrapper-2">
                                    <p class="promo-slider__subtitle" style="font-size: 1.25rem;">
                                        Informasi seputar harga tiket masuk ragunan</p>
                                </div>
                                <div class="promo-slider__wrapper-3 pt-3">
                                    <a class="button button--primary button--radius button--filled"
                                        href="/info-pengunjung/tiket" style="font-size: 700;">Selengkapnya</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <!-- slider nav start-->
                <div class="promo-slider__nav"></div>
                <!-- slider nav end-->
            </div>
        </div>
    </div>
</section>
<!-- banner end-->

<!-- button action start-->
<section class="section no-padding-top pb-3">
    <div class="row">
        <div class="col-lg-4" data-aos="fade-up">
            <div class="cta-banner">
                <img class="img--bg" src="/assets/img/cta-banner_1.jpg" alt="img" />
                <h3 class="cta-banner__title"><span>Informasi</span> Tiket</h3>
                <a class="button button--primary button--white color--primary" href="/info-pengunjung/tiket">Lihat
                    Detail</a>
            </div>
        </div>
        <div class="col-lg-4" data-aos="fade-up">
            <div class="cta-banner">
                <img class="img--bg" src="/assets/img/cta-banner_2.jpg" alt="img" />
                <h3 class="cta-banner__title color--white"><span class="color--orange">Jadwal Makan</span> Satwa</h3>
                <a class="button button--primary button--white" href="/info-pengunjung/jadwal-makan-satwa">Lihat
                    Detail</a>
            </div>
        </div>
        <div class="col-lg-4" data-aos="fade-up">
            <div class="cta-banner">
                <img class="img--bg" src="/assets/img/cta_bg.jpg" alt="img" />
                <h3 class="cta-banner__title"><span>Petunjuk</span> Arah</h3>
                <a class="button button--primary button--white color--primary" href="/info-pengunjung/peta">Lihat
                    Detail</a>
            </div>
        </div>
    </div>
</section>
<!-- button action end-->

<!-- section start-->
<section class="section about-safari pt-5 pb-2" data-aos="fade-left">
    <img class="about-safari__left" src="/assets/img/about-safari_left.png" alt="img" /><img class="about-safari__right"
        src="/assets/img/about-safari_right.png" alt="img" />
    <div class="container">
        <div class="row bottom-70">
            <div class="col-lg-5">
                <div class="heading heading--style-2 bottom-30"><span class="heading__pre-title">Statistik</span>
                    <h2 class="heading__title no-margin-bottom"><span>Taman Margasatwa</span> <span>Ragunan</span></h2>
                </div>
            </div>
            <div class="col-lg-7 align-self-center">
                <div class="row">
                    <div class="col-md-4">
                        <div class="counter-item counter-item--front-1">
                            <div class="counter-item__top">
                                <h6 class="counter-item__title">Satwa</h6>
                            </div>
                            <div class="counter-item__lower"><span class="js-counter">2.009</span></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="counter-item counter-item--front-1">
                            <div class="counter-item__top">
                                <h6 class="counter-item__title">Luas Taman (Ha)</h6>
                            </div>
                            <div class="counter-item__lower"><span class="js-counter">147</span></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="counter-item counter-item--front-1">
                            <div class="counter-item__top">
                                <h6 class="counter-item__title">Jumlah Pengunjung</h6>
                            </div>
                            <div class="counter-item__lower"><span class="js-counter">125.000</span></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- section end-->

<!-- berita terbaru start-->
<section class="section blogs pt-2   pb-5" data-aos="zoom-in">
    <div class="container">
        <div class="row bottom-50">
            <div class="col-12">
                <div class="heading heading--style-2 heading--center">
                    <span class="heading__pre-title">Info Pengunjung</span>
                    <h2 class="heading__title no-margin-bottom"><span>Berita</span> <span>Terbaru</span></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="slider-holder blogs-slider-holder bottom-70 slider-berita">
        <div class="slider-holder__wrapper">
            <div class="blogs-slider">
                <?php foreach ($berita as $b) : ?>
                <div class="blogs-slider__item">
                    <div class="blogs-item">
                        <img class="img--bg" src="<?= $b['gambar']; ?>" alt="img" />
                        <div class="blogs-item__details">
                            <div class="blogs-item__date" style="font-weight: bold;"><?= $b['tanggal_lengkap']; ?></div>
                            <h5 class="blogs-item__title">
                                <a href="/info-pengunjung/berita-kegiatan/<?= $b['slug']; ?>">
                                    <span style="font-weight: bold;"><?= $b['judul_berita']; ?></span>
                                </a>
                            </h5>
                            <p><?= $b['deskripsi_super_singkat']; ?></p>
                            <a class="blogs-item__link" href="/info-pengunjung/berita-kegiatan/<?= $b['slug']; ?>">Baca
                                selengkapnya</a>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row align-items-center flex-column-reverse flex-sm-row">
            <div class="col-sm-6 text-center text-sm-left"><a class="button button--primary"
                    href="/info-pengunjung/berita-kegiatan">Semua Berita</a></div>
            <div class="col-sm-6 text-center text-sm-right">
                <div class="blogs-slider__dots"></div>
            </div>
        </div>
    </div>
</section>
<!-- berita terbaru end-->

<!-- section start-->
<section class="section animals-section pt-5 pb-5" data-aos="fade-left">
    <img class="animals-section__left" src="/assets/img/animals-section_left.png" alt="img" /><img
        class="animals-section__right" src="/assets/img/animals-section_right.png" alt="img" />
    <div class="container">
        <div class="row bottom-50">
            <div class="col-12">
                <div class="heading heading--center"><span class="heading__pre-title">Satwa</span>
                    <h2 class="heading__title no-margin-bottom"><span>Satwa</span> <span>yang Ada</span></h2>
                </div>
            </div>
        </div>
        <div class="row offset-30">
            <div class="col-6 col-sm-4 col-md-3 col-xl-2 bottom-30">
                <div class="amimal text-center">
                    <div class="animal__img"><img src="/assets/img/tigers.png" alt="img" /></div>
                    <h6 class="animal__title">Harimau</h6>
                </div>
            </div>
            <div class="col-6 col-sm-4 col-md-3 col-xl-2 bottom-30">
                <div class="amimal text-center">
                    <div class="animal__img"><img src="/assets/img/girafes.png" alt="img" /></div>
                    <h6 class="animal__title">Jerapah</h6>
                </div>
            </div>
            <div class="col-6 col-sm-4 col-md-3 col-xl-2 bottom-30">
                <div class="amimal text-center">
                    <div class="animal__img"><img src="/assets/img/orangutans.png" alt="img" /></div>
                    <h6 class="animal__title">Orangutan</h6>
                </div>
            </div>
            <div class="col-6 col-sm-4 col-md-3 col-xl-2 bottom-30">
                <div class="amimal text-center">
                    <div class="animal__img"><img src="/assets/img/zebras.png" alt="img" /></div>
                    <h6 class="animal__title">Zebra</h6>
                </div>
            </div>
            <div class="col-6 col-sm-4 col-md-3 col-xl-2 bottom-30">
                <div class="amimal text-center">
                    <div class="animal__img"><img src="/assets/img/elephants.png" alt="img" /></div>
                    <h6 class="animal__title">Gajah</h6>
                </div>
            </div>
            <div class="col-6 col-sm-4 col-md-3 col-xl-2 bottom-30">
                <div class="amimal text-center">
                    <div class="animal__img"><img src="/assets/img/lions.png" alt="img" /></div>
                    <h6 class="animal__title">Singa</h6>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- section end-->

<!-- section start-->
<section class="section pt-5" data-aos="fade-right">
    <div class="container">
        <div class="row bottom-50 align-items-end">
            <div class="col-md-8">
                <div class="heading heading--style-2"><span class="heading__pre-title">Gallery Satwa</span>
                    <h2 class="heading__title no-margin-bottom"><span style="font-weight: normal;">Kenal lebih dekat
                            dengan </span> <span style="font-weight: bold;">satwa
                        </span>
                    </h2>
                </div>
            </div>
            <div class="col-md-4">
                <div class="destination-slider__dots text-left text-md-right"></div>
            </div>
        </div>
    </div>
    <div class="slider-holder">
        <div class="slider-holder__wrapper">
            <div class="destination-slider">
                <?php foreach ($mammalia as $m) : ?>
                <div class="destination-slider__item">
                    <div class="destination-item"><img class="img--bg" src="<?= $m['gambar']; ?>" alt="img" />
                        <!-- <div class="destination-item__count"><span></span></div> -->
                        <div class="destination-item__details">
                            <h6 class="destination-item__title" style="font-weight: 1000; font-size: 1.25rem;"><a
                                    href="/satwa-ragunan/fauna/mamalia/<?= $m['nama-slug']; ?>"><?= $m['nama']; ?></a>
                            </h6><a class="destination-item__link"
                                href="/satwa-ragunan/fauna/mamalia/<?= $m['nama-slug']; ?>">Selengkapnya</a>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
                <?php foreach ($reptilia as $m) : ?>
                <div class="destination-slider__item">
                    <div class="destination-item"><img class="img--bg" src="<?= $m['gambar']; ?>" alt="img" />
                        <!-- <div class="destination-item__count"><span></span></div> -->
                        <div class="destination-item__details">
                            <h6 class="destination-item__title" style="font-weight: 1000; font-size: 1.25rem;"><a
                                    href="/satwa-ragunan/fauna/reptil/<?= $m['nama-slug']; ?>"><?= $m['nama']; ?></a>
                            </h6><a class="destination-item__link"
                                href="/satwa-ragunan/fauna/reptil/<?= $m['nama-slug']; ?>">Selengkapnya</a>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
                <?php foreach ($aves as $m) : ?>
                <div class="destination-slider__item">
                    <div class="destination-item"><img class="img--bg" src="<?= $m['gambar']; ?>" alt="img" />
                        <!-- <div class="destination-item__count"><span></span></div> -->
                        <div class="destination-item__details">
                            <h6 class="destination-item__title" style="font-weight: 1000; font-size: 1.25rem;"><a
                                    href="/satwa-ragunan/fauna/aves/<?= $m['nama-slug']; ?>"><?= $m['nama']; ?></a>
                            </h6><a class="destination-item__link"
                                href="/satwa-ragunan/fauna/aves/<?= $m['nama-slug']; ?>">Selengkapnya</a>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
                <?php foreach ($pisces as $m) : ?>
                <div class="destination-slider__item">
                    <div class="destination-item"><img class="img--bg" src="<?= $m['gambar']; ?>" alt="img" />
                        <!-- <div class="destination-item__count"><span></span></div> -->
                        <div class="destination-item__details">
                            <h6 class="destination-item__title" style="font-weight: 1000; font-size: 1.25rem;"><a
                                    href="/satwa-ragunan/fauna/pisces/<?= $m['nama-slug']; ?>"><?= $m['nama']; ?></a>
                            </h6><a class="destination-item__link"
                                href="/satwa-ragunan/fauna/pisces/<?= $m['nama-slug']; ?>">Selengkapnya</a>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>
<!-- section end-->

<div class="cta-block" data-aos="fade-down">
    <img class="img--bg" src="/assets/img/cta-bg_2.png" alt="bg" />
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <h4 class="cta-block__title">Newsletters <span>Kami</span></h4>
                <p class="cta-block__text">Subscribe dan dapatkan informasi terbaru</p>
            </div>
            <div class="col-lg-6 offset-30">
                <form class="form cta-form bottom-30" action="javascript:void(0);">
                    <input class="form__field" type="email" placeholder="Email Anda" />
                    <input class="form__submit" type="submit" value="Subscribe" />
                </form>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection(); ?>