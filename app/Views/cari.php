<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>

<section class="promo-primary" data-aos="fade-up">
    <picture>
        <source srcset="/assets/img/event-details.jpg" media="(min-width: 992px)" /><img class="img--bg" src="/assets/img/event-details.jpg" alt="img" />
    </picture>
    <div class="container">
        <div class="row">
            <div class="col-auto">
                <div class="align-container">
                    <div class="align-container__item"><span class="promo-primary__pre-title">Info Pengunjung</span>
                        <h1 class="promo-primary__title"><span style="font-weight: bold;">Hasil Pencarian</span>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section pt-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="heading heading--primary heading--center"><span class="heading__pre-title">Pencarian</span>
                    <h2 class="heading__title"><span>Hasil Pencarian untuk "<?= $keyword; ?>"</span></h2>
                    <p class="no-margin-bottom">
                        <b>Disclaimer</b>: Hasil pencarian yang ditampilkan adalah <b>hasil dummy</b>
                    </p>
                </div>
            </div>
            <div class="col-lg-12 mt-5">
                <div class="row">
                    <?php for ($i = 0; $i < 3; $i++) { ?>
                        <div class="col-12 col-sm-6 col-md-4">
                            <div class="shop-item">
                                <div class="shop-item__img">
                                    <span class="shop-item__badge shop-item__badge--hot">Berita & Kegiatan</span>
                                    <a class="shop-item__add" href="/info-pengunjung/berita-kegiatan/<?= $berita[$i]['slug']; ?>">
                                        <span> Lihat Selengkapnya</span>
                                    </a>
                                    <img class="img--contain" src="<?= $berita[$i]['gambar']; ?>" alt="img" />
                                </div>
                                <div class="shop-item__details">
                                    <h6 class="shop-item__name">
                                        <a href="/info-pengunjung/berita-kegiatan/<?= $berita[$i]['slug']; ?>">
                                            <?= $berita[$i]['judul_berita']; ?>
                                        </a>
                                    </h6>
                                    <div class="shop-item__details-lower">
                                        <span class="shop-item__price">
                                            <?= substr($berita[$i]['deskripsi_singkat'], 0, 100) . '....';; ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                    <?php for ($i = 0; $i < 3; $i++) { ?>
                        <div class="col-12 col-sm-6 col-md-4">
                            <div class="shop-item">
                                <div class="shop-item__img">
                                    <span class="shop-item__badge shop-item__badge--hot">Flora</span>
                                    <a class="shop-item__add" href="/satwa-ragunan/flora/<?= $flora[$i]['nama-slug']; ?>">
                                        <span> Lihat Selengkapnya</span>
                                    </a>
                                    <img class="img--contain" src="<?= $flora[$i]['gambar']; ?>" alt="img" />
                                </div>
                                <div class="shop-item__details">
                                    <h6 class="shop-item__name">
                                        <a href="/satwa-ragunan/flora/<?= $flora[$i]['nama-slug']; ?>">
                                            <?= $flora[$i]['nama']; ?>
                                        </a>
                                    </h6>
                                    <div class="shop-item__details-lower">
                                        <span class="shop-item__price">
                                            <?= substr($flora[$i]['deskripsi'], 0, 100) . '....';; ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="row">
                    <div class="col-12">
                        <ul class="pagination pagination--rounded justify-content-center">
                            <li class="pagination__item pagination__item--prev">
                                <i class="fa fa-angle-left" aria-hidden="true"></i>
                            </li>
                            <li class="pagination__item"><span>1</span></li>
                            <li class="pagination__item pagination__item--active"><span>2</span></li>
                            <li class="pagination__item pagination__item--next">
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?= $this->endSection(); ?>