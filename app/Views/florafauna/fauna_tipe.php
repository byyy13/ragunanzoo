<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>

<section class="promo-primary" data-aos="fade-up">
    <picture>
        <source srcset="<?= $promo_img; ?>" media="(min-width: 992px)" /><img class="img--bg" src="<?= $promo_img; ?>" alt="img" />
    </picture>
    <div class="container">
        <div class="row">
            <div class="col-auto">
                <div class="align-container">
                    <div class="align-container__item">
                        <span class="promo-primary__pre-title">Fauna</span>
                        <h1 class="promo-primary__title"><span style="font-weight: bold;"><?= $tipe; ?></span></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section animals" data-aos="zoom-in">
    <div class="container">
        <div class="row bottom-70">
            <div class="col-xl-4">
                <div class="heading heading--style-2 bottom-lg-none"><span class="heading__pre-title">Satwa
                        <?= $tipe; ?></span>
                    <h2 class="heading__title no-margin-bottom"><span>Awesome <?= $tipe; ?></span><br /> <span>in
                            our zoo</span></h2>
                </div>
            </div>
            <div class="col-xl-8">
                <p class="no-margin-bottom">
                    <?php if ($tipe == 'mamalia') { ?>
                        Mamalia adalah hewan bertulang belakang dari kelas Mammalia. Mamalia dicirikan dengan adanya kelenjar susu yang memproduksi susu untuk memberi makan anak-anak mereka, daerah neokorteks otak, bulu atau rambut, dan tiga tulang telinga tengah.
                    <?php } else if ($tipe == 'reptil') { ?>
                        Reptil adalah hewan-hewan yang termasuk dalam kelas Reptilia atau sebuah pengelompokan paraphyletic yang terdiri dari semua sauropsida kecuali burung. Reptil yang masih hidup terdiri dari kura-kura, buaya, squamata, dan rhynchocephalian. Saat ini, jumlah hewan reptil mencakup sekitar 12.000 spesies.
                    <?php } else if ($tipe == 'aves') { ?>
                        Aves atau burung adalah sekelompok vertebrata berdarah panas yang dicirikan dengan bulu, rahang berparuh ompong, bertelur dengan cangkang keras, tingkat metabolisme yang tinggi, jantung dengan empat bilik, dan kerangka yang kuat namun ringan.
                    <?php } else if ($tipe == 'pisces') { ?>
                        Pisces atau ikan adalah hewan vertebrata akuatik poikiloterm (berdarah dingin) yang bernapas dengan insang. Secara sistematik, ikan atau pisces merupakan kelas yang berada di bawah filum Chordata. Ciri-ciri dari kelas ini adalah bertulang belakang, bertulang rawan, mempunyai sirip tunggal atau berpasangan, memiliki operkulum (tutup insang), bersisik dan berlendir, dan ukuran yang bervariasi.
                    <?php } ?>
           
                </p>
            </div>
        </div>
        <div class="row no-gutters left-3 right-3 justify-content-center">
            <?php foreach ($hewan as $h) : ?>
                <div class="category_1 col-sm-6 col-lg-4 col-xl-3 gallery-masonry__item">
                    <a class="animal-block gallery-masonry__img gallery-masonry__item--height-1" href="/satwa-ragunan/fauna/<?= $tipe; ?>/<?= trim($h['nama-slug']); ?>">
                        <img class="img--bg" src="<?= $h['gambar']; ?>" alt="img" />
                        <div class="gallery-masonry__description">
                            <span style="font-weight: bold;"><b><?= $h['kelas']; ?></b>
                            </span><span><?= $h['nama']; ?></span>Selengkapnya</span>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>

        <div class="row pt-4 d-flex justify-content-center">
            <div class="col-12">
                <ul class="pagination pagination--rounded justify-content-center">
                    <!-- PREV PAJANGAN -->
                    <li class="pagination__item pagination__item--prev">
                        <i class="fa fa-angle-left" aria-hidden="true"></i><span></span>
                    </li>
                    <?php for ($i = 1; $i <= $totalPages; $i++) : ?>
                        <?php if ($i == $currentPage) : ?>
                            <li class="pagination__item pagination__item--active"><span><?= $i ?></span></li>
                        <?php else : ?>
                            <a href="<?= site_url('satwa-ragunan/fauna/' . $tipe . '?page=' . $i) ?>">
                                <li class="pagination__item "><span><?= $i ?></span></li>
                            </a>
                        <?php endif; ?>
                    <?php endfor; ?>
                    <!-- NEXT PAJANGAN -->
                    <li class="pagination__item pagination__item--next">
                        <i class="fa fa-angle-right" aria-hidden="true"></i><span></span>
                    </li>
                </ul>
            </div>
        </div>
</section>

<?= $this->endSection(); ?>