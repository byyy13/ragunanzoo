<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>
<!-- promo start-->
<section class="promo-primary" data-aos="fade-up">
    <picture>
        <source srcset="<?= $hewan['gambar_background']; ?>" media="(min-width: 992px)" /><img class="img--bg"
            src="<?= $hewan['gambar_background']; ?>" alt="img" />
    </picture>
    <div class="container">
        <div class="row">
            <div class="col-auto">
                <div class="align-container">
                    <div class="align-container__item"><span
                            class="promo-primary__pre-title"><?= strtoupper($tipe); ?></span>
                        <h1 class="promo-primary__title"><span style="font-weight: bold;"><?= $hewan['nama']; ?></span>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="promo-cta pr-0 mr-0"><img class="img--bg" style="opacity: 0.55;"
            src="<?= $hewan['gambar_background']; ?>" alt="img" />
        <h4 class="promo-cta__title"> <span> Tonton Video</span></h4>
        <a class="video-trigger" href="<?= $hewan['link_youtube']; ?>"><img class="img--bg"
                src="<?= $hewan['gambar_background']; ?>" style="opacity: 0.55;" alt="image" />
            <span class="video-trigger__icon">
                <span style="font-weight: bold; color: #fff;">Di sini! </span>
                </i>
            </span>
        </a>
    </div>
</section>
<!-- promo end-->
<!-- section start-->
<section class="section py-5" data-aos="fade-down">
    <img class="d-block section__bg height100 width100 t0" src="/assets/img/section-bg.png" alt="img" />
    <div class="container">
        <div class="row bottom-30">
            <div class="col-12">
                <div class="heading heading--primary">
                    <h2 class="heading__title no-margin-bottom"><span>INFO</span></h2>
                </div>
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-12">
                <blockquote class="blockquote">
                    <p class="blockquote__text">
                        <b>Kelas:</b> <?= $hewan['kelas']; ?>; <b>Orde:</b> <?= $hewan['orde']; ?>;
                        <b>Famili:</b> <?= $hewan['famili']; ?>; <b>Hewan:</b> <?= $hewan['spesies']; ?>;
                        <b>Genus:</b> <?= $hewan['genus']; ?>; <br>
                        <b>Perkembangbiakan:</b> <?= $hewan['perkembangbiakan']; ?>
                    </p>
                </blockquote>
            </div>
        </div>
    </div>
</section>
<section class="section animal-details p-4 " data-aos="fade-left">
    <div class="container">
        <div class="row mt-4">
            <div class="col-lg-12 col-xl-12 ">
                <div class="row offset-30">
                    <div class="col-12 col-sm-6 col-md-4 bottom-30">
                        <div class="animal-details__icon">
                            <div class="icon-item__img">
                                <img src="<?= $hewan['icon']; ?>" alt="icon" />
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 bottom-30">
                        <h6 class="animal-details__title">Makanan</h6>
                        <p><?= $hewan['makanan']; ?></p>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 bottom-30">
                        <h6 class="animal-details__title">Penyebaran</h6>
                        <p><?= $hewan['penyebaran']; ?></p>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 bottom-30">
                        <h6 class="animal-details__title">Perkembangan</h6>
                        <p><?= $hewan['perkembangbiakan']; ?></p>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 bottom-30">
                        <h6 class="animal-details__title">Habitat</h6>
                        <p><?= $hewan['habitat']; ?></p>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 bottom-30">
                        <h6 class="animal-details__title">Masa Hidup</h6>
                        <p><?= $hewan['masa_hidup']; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section py-5" data-aos="fade-right">
    <img class="d-block section__bg height100 width100 t0" src="/assets/img/section-bg.png" alt="img" />
    <div class="container">
        <div class="row bottom-30">
            <div class="col-12">
                <div class="heading heading--primary">
                    <h2 class="heading__title no-margin-bottom"><span>DESKRIPSI</span></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <p class="blockquote__text"><?= $hewan['deskripsi']; ?></p>
            </div>
        </div>
    </div>
</section>
<!-- section end-->
<?= $this->endSection(); ?>