<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>

<section class="promo-primary" data-aos="fade-up">
    <picture>
        <source srcset="<?= $tumbuhan['gambar_background']; ?>" media="(min-width: 992px)" /><img class="img--bg"
            src="<?= $tumbuhan['gambar_background']; ?>" alt="img" />
    </picture>
    <div class="container">
        <div class="row">
            <div class="col-auto">
                <div class="align-container">
                    <div class="align-container__item"><span class="promo-primary__pre-title">Flora</span>
                        <h1 class="promo-primary__title"><span
                                style="font-weight: bold;"><?= $tumbuhan['nama']; ?></span>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section animal-details p-4 " data-aos="fade-left">
    <div class="container">
        <div class="row mt-4">
            <div class="col-lg-12 col-xl-12 ">
                <div class="row offset-30">
                    <div class="col-12 col-sm-6 col-md-4 bottom-30">
                        <h6 class="animal-details__title">Nama</h6>
                        <p><?= $tumbuhan['nama']; ?>(<i><?= $tumbuhan['nama_ilmiah']; ?></i>)</p>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 bottom-30">
                        <h6 class="animal-details__title">Kelas</h6>
                        <p><?= $tumbuhan['kelas']; ?></p>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 bottom-30">
                        <h6 class="animal-details__title">Orde</h6>
                        <p><?= $tumbuhan['orde']; ?></p>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 bottom-30">
                        <h6 class="animal-details__title">Famili</h6>
                        <p><?= $tumbuhan['famili']; ?></p>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 bottom-30">
                        <h6 class="animal-details__title">Species</h6>
                        <p><?= $tumbuhan['spesies']; ?></p>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 bottom-30">
                        <h6 class="animal-details__title">Genus</h6>
                        <p><?= $tumbuhan['genus']; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section py-5" data-aos="fade-right">
    <img class="d-block section__bg height100 width100 t0" src="/assets/img/section-bg.png" alt="img" />
    <div class="container">
        <div class="row bottom-30">
            <div class="col-12">
                <div class="heading heading--primary">
                    <h2 class="heading__title no-margin-bottom"><span>DESKRIPSI</span></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <p class="blockquote__text"><?= $tumbuhan['deskripsi']; ?></p>
            </div>
        </div>
    </div>
</section>
<?= $this->endSection(); ?>