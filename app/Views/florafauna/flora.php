<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>

<section class="promo-primary" data-aos="fade-up">
    <picture>
        <source srcset="/assets/img/extended/anggrek_bg.jpg" media="(min-width: 992px)" /><img class="img--bg"
            src="/assets/img/extended/anggrek_bg.jpg" alt="img" />
    </picture>
    <div class="container">
        <div class="row">
            <div class="col-auto">
                <div class="align-container">
                    <div class="align-container__item">
                        <span class="promo-primary__pre-title">Flora</span>
                        <h1 class="promo-primary__title"><span style="font-weight: bold;">Seluruh Flora</span></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section animals" data-aos="zoom-in">
    <div class="container">
        <div class="row bottom-70">
            <div class="col-xl-4">
                <div class="heading heading--style-2 bottom-lg-none"><span class="heading__pre-title">Flora</span>
                    <h2 class="heading__title no-margin-bottom"><span>Awesome Flora</span><br />
                        <span>in our zoo</span>
                    </h2>
                </div>
            </div>
            <div class="col-xl-8">
                <p class="no-margin-bottom">Flora merujuk kepada keseluruhan tumbuhan yang ada dalam satu kawasan
                    geografi atau ekosistem tertentu. Ini termasuk semua jenis tumbuhan mulai dari tumbuhan pohon besar,
                    semak, rerumputan, lumut, dan ganggang yang tumbuh di suatu wilayah atau lingkungan tertentu.
                </p>
            </div>
        </div>
        <div class="row no-gutters left-3 right-3">
            <?php foreach ($tumbuhan as $t) : ?>
            <div class="category_1 col-sm-6 col-lg-4 col-xl-3 gallery-masonry__item">
                <a class="animal-block gallery-masonry__img gallery-masonry__item--height-1"
                    href="/satwa-ragunan/flora/<?= trim($t['nama-slug']); ?>">
                    <img class="img--bg" src="<?= $t['gambar']; ?>" alt="img" />
                    <div class="gallery-masonry__description">
                        <span style="font-weight: bold;">
                            <b><?= $t['nama_ilmiah']; ?></b></span><span><?= $t['nama']; ?></span><span>Selengkapnya</span>
                    </div>
                </a>
            </div>
            <?php endforeach; ?>
        </div>

        <div class="row pt-4 d-flex justify-content-center">
            <div class="col-12">
                <ul class="pagination pagination--rounded justify-content-center">
                    <!-- PREV PAJANGAN -->
                    <li class="pagination__item pagination__item--prev">
                        <i class="fa fa-angle-left" aria-hidden="true"></i><span></span>
                    </li>
                    <?php for ($i = 1; $i <= $totalPages; $i++) : ?>
                    <?php if ($i == $currentPage) : ?>
                    <li class="pagination__item pagination__item--active"><span><?= $i ?></span></li>
                    <?php else : ?>
                    <a href="<?= site_url('satwa-ragunan/flora?page=' . $i) ?>">
                        <li class="pagination__item "><span><?= $i ?></span></li>
                    </a>
                    <?php endif; ?>
                    <?php endfor; ?>
                    <!-- NEXT PAJANGAN -->
                    <li class="pagination__item pagination__item--next">
                        <i class="fa fa-angle-right" aria-hidden="true"></i><span></span>
                    </li>
                </ul>
            </div>
        </div>
</section>

<?= $this->endSection(); ?>