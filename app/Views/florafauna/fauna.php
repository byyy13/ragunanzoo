<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>
<!-- promo start-->
<section class="promo-primary" data-aos="fade-up">
    <picture>
        <source srcset="/assets/img/animals.jpg" media="(min-width: 992px)" /><img class="img--bg"
            src="/assets/img/animals.jpg" alt="img" />
    </picture>
    <div class="container">
        <div class="row">
            <div class="col-auto">
                <div class="align-container">
                    <div class="align-container__item">
                        <span class="promo-primary__pre-title">Fauna</span>
                        <h1 class="promo-primary__title"><span>SELURUH</span> <span>FAUNA</span></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- promo end-->

<section class="section" data-aos="zoom-in">
    <div class="container">
        <div class="row bottom-50">
            <div class="col-12">
                <div class="heading heading--center"><span class="heading__pre-title">lihat kategori</span>
                    <h2 class="heading__title no-margin-bottom"><span>kategori fauna</span>
                    </h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-lg-3 bottom-30">
                <a href="/satwa-ragunan/fauna/mamalia">
                    <div class="tours-item">
                        <div class="tours-item__img"><img class="img--bg" src="/assets/img/extended/mamalia3.jpg"
                                alt="img" />
                        </div>
                        <div class="tours-item__details">
                            <h4 class="tours-item__title">Mamalia<br /></h4>
                        <a class="tours-item__link"
                                href="/satwa-ragunan/fauna/mamalia">Selengkapnya</a>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-3 bottom-30">
                <a href="/satwa-ragunan/fauna/reptil">
                    <div class="tours-item">
                        <div class="tours-item__img"><img class="img--bg" src="/assets/img/extended/reptil4.jpg"
                                alt="img" />
                        </div>
                        <div class="tours-item__details">
                            <h4 class="tours-item__title">Reptil<br /> </h4>
                         <a class="tours-item__link"
                                href="/satwa-ragunan/fauna/reptil">Selengkapnya</a>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-3 bottom-30">
                <a href="/satwa-ragunan/fauna/aves">
                    <div class="tours-item">
                        <div class="tours-item__img"><img class="img--bg" src="/assets/img/extended/aves3.jpg"
                                alt="img" />
                        </div>
                        <div class="tours-item__details">
                            <h4 class="tours-item__title">Aves<br /> </h4>
                            <div class="tours-item__edition"></div><a class="tours-item__link"
                                href="/satwa-ragunan/fauna/aves">Selengkapnya</a>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-3 bottom-30">
                <a href="/satwa-ragunan/fauna/pisces">
                    <div class="tours-item">
                        <div class="tours-item__img"><img class="img--bg" src="/assets/img/extended/pisces4.jpg"
                                alt="img" />
                        </div>
                        <div class="tours-item__details">
                            <h4 class="tours-item__title">Pisces<br /> </h4>
                          <a class="tours-item__link"
                                href="/satwa-ragunan/fauna/pisces">Selengkapnya</a>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
<?= $this->endSection(); ?>