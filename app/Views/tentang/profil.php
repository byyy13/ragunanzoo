<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<!-- banner start-->
<section class="promo-primary" data-aos="fade-up">
    <picture>
        <source srcset="/assets/img/extended/layanan.jpg" media="(min-width: 992px)" /><img class="img--bg"
            src="/assets/img/extended/layanan.jpg" alt="img" />
    </picture>
    <div class="container">
        <div class="row">
            <div class="col-auto">
                <div class="align-container">
                    <div class="align-container__item"><span class="promo-primary__pre-title">Tentang</span>
                        <h1 class="promo-primary__title"><span></span> <span>Profil</span></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- banner end-->

<!-- info start-->
<section class="section" data-aos="fade-right">
    <img class="section__bg t50 r0" src="/assets/img/about_bg.png" alt="img" />
    <div class="container">
        <div class="row flex-column-reverse flex-lg-row">
            <div class="col-lg-6 col-xl-5">
                <div class="img-box">
                    <div class="img-box__img"><img class="img--bg" src="/assets/img/img-box.jpg" alt="imgez" /></div>
                </div>
            </div>
            <div class="col-lg-6 col-xl-6 offset-xl-1">
                <div class="heading heading--primary heading--style-2"><span class="heading__pre-title">Tentang</span>
                    <h2 class="heading__title"><span>Tentang</span><br /> <span>
                            <h4>Taman Margasatwa Ragunan</h4>
                        </span></h2>
                </div>
                <p>Selamat datang di Taman Margasatwa Ragunan. Sebuah taman seluas 147 hektar dan berpenghuni lebih dari
                    2.000 ekor satwa serta ditumbuhi lebih dari 50.000 pohon membuat suasana lingkungannya sejuk dan
                    nyaman. Lahannnya tertata dan terbangun serta sebagian lagi masih dikembangkan menuju suatu kebun
                    binatang yang modern sebagai identitas kota Jakarta.</p>
                <p>Berkunjung ke Taman Margasatwa Ragunan berarti memasuki sebuah hutan tropis mini, di dalamnya
                    terdapat keanekaragaman hayati yang memiliki nilai konservasi tinggi dan menyimpan harapan untuk
                    masa depan.</p>
                <p>Sebuah kebun binatang modern menampilkan suatu system ekologi yang lengkap yang bias menjadi satu
                    sumber ilmu pengetahuan yang akan mengawali langkah pelestarian kehidupan alam liar. Singkatnya,
                    kebuna binatang adalah “Kapal Nuh” kita dalam menghadapi bencana dan kerusakan alam yang akhir-akhir
                    ini sering terjadi. Bila nanti sudah tidak ada lagi hutan di bumi ini, paling tidak masih ada
                    contoh-contoh makhluk yang menakjubkan ini di kebun binatang, entah itu telah berwujud satwa ataupun
                    masih berbentuk embrio, sel atau DNA.</p>
            </div>

        </div>
    </div>
</section>
<!-- info end-->

<!-- visi misi start-->
<section class="section no-padding-top" data-aos="fade-left">
    <div class="container">
        <div class="row flex-column-reverse flex-lg-row">
            <div class="col-lg-12 col-xl-12">
                <div class="heading heading--primary heading--style-2"><span class="heading__pre-title">VISI &
                        MISI</span>
                    <h2 class="heading__title"><span>VISI & MISI</span><br /></h2>
                </div>
                <h6>Visi</h6>
                <p>Menjadikan Taman Margasatwa Ragunan seperti Kebun Binatang di Negara Maju yang dihuni oleh
                    satwa-satwa yang sejahtera.</p>
                <h6>Misi</h6>
                <ol class="ordered-list">
                    <li>Meningkatkan kualitas kesejahteraan satwa.</li>
                    <li>Meningkatkan kualitas pendidikan lingkungan.</li>
                    <li>Meningkatkan hubungan ilmiah dengan universitas, instansiterkait, dan lembaga konservasi.</li>
                    <li>Meningkatkan profesionalisme sumber daya manusia.</li>
                    <li>Meningkatkan hubungan antar Lembaga Konservasi di dalam dan luar negeri melalui program tukar
                        menukar satwa.</li>
                    <li>Meningkatkan kualitas pelayanan pengunjung.</li>
                    <li>Meningkatkan cinta satwa kepada masyarakat</li>
                </ol>
                <!-- <p> 1. Meningkatkan kualitas kesejahteraan satwa. <br>
                    2. Meningkatkan kualitas pendidikan lingkungan. <br>
                    3. Meningkatkan hubungan ilmiah dengan universitas, instansiterkait, dan lembaga konservasi. <br>
                    4. Meningkatkan profesionalisme sumber daya manusia. <br>
                    5. Meningkatkan hubungan antar Lembaga Konservasi di dalam dan luar negeri melalui program tukar
                    menukar satwa. <br>
                    6. Meningkatkan kualitas pelayanan pengunjung. <br>
                    7. Meningkatkan cinta satwa kepada masyarakat.</p> -->
                <!-- <ol class="ordered-list">
                    <li>Streamer fish California halibut </li>
                    <li>Pacif Slickhead grunion lake trout.</li>
                    <li>Canthigaster rostrata spikefish</li>
                    <li>Brown trout loach summer flounder</li>
                    <li>European minnow black dragonfish</li>
                    <li>Orbicular batfish stingray</li>
                </ol> -->
            </div>
        </div>
    </div>
</section>
<!-- visi misi end-->

<!-- timeline start -->
<section class="section no-padding-top">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="heading heading--primary heading--center" data-aos="zoom-in">
                    <span class="heading__pre-title">Timeline</span>
                    <h2 class="heading__title"><span>Sejarah Singkat</span></h2>
                </div>

                <section class="timeline_area section_padding_130">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <!-- Timeline Area-->
                                <div class="apland-timeline-area">
                                    <!-- Single Timeline Content-->
                                    <div class="single-timeline-area">
                                        <div class="timeline-date" data-aos="fade-left">
                                            <p>1864</p>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 col-md-6 col-lg-4">
                                                <div class="single-timeline-content d-flex" data-aos="fade-left">
                                                    <div class="timeline-text">
                                                        <p>Taman Margasatwa Ragunan didirikan pada tanggal 19 September
                                                            tahun 1864 di Batavia ( kini Jakarta ) dengan nama “Planten
                                                            en Dierentuin” ini pertama kali di kelola oleh perhimpunan
                                                            penyayang Flora dan Fauna Batavia (Culture Vereniging
                                                            Planten en Dierentuin at Batavia ).</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-4">
                                                <div class="single-timeline-content d-flex" data-aos="fade-left">
                                                    <div class="timeline-text">
                                                        <p>Taman ini berdiri di atas lahan seluas 10 hektar di Jalan
                                                            Cikini Raya No 73 yang di hibahkan oleh Raden Saleh, pelukis
                                                            ternama di Indonesia.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Single Timeline Content-->
                                    <div class="single-timeline-area">
                                        <div class="timeline-date" data-aos="fade-left">
                                            <p>1949</p>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 col-md-6 col-lg-4">
                                                <div class="single-timeline-content d-flex" data-aos="fade-left">
                                                    <div class="timeline-text">
                                                        <p>Setelah Indonesia Merdeka, pada tahun 1949 namanya di ubah
                                                            menjadi Kebun Binatang Cikini.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-4">
                                                <div class="single-timeline-content d-flex" data-aos="fade-left">
                                                    <div class="timeline-text">
                                                        <p>Dengan perkembangan Jakarta, Cikini menjadi tidak cocok lagi
                                                            untuk peragaan satwa. Pada tahun 1964. Pada masa Gubernur
                                                            DCI Jakarta Dr. Soemarno dibentuk Badan Persiapan
                                                            Pelaksanaan Pembangunan Kebun Binatang untuk memindahkan
                                                            dari Jl. Cikini Raya no 73 Ke Pasar Minggu Jakarta Selatan
                                                            yang diketuai oleh Drh. T.H.E.W. Umboh.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-4">
                                                <div class="single-timeline-content d-flex" data-aos="fade-left">
                                                    <div class="timeline-text">
                                                        <p>Pemerintah DKI Jakarta menghibahkan lahan seluas 30 ha di
                                                            Ragunan , Pasar Minggu. Jaraknya kira-kira 20 Km dari pusat
                                                            kota. Kepindahan dari Kebun Binatang Cikini ke Ragunan
                                                            membawa lebih dari 450 ekor satwa yang merupakan sisa
                                                            koleksi terakhir dari Kebun Binatang Cikini.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Single Timeline Content-->
                                    <div class="single-timeline-area">
                                        <div class="timeline-date" data-aos="fade-left">
                                            <p>1966</p>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 col-md-6 col-lg-4">
                                                <div class="single-timeline-content d-flex" data-aos="fade-left">
                                                    <div class="timeline-text">
                                                        <p>Pada tahun 1974 Taman Margasatwa Ragunan dipimpin oleh
                                                            Benjamin Galstaun direktur pertama waktu itu.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Single Timeline Content-->
                                    <div class="single-timeline-area">
                                        <div class="timeline-date" data-aos="fade-left">
                                            <p>1974</p>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 col-md-6 col-lg-4">
                                                <div class="single-timeline-content d-flex" data-aos="fade-left">
                                                    <div class="timeline-text">
                                                        <p>Kebun Binatang Ragunan dibuka secara resmi pada 22 Juni 1966
                                                            oleh Gubernur DKI ( Daerah Khusus Ibukota ) Jakarta Mayor
                                                            Jenderal Ali Sadikin dengan nama Taman Margasatwa Ragunan.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Single Timeline Content-->
                                    <div class="single-timeline-area">
                                        <div class="timeline-date" data-aos="fade-left">
                                            <p>1974</p>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 col-md-6 col-lg-4">
                                                <div class="single-timeline-content d-flex" data-aos="fade-left">
                                                    <div class="timeline-text">
                                                        <p>Kebun Binatang Ragunan dibuka secara resmi pada 22 Juni 1966
                                                            oleh Gubernur DKI ( Daerah Khusus Ibukota ) Jakarta Mayor
                                                            Jenderal Ali Sadikin dengan nama Taman Margasatwa Ragunan.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Single Timeline Content-->
                                    <div class="single-timeline-area">
                                        <div class="timeline-date" data-aos="fade-left">
                                            <p>1983</p>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 col-md-6 col-lg-4">
                                                <div class="single-timeline-content d-flex" data-aos="fade-left">
                                                    <div class="timeline-text">
                                                        <p>Pada tahun 1983 berubah namanya menjadi Badan Pengelola Kebun
                                                            Binatang Ragunan</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Single Timeline Content-->
                                    <div class="single-timeline-area">
                                        <div class="timeline-date" data-aos="fade-left">
                                            <p>2001</p>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 col-md-6 col-lg-4">
                                                <div class="single-timeline-content d-flex" data-aos="fade-left">
                                                    <div class="timeline-text">
                                                        <p>
                                                            Pada tahun 2001 berubah lagi menjadi Kantor Taman Margasatwa
                                                            Ragunan</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Single Timeline Content-->
                                    <div class="single-timeline-area">
                                        <div class="timeline-date" data-aos="fade-left">
                                            <p>2009</p>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 col-md-6 col-lg-4">
                                                <div class="single-timeline-content d-flex" data-aos="fade-left">
                                                    <div class="timeline-text">
                                                        <p>Tahun 2009 berubah menjadi UPT ( Unit Pelayanan Teknis )
                                                            Taman Margasatwa Ragunan.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Single Timeline Content-->
                                    <div class="single-timeline-area">
                                        <div class="timeline-date" data-aos="fade-left">
                                            <p>2010</p>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 col-md-6 col-lg-4">
                                                <div class="single-timeline-content d-flex" data-aos="fade-left">
                                                    <div class="timeline-text">
                                                        <p>Pada tahun 2010 namanya berubah menjadi BLUD ( Badan Layanan
                                                            Umum Daerah ) Taman Margasatwa Ragunan. Saat ini luas Taman
                                                            Margasatwa Ragunan mencapai 147 Hektar dengan koleksi satwa
                                                            2101 ekor satwa dari 220 spesies.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Single Timeline Content-->
                                    <div class="single-timeline-area">
                                        <div class="timeline-date" data-aos="fade-left">
                                            <p>2015</p>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 col-md-6 col-lg-4">
                                                <div class="single-timeline-content d-flex" data-aos="fade-left">
                                                    <div class="timeline-text">
                                                        <p>Pada Tahun 2015 BLUD Taman Margasatwa Ragunan berubah namanya
                                                            menjadi Kantor Pengelola Taman Margasatwa Ragunan sesuai
                                                            dengan Perda Nomor 12 Tahun 2014 tentang Organisasi
                                                            Perangkat Daerah.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</section>

<!-- timeline end -->

<?= $this->endSection(); ?>