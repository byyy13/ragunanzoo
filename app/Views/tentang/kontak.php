<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>
<!-- banner start-->
<section class="promo-primary" data-aos="fade-up">
    <picture>
        <source srcset="/assets/img/about-bg.jpg" media="(min-width: 992px)" /><img class="img--bg"
            src="/assets/img/about-bg.jpg" alt="img" />
    </picture>
    <div class="container">
        <div class="row">
            <div class="col-auto">
                <div class="align-container">
                    <div class="align-container__item"><span class="promo-primary__pre-title">Tentang</span>
                        <h1 class="promo-primary__title"><span></span> <span>Hubungi Kami</span></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- banner end-->
<!-- section start-->
<section class="section" data-aos="zoom-in">
    <div class="container">
        <div class="row bottom-50">
            <div class="col-12">
                <div class="heading heading--primary heading--center">
                    <h2 class="heading__title no-margin-bottom"><span>Hubungi</span> <span>Kami</span></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-10 offset-lg-1 col-xl-8 offset-xl-2">
                <form class="form" id="ajax-form" action="" method="POST">
                    <?= csrf_field(); ?>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <input class="form__field" type="text" name="name" placeholder="Nama Lengkap" />
                        </div>
                        <div class="form-group col-md-6">
                            <input class="form__field" type="email" name="email" placeholder="Alamat Email" />
                        </div>
                        <!-- <div class="col-md-4">
                            <input class="form__field" type="tel" name="phone" placeholder="Kode Keamanan" />
                        </div> -->
                        <div class="form-group col-md-12">
                            <input class="form__field" type="text" name="subject" placeholder="Judul" />
                        </div>
                        <div class="form-group col-12">
                            <textarea class="form__field form__message" name="message"
                                placeholder="Pertanyaan"></textarea>
                        </div>
                        <div class="col-12 text-center">
                            <input class="form__submit" type="submit" value="Kirim" />
                        </div>
                        <!-- <div class="col-12">
                            <div class="alert alert--success alert--filled">
                                <div class="alert__icon">
                                    <svg class="icon">
                                        <use xlink:href="#check"></use>
                                    </svg>
                                </div>
                                <p class="alert__text"><strong>Terima Kasih!</strong> Tanggapan Anda Berhasil Dikirim
                                </p><span class="alert__close">
                                    <svg class="icon">
                                        <use xlink:href="#close"></use>
                                    </svg></span>
                            </div>
                            <div class="alert alert--error alert--filled">
                                <div class="alert__icon">
                                    <svg class="icon">
                                        <use xlink:href="#close"></use>
                                    </svg>
                                </div>
                                <p class="alert__text"><strong>Maaf!</strong> Tanggapan Anda Gagal Dikirim</p><span
                                    class="alert__close">
                                    <svg class="icon">
                                        <use xlink:href="#close"></use>
                                    </svg></span>
                            </div>
                        </div> -->
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- section end-->
<!-- lokasi start-->
<section class="section no-padding-top" data-aos="fade-left">
    <div class="container">
        <div class="row flex-column-reverse flex-lg-row">
            <div class="col-lg-12 col-xl-12">
                <div class="heading heading--primary heading--style-2 align-items-center"><span
                        class="heading__pre-title">LOKASI GEOGRAFIS</span>
                    <h2 class="heading__title"><span>LOKASI GEOGRAFIS</span><br /></h2>
                </div>
                <div class="map-responsive">
                    <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15862.545779631531!2d106.81217139231197!3d-6.311399270664767!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69edfdb17f7b7d%3A0xf9dd2a3f5ab5069a!2sTaman%20Margasatwa%20Ragunan!5e0!3m2!1sid!2sid!4v1686386821928!5m2!1sid!2sid"
                        width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"
                        referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- lokasi end-->

<?= $this->endSection(); ?>