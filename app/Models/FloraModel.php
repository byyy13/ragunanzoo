<?php

namespace App\Models;

use CodeIgniter\Model;

class FloraModel extends Model
{
    protected $tumbuhan = [
        [
            'nama' => 'Belimbing Wuluh',
            'nama-slug' => 'belimbing-wuluh',
            'gambar' => '/assets/img/extended/belimbing.jpg',
            'gambar_background' => '/assets/img/extended/belimbing_bg.jpeg',
            'deskripsi' => 'Dikenal pula dengan nama belimbing masam, belimbing buloh, belimbing besi, atau belimbing botol. Tumbuhan asli Indonesia dan Malaya. Perawakan berupa pohon kecil tinggi sampai 15 m. Daun majemuk, berpasangan, anak daun berbentuk lonjong sampai bundar telur. Bunga kecil, biasanya muncul pada batang utama atau cabang maupun ranting. Kelopak bunga berwarna hijau, mahkota merah keunguan. Buah lonjong, berair, rasa sangat masam.
            Tumbuh sampai ketinggian 750 m dpl. Di Indonesia belimbing wuluh ditanam di  pekarangan atau kebun, terkadang dijumpai tumbuh liar di belukar muda. Perbanyakan melalui biji, cangkokan, atau okulasi. Buah mengandung vitamin C, digunakan untuk bumbu dapur, manisan, atau obat batuk tradisional.',
            'nama_ilmiah' => 'Averrhoa bilimbi L.',
            'kelas' => 'Magnoliopsida',
            'orde' => 'Oxalidales',
            'famili' => 'Oxalidaceae',
            'spesies' => 'Averrhoa bilimbi',
            'genus' => 'Averrhoa',
            'perkembangbiakan' => 'Biji',

        ],
        [
            'nama' => 'Bougenvil',
            'nama-slug' => 'boigenvil',
            'gambar' => '/assets/img/extended/bougenvil.jpg',
            'gambar_background' => '/assets/img/extended/bougenvil_bg.jpg',
            'deskripsi' => 'Bougenvil (Bougainvillea) merupakan tanaman hias populer. Bentuknya adalah pohon kecil yang sukar tumbuh tegak. Keindahannya berasal dari seludang bunganya yang berwarna cerah dan menarik perhatian karena tumbuh dengan rimbunnya.',
            'nama_ilmiah' => 'Bougainvillea',
            'kelas' => 'Magnoliopsida',
            'orde' => 'Caryophyllales',
            'famili' => 'Nyctaginaceae',
            'spesies' => 'Bougainvillea Buttiana',
            'genus' => 'Bougainvillea',
            'perkembangbiakan' => 'Biji',
        ],
        [
            'nama' => 'Asam Selong',
            'nama-slug' => 'asam-selong',
            'gambar' => '/assets/img/extended/asam-selong.jpg',
            'gambar_background' => '/assets/img/extended/asam-selong_bg.jpg',
            'deskripsi' => 'Berasal dari Brazilia, sekarang telah tersebar di daerah tropika lainnya. Perdu atau pohon kecil, tinggi sampai dengan 5 m. Daun muda berwarna merah tua. Bunga kuning susu, kecil. Buah muda  berwarna hijau, menjadi merah tua kehitaman ketika masak, berbentuk bulat berlekuk-lekuk, berasa asam-manis. Tumbuh sampai baik pada ketinggian 300 m dpl., pada tanah masam bertekstur remah. Perbanyakan dengan biji atau cangkokan. Umumnya ditanam sebagai tanaman hias di pekarangan. Berbunga April s.d. Mei dan berbuah Mei s.d. Juni. Buah masak dimakan segar atau dibuat selai. Daun muda digunakan sebagai pengganti teh. Teh asam selong dicampur dengan ektrak buah dan alkohol digunakan sebagai obat.',
            'nama_ilmiah' => 'Eugenia uniﬂora L.',
            'kelas' => 'Magnoliopsida',
            'orde' => 'Myrtales',
            'famili' => 'Myrtaceae',
            'spesies' => 'Eugenia uniﬂora',
            'genus' => 'Eugenia',
            'perkembangbiakan' => 'Biji',
        ],
        [
            'nama' => 'Akasia',
            'nama-slug' => 'akasia',
            'gambar' => '/assets/img/extended/akasia.jpg',
            'gambar_background' => '/assets/img/extended/akasia_bg.jpg',
            'deskripsi' => 'Di Indonesia jenis ini sangat terkenal karena pemanfaatannya sebagai tanaman penghijauan. Berperawakan pohon, tinggi mencapai 15 m, diameter batang 50 cm.  Batang umumnya berbengkok-bengkok dan mempunyai banyak cabang. Kulit batang kelabu coklat beralur sempit dan dangkal. <br><br>
            
            Daun tunggal yang berbentuk bulan sabit merupakan daun semu, daun sejatinya berupa daun majemuk menyirip. Berbunga sepanjang tahun, pembungaan berbentuk bulir yang muncul pada ketiak daun.<br><br>

            Buah berupa polongan terpilin, biji gepeng berwana hitam. Berasal dari Kepulauan Kei, Papua, Papua Nugini, dan Australia Utara.  Di Indonesia banyak ditanam sebagai peteduh atau jalur hijau.<br><br>

            Tumbuh baik pada dataran rendah sampai ketinggian 600 m dpl.  Cukup tahan kekeringan dan dapat tumbuh pada daerah-daerah yang miskin unsur hara, sehingga dijadikan tanaman penghijaun untuk lahan kritis. Mudah tumbuh dan pertumbuhannya sangat cepat, perbanyakan menggunakan biji atau setek.<br><br>

            Biji disemaikan dulu, setelah semai mencapai tinggi tertentu baru dipindah. Dalam penyimpanan yang baik, biji kering dapat disimpan untuk waktu yang lama. Di Jawa akasia akan berbunga lebih banyak pada  bulan Juli s.d. November. Kayu ringan, digunakan untuk perkakas rumah tangga, bahan bangunan, atau dibuat arang berkualitas tinggi.
            ',
            'nama_ilmiah' => 'Acacia auriculiformis Benth.',
            'kelas' => 'Magnoliopsida',
            'orde' => 'Fabales',
            'famili' => 'Fabaceae',
            'spesies' => 'Acacia auriculiformis',
            'genus' => 'Acacia',
            'perkembangbiakan' => 'Biji',
        ],
        [
            'nama' => 'Beringin',
            'nama-slug' => 'beringin',
            'gambar' => '/assets/img/extended/beringin.jpg',
            'gambar_background' => '/assets/img/extended/beringin_bg.jpg',
            'deskripsi' => 'Di Jawa dikenal dengan nama waringin. Secara alami tersebar di Himalaya bagian timur, Myanmar, Jawa, Sulawesi, sampai ke Timor. Dijumpai tumbuh dari pantai sampai ketinggian 1.400 m dpl., pada tempat-tempat tidak terpelihara atau di hutan sekuder maupun primer.  Semai sering tumbuh menumpang pada pohon lain, di batu-batu, pada tembok atau genteng yang berlumut.<br><br>
            Perawakan berupa pohon mencapai tinggi 35 m, tajuk rindang berbentuk payung. Daun bundar telur sampai elip dengan ujung meruncing, hijau mengkilap. Buah muncul pada ranting-ranting, tunggal atau berpasangan, bertuk bulat telur, kecil, bila masak berwarna merah kekuningan kemudian menjadi merah tua. Dari sekian kerabat Ficus di Indonesia, beringin merupakan spesies yang paling dikenal oleh masyarakat, ditanam sebagai peteduh atau dibuat bonsai. Di perdesaan beringin dipercaya sebagai pohon tempat tinggal ruh-ruh halus. Beringin cenderung ditanam di hampir setiap alun-alun kota di Jawa.',
            'nama_ilmiah' => 'Ficus benjamina L.',
            'kelas' => 'Magnoliopsida',
            'orde' => 'Rosales',
            'famili' => 'Moraceae',
            'spesies' => 'Ficus benjamina',
            'genus' => 'Ficus',
            'perkembangbiakan' => 'Biji',
        ],
        [
            'nama' => 'Matoa',
            'nama-slug' => 'matoa',
            'gambar' => '/assets/img/extended/matoa.jpg',
            'gambar_background' => '/assets/img/extended/matoa_bg.jpg',
            'deskripsi' => 'Matoa (Pometia pinnata) adalah tanaman buah khas Papua, tergolong pohon besar dengan tinggi rata-rata 18 meter dengan diameter rata-rata maksimum 100 cm.<br><br>
            Umumnya berbuah sekali dalam setahun. Berbunga pada bulan Juli sampai Oktober dan berbuah 3 atau 4 bulan kemudian. Penyebaran buah matoa di Papua hampir terdapat di seluruh wilayah dataran rendah hingga ketinggian ± 1200 m dpl. Tumbuh baik pada daerah yang kondisi tanahnya kering (tidak tergenang) dengan lapisan tanah yang tebal. Iklim yang dibutuhkan untuk pertumbuhan yang baik adalah iklim dengan curah hujan yang tinggi (>1200 mm/tahun).',
            'nama_ilmiah' => 'Pometia pinnata',
            'kelas' => 'Magnoliopsida',
            'orde' => 'Sapindales',
            'famili' => 'Sapindaceae',
            'spesies' => 'Pometia pinnata',
            'genus' => 'Pometia',
            'perkembangbiakan' => 'Biji',
        ],
        [
            'nama' => 'Kenanga',
            'nama-slug' => 'kenanga',
            'gambar' => '/assets/img/extended/kenanga.jpg',
            'gambar_background' => '/assets/img/extended/kenanga_bg.jpg',
            'deskripsi' => 'Kenanga (Cananga odorata) adalah nama bagi sejenis bunga dan pohon yang menghasilkannya.<br><br>
            Ada dua forma kenanga, yaitu macrophylla, yang dikenal sebagai kenanga biasa, dan genuina, dikenal sebagai kenanga filipina atau ylang-ylang. Selain itu, masih dikenal pula kenanga perdu (Cananga odorata fruticosa), yang banyak ditanam sebagai hiasan di halaman rumah.',
            'nama_ilmiah' => 'Cananga odorata',
            'kelas' => 'Magnoliopsida',
            'orde' => 'Magnoliales',
            'famili' => 'Annonaceae',
            'spesies' => 'Cananga odorata',
            'genus' => 'Cananga',
            'perkembangbiakan' => 'Biji',
        ],
        [
            'nama' => 'Gandaria',
            'nama-slug' => 'gandaria',
            'gambar' => '/assets/img/extended/gandaria.webp',
            'gambar_background' => '/assets/img/extended/gandaria_bg.jpg',
            'deskripsi' => 'Gandaria (Bouea macrophylla Griffith) atau nama lokal lainnya jatake adalah tanaman yang berasal dari kepulauan Indonesia dan Malaysia.<br><br>
            Tanaman ini tumbuh di daerah tropis, dan banyak dibudidayakan di Sumatera dan Thailand.',
            'nama_ilmiah' => 'Bouea macrophylla',
            'kelas' => 'Magnoliopsida',
            'orde' => 'Sapindales',
            'famili' => 'Anacardiaceae',
            'spesies' => 'Bouea macrophylla',
            'genus' => 'Bouea',
            'perkembangbiakan' => 'Biji',
        ],
        [
            'nama' => 'Pinus',
            'nama-slug' => 'pinus',
            'gambar' => '/assets/img/extended/pinus.webp',
            'gambar_background' => '/assets/img/extended/pinus_bg.jpg',
            'deskripsi' => 'Pinus (Pinus merkusii) adalah sebutan bagi sekelompok tumbuhan yang semuanya tergabung dalam marga Pinus.<br><br>
            Di Indonesia penyebutan tusam atau pinus biasanya ditujukan pada tusam Sumatera. Tusam kebanyakan bersifat berumah satu (monoecious), yaitu dalam satu tumbuhan terdapat organ jantan dan betina namun terpisah, meskipun beberapa spesies bersifat setengah berumah dua (sub-dioecious).',
            'nama_ilmiah' => 'Pinus merkusii',
            'kelas' => 'Pinopsida',
            'orde' => 'Pinales',
            'famili' => 'Pinaceae',
            'spesies' => 'Pinus merkusii',
            'genus' => 'Pinus',
            'perkembangbiakan' => 'Biji',

        ],
        [
            'nama' => 'Melinjo',
            'nama-slug' => 'melinjo',
            'gambar' => '/assets/img/extended/melinjo.jpeg',
            'gambar_background' => '/assets/img/extended/melinjo_bg.jpg',
            'deskripsi' => 'Di berbagai daerah dikenal dengan nama melinjo, meninjo, mulieng, batang baguak, belinjo, tangkil, trangkil, so, bagu, dan nama lokal lain. Berperawakan pohon, batang lurus, tinggi dapat mencapai 20 m. Daun lanset sampai bundar telur memanjang. Bunga berbentuk bulir, berkelamin satu, bunga betina selalu tumbuh menyendiri, bunga jantan kadang-kadang menggerombol.<br><br>
            Buah berbentuk bulat telur, berwarna hijau sewaktu muda, kemudian kuning dan menjadi merah setelah tua. Tumbuh liar atau ditanam, adaptif tumbuh di dataran rendah beriklim kering, tetapi dapat tumbuh pula di daerah lembab sampai ketinggian 1.200 m dpl.<br><br>
            Tidak membutuhkan banyak perawatan, tidak banyak mengalami gangguan hama dan penyakit, setelah mulai menghasilkan akan berbunga dan berbuah terus-menerus. Perbanyakan dengan biji atau tunas akar.  Mempunyai sebaran luas, dari Assam sampai Fiji.<br><br>
            Di Indonesia banyak ditanam di pekarangan atau kebun campuran, sebagai tanaman pokok atau tanaman sela. Daun, bunga, dan buah muda maupun tua dapat dimakan. Buah tua dibuat kerupuk emping, hasil utama dari budidaya jenis ini. Serat kulit batang dibuat tali jala, tali pengikat kapal, atau tali panjat.',
            'nama_ilmiah' => 'Gnetum gnemon',
            'kelas' => 'Gnetopsida',
            'orde' => 'Gnetales',
            'famili' => 'Gnetaceae',
            'spesies' => 'Gnetum gnemon',
            'genus' => 'Gnetum',
            'perkembangbiakan' => 'Biji',
        ],
    ];

    public function getAllFlora()
    {
        return $this->tumbuhan;
    }

    public function getFloraBySlug($slug)
    {
        $flora = $this->tumbuhan;
        foreach ($flora as $f) {
            if ($f['nama-slug'] == $slug) {
                return $f;
            }
        }
    }
}