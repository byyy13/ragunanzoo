<?php

namespace App\Models;

use CodeIgniter\Model;

class BeritaModel extends Model
{
    protected $berita = [
        [
            'judul_berita' => 'Perubahan Jam Buka Selama Puasa',
            'slug' => '1',
            'deskripsi_singkat' => 'Unit Pengelola Taman Margasatwa Ragunan mengucapkan Selamat Menunaikan Ibadah Puasa 1444 Hijriah.',
            'deskripsi_super_singkat' => 'Unit Pengelola Taman Margasatwa Ragunan mengucapkan Selamat Menunaikan Ibadah Puasa 1444 Hijriah.',
            'deskripsi_lengkap' => [
                'Unit Pengelola Taman Margasatwa Ragunan mengucapkan Selamat Menunaikan Ibadah Puasa 1444 Hijriah.',
                'Sambut Ramadan dengan keimanan dan raih ketakwaan melalui ibadah puasa Ramadan.',
                'Selama Bulan Ramadhan TMR tetap buka mulai pukul 07:00 - 15:00 WIB.',
            ],
            'gambar' => '/assets/img/extended/berita5.jpg',
            'gambar_lengkap' => [],
            'link_youtube' => '',
            'tanggal' => '23',
            'bulan' => 'Mar',
            'tahun' => '2023',
            'tanggal_lengkap' => '23 Maret 2023',
        ],
        [
            'judul_berita' => 'Ganjil Genap Tidak Berlaku',
            'slug' => '2',
            'deskripsi_singkat' => 'Hallo sahabat ragunan, untuk kamu yang akan berkunjung weekend ini ke Taman Margasatwa Ragunan, kita sudah meniadakan Ganjil Genap ya. Jika selanjutnya ada perubahan akan kita infokan di sosial media kita.',
            'deskripsi_super_singkat' => 'Taman Margasatwa Ragunan sudah meniadakan Ganjil Genap ya',
            'deskripsi_lengkap' => [
                'Hallo sahabat ragunan, untuk kamu yang akan berkunjung weekend ini ke Taman Margasatwa Ragunan, kita sudah meniadakan Ganjil Genap ya. Jika selanjutnya ada perubahan akan kita infokan di sosial media kita.',
                'Tetap Patuhi Protokol kesehatan saat berkunjung ke Taman Margasatwa Ragunan agar #rekreasiamandannyaman',
            ],
            'gambar' => '/assets/img/extended/berita4.jpeg',
            'gambar_lengkap' => [],
            'link_youtube' => '',
            'tanggal' => '18',
            'bulan' => 'Feb',
            'tahun' => '2022',
            'tanggal_lengkap' => '18 Februari 2022',
        ],
        [
            'judul_berita' => 'Pertukaran Satwa, RagunanZoo dengan MedanZoo',
            'slug' => '3',
            'deskripsi_singkat' => 'Dalam rangka kegiatan kerjasama antar LK berupa tukar menukar satwa antara UP Taman Margasatwa Ragunan dengan Medan Zoo  pada tanggal 15 Februari 2019 dilakukan acara serah terima tukar menukar satwa dari Taman Margasatwa Ragunan ke Medan Zoo.',
            'deskripsi_super_singkat' => 'Kerjasama antar LK berupa tukar menukar satwa antara UP Taman Margasatwa Ragunan dengan Medan Zoo  pada tanggal 15 Februari 2019',
            'deskripsi_lengkap' => [
                'Dalam rangka kegiatan kerjasama antar LK berupa tukar menukar satwa antara UP Taman Margasatwa Ragunan dengan Medan Zoo  pada tanggal 15 Februari 2019 dilakukan acara serah terima tukar menukar satwa dari Taman Margasatwa Ragunan ke Medan Zoo. Satwa Dari Medan Zoo telah tiba di Taman Margasatwa Ragunan pada tanggal 14 Februari 2019 pada pukul 05.00 WIB. Sedangkan pengiriman satwa dari Taman Margasatwa Ragunan ke Medan Zoo akan dilaksanakan pada tanggal 15 Februari 2019.',
                'Satwa yang dipertukarkan adalah 2 ekor harimau sumatera dari Medan Zoo dan sepasang Orangutan Sumatera, 2 pasang kapibara dan 3 pasang burung pelikan. Diharapakan dari pertukaran satwa ini dapat terus meningkatkan kesejahteraan satwa Taman Margasatwa Ragunan dan Medan Zoo menjadi lebih baik lagi sebagai lembaga konservasi.',
            ],
            'gambar' => '/assets/img/extended/berita2.jpg',
            'gambar_lengkap' => [
                '/assets/img/extended/berita2.jpg',
                '/assets/img/extended/berita2_1.jpeg',
                '/assets/img/extended/berita2_2.jpg',
                '/assets/img/extended/berita2_3.jpg',
            ],
            'link_youtube' => '',
            'tanggal' => '15',
            'bulan' => 'Feb',
            'tahun' => '2019',
            'tanggal_lengkap' => '15 Februari 2019',
        ],
        [
            'judul_berita' => 'World Wildlife Day - Big Cats: Predators under threat',
            'slug' => '4',
            'deskripsi_singkat' => 'World Wildlife Day 2018 yang diperingati setiap 3 Maret 2018 mengangkat tema “Big Cats: predators under threat”, semoga dengan kampanye penyadartahuan ini, kita semua dapat turut terlibat dalam upaya konservasi mereka.',
            'deskripsi_super_singkat' => 'World Wildlife Day 2018 yang diperingati setiap 3 Maret 2018 mengangkat tema “Big Cats: predators under threat”',
            'deskripsi_lengkap' => [
                'World Wildlife Day 2018 yang diperingati setiap 3 Maret 2018 mengangkat tema “Big Cats: predators under threat”, semoga dengan kampanye penyadartahuan ini, kita semua dapat turut terlibat dalam upaya konservasi mereka.',
                'Salam Lestari!!',
            ],
            'gambar' => '/assets/img/extended/berita1.jpg',
            'gambar_lengkap' => [],
            'link_youtube' => 'https://www.youtube.com/watch?v=_NV4U7qLrJk',
            'tanggal' => '03',
            'bulan' => 'Mar',
            'tahun' => '2018',
            'tanggal_lengkap' => '3 Maret 2018',
        ],
        [
            'judul_berita' => 'Potensi Cuaca Ekstrim Meningkat (13 s.d 20 November 2016), Masyarakat Dihimbau Waspada',
            'slug' => '5',
            'deskripsi_singkat' => 'Badan Meteorologi, Klimatologi dan Geofisika memperkirakan bahwa pada tanggal 13 s.d 20 November 2016 berpotensi terjadi peningkatan intensitas curah hujan disertai kilat/petir dan angin kencang',
            'deskripsi_super_singkat' => 'BMKG memperkirakan bahwa pada tanggal 13 s.d 20 November 2016 berpotensi terjadi peningkatan intensitas curah hujan disertai kilat/petir dan angin kencang',
            'deskripsi_lengkap' => [
                'Badan Meteorologi, Klimatologi dan Geofisika memperkirakan bahwa pada tanggal 13 s.d 20 Nopember 2016 berpotensi terjadi peningkatan intensitas curah hujan disertai kilat/petir dan angin kencang, hal ini disebabkan karena kondisi dinamika atmosfer yang lembab dan basah dan pemanasan yg tinggi serta kondisi lokal yang kuat, meningkatkan kejadian bencana hidrometeorologi di beberapa wilayah rawan.',
                'Dalam beberapa hari kedepan potensi pertumbuhan awan hujan relatif lebih tinggi, diperkirakan potensi hujan lebat disertai kilat, petir serta angin kencang masih terus meningkat dalam seminggu kedepan di wilayah JABODETABEK.',
                'Oleh karena itu BPBD Provinsi DKI Jakarta menghimbau kepada masyarakat agar berhati-hati dan waspada ketika beraktifitas diluar, hujan lebat dengan durasi singkat pada siang/sore hari berpotensi menyebabkan jalanan licin, genangan jalan, banjir, longsor, pohon tumbang dan papan reklame roboh.',
            ],
            'gambar' => '/assets/img/extended/berita3.jpg',
            'gambar_lengkap' => [],
            'link_youtube' => '',
            'tanggal' => '13',
            'bulan' => 'Nov',
            'tahun' => '2016',
            'tanggal_lengkap' => '13 November 2016',
        ],
    ];

    public function getAllBerita()
    {
        return $this->berita;
    }

    public function getBeritaBySlug($slug)
    {
        $berita = $this->berita;
        foreach ($berita as $b) {
            if ($b['slug'] == $slug) {
                return $b;
            }
        }
    }
}