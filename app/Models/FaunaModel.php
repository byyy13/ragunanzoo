<?php

namespace App\Models;

use CodeIgniter\Model;

class FaunaModel extends Model
{
    protected $hewanMamalia = [
        [
            'nama' => 'Beruang Madu',
            'nama-slug' => 'beruang-madu',
            'gambar' => '/assets/img/extended/beruang_1.jpg',
            // cari yg landscape plz!!
            'icon' => '/assets/img/svg/animals_5/006-bear.svg',
            'gambar_background' => '/assets/img/extended/beruang_1.jpg',
            'deskripsi' => 'Beruang madu (Helarctos malayanus) memiliki seluruh tubuh berwarna hitam mengkilap, ekor sangat pendek, moncong abu-abu. Dada atas ada bercak putih kekuningan membentuk huruf “V” atau “C”. Berat tubuh dapat mencapai 60 kg.',
            'kelas' => 'mamalia',
            'orde' => 'karnivora',
            'famili' => 'ursidae',
            'spesies' => 'Helarctos malayanus',
            'genus' => 'Helarctos',
            'perkembangbiakan' => 'Lama kehamilan antara 3-4 bulan, biasanya melahirkan 1-2 ekor anak.',
            'makanan' => 'Buah-buahan, madu, serangga, telur burung, dan bangkai',
            'penyebaran' => 'Asia Tenggara',
            'habitat' => 'Hutan dataran rendah dan hutan pegunungan',
            'masa_hidup' => '20-30 tahun',
            'link_youtube' => 'https://www.youtube.com/watch?v=pKr9EGYdcGA&ab_channel=TamanMargasatwaRagunanJakarta4'
        ],
        [
            'nama' => 'Harimau Sumatera',
            'nama-slug' => 'harimau-sumatera',
            'gambar' => '/assets/img/animal-details.jpg',
            // cari yg landscape plz!!
            'icon' => '/assets/img/svg/animals_5/027-tiger.svg',
            'gambar_background' => '/assets/img/animal-details.jpg',
            'deskripsi' => 'Harimau sumatera (Panthera tigris sumatrae) adalah subspesies harimau yang habitat aslinya di pulau Sumatera. memiliki ciri kulit loreng coklat kekuning-kuningan dengan garis-garis hitam vertikal dari kepala sampai ekor. Tinggi dapat mencapai 60 cm, dengan panjang 250 cm. Merupakan sub spesies Harimau terakhir yang ada di Indonesia.',
            'kelas' => 'mamalia',
            'orde' => 'karnivora',
            'famili' => 'felidae',
            'spesies' => 'Panthera tigris',
            'genus' => 'Panthera',
            'perkembangbiakan' => 'Masa kehamilan sekitar 103 hari. Biasanya melahirkan 2 atau 3 ekor anak harimau sekaligus, dan paling banyak 6 ekor.',
            'makanan' => 'Karnivora (Daging)',
            'penyebaran' => 'Sumatera (endemik)',
            'habitat' => 'Hutan, pantai, sampai pegunungan',
            'masa_hidup' => '20 tahun',
            'link_youtube' => 'https://www.youtube.com/watch?v=4bVAjgeqLxo&ab_channel=TamanMargasatwaRagunanJakarta'
        ],
        [
            'nama' => 'Singa Afrika',
            'nama-slug' => 'singa-afrika',
            'gambar' => '/assets/img/extended/berita1.jpg',
            // cari yg landscape plz!!
            'icon' => '/assets/img/svg/animals_5/034-lion.svg',
            'gambar_background' => '/assets/img/extended/berita1.jpg',
            'deskripsi' => 'Singa (Panthera leo) adalah spesies hewan dari keluarga felidae atau jenis kucing. Singa merupakan hewan yang hidup berkelompok. Biasanya terdiri dari seekor jantan dan banyak betina. Kelompok ini menjaga daerah kekuasaannya.',
            'kelas' => 'mamalia',
            'orde' => 'karnivora',
            'famili' => 'felidae',
            'spesies' => 'Panthera leo',
            'genus' => 'Panthera',
            'perkembangbiakan' => 'Singa dewasa akan bereproduksi pada saat mereka berumur empat tahun. Masa kehamilan rata-rata sekitar 110 hari, sekali melahirkan akan melahirkan 1-6 ekor.',
            'makanan' => 'Karnivora (Daging)',
            'penyebaran' => 'Daerah selatan Eurasia, mulai dari Yunani sampai India',
            'habitat' => 'Savana, padang rumput, hutan, dan semak belukar',
            'masa_hidup' => '20 tahun',
            'link_youtube' => 'https://www.youtube.com/watch?v=RY3qSUeaRIw&pp=ygUUc2luZ2EgYWZyaWthIHJhZ3VuYW4%3D'
        ],
    ];

    protected $hewanReptil = [
        [
            'nama' => 'Komodo',
            'nama-slug' => 'komodo',
            'gambar' => '/assets/img/extended/komodo_makan.webp',
            // cari yg landscape plz!!
            'icon' => '/assets/img/svg/animals_5/047-walrus.svg',
            'gambar_background' => '/assets/img/extended/komodo_makan.webp',
            'deskripsi' => 'Komodo adalah spesies kadal besar yang ditemukan di pulau-pulau Indonesia Komodo, Rinca, Flores, Gili Motang, dan Padar. Merupakan anggota keluarga biawak (Varanidae), dan  spesies kadal terbesar yang masih hidup, panjang maksimalnya dalap mencapai 3 meter (10 kaki) dan beratnya bisa mencapai 70 kilogram.',
            'kelas' => 'Reptilia',
            'orde' => 'Squamata',
            'famili' => 'Varanidae',
            'spesies' => 'V. Komodensis',
            'genus' => 'Varanus',
            'perkembangbiakan' => 'Lama kehamilan antara 7-8 bulan, biasanya hanya 1 kelahiran anak.',
            'makanan' => 'Karnivora (Daging)',
            'penyebaran' => ' Pulau Komodo, Rinca, Nusa Kode, dan Gili Motang (endemik)',
            'habitat' => 'Padang rumput terbuka (sabana), hutan belukar dan pesisir pantai',
            'masa_hidup' => '45 tahun',
            'link_youtube' => 'https://www.youtube.com/watch?v=mOipMWP05Cc&ab_channel=METROTV'
        ],
        [
            'nama' => 'Buaya',
            'nama-slug' => 'buaya',
            'gambar' => '/assets/img/extended/buaya_1.jpg',
            // cari yg landscape plz!!
            'icon' => '/assets/img/svg/animals_5/047-walrus.svg',
            'gambar_background' => '/assets/img/extended/buaya_1.jpg',
            'deskripsi' => 'Buaya adalah reptil bertubuh besar yang hidup di air. Secara ilmiah, buaya meliputi seluruh spesies anggota suku Crocodylidae, termasuk pula buaya sepit (Tomistoma schlegelii). Meski demikian nama ini dapat pula dikenakan secara longgar untuk menyebut ‘buaya’ aligator, kaiman dan gavial; yakni kerabat-kerabat buaya yang berlainan suku.',
            'kelas' => 'Reptilia',
            'orde' => 'Crocodilia',
            'famili' => 'Crocodylidae',
            'spesies' => 'Tomistoma schlegelii',
            'genus' => 'Tomistoma',
            'perkembangbiakan' => 'Buaya berkembang biak dengan bertelur, sehingga disebut dengan hewan ovipar. Telur ini diletakkan di lubang atau sarang berupa gundukan tanah, tergantung pada spesies buaya tersebut.',
            'makanan' => 'Karnivora (Daging)',
            'penyebaran' => 'Pedalaman Sulawesi, Sumatra maupun Kalimantan',
            'habitat' => 'Perairan tawar seperti sungai, danau, rawa dan lahan basah ',
            'masa_hidup' => '70 tahun',
            'link_youtube' => 'https://youtu.be/g1uCPdIM1Pw'
        ]
    ];
    protected $hewanAves = [

        //cari foto yg lebih bagus + ubah icon
        [
            'nama' => 'Cendrawasih',
            'nama-slug' => 'cendrawasih',
            'gambar' => '/assets/img/extended/cendrawasih2.png',
            // cari yg landscape plz!!
            'icon' => '/assets/img/svg/animals/bird.svg',
            'gambar_background' => '/assets/img/extended/cendrawasih2.png',
            'deskripsi' => 'Cendrawasih merupakan sejenis burung pengicau berukuran sedang, dengan panjang sekitar 33 cm,  berwarna kuning dan coklat, dan berparuh kuning. Burung jantan dewasa berukuran 72 cm yang termasuk bulu-bulu hiasan berwarna merah darah dengan ujung berwarna putih pada bagian sisi perutnya, betina berukuran lebih kecil dari burung jantan, dengan muka berwarna coklat tua dan tidak punya bulu-bulu hiasan. Burung jantan memikat pasangan dengan ritual tarian yang memamerkan bulu-bulu hiasannya.',
            'kelas' => 'aves',
            'orde' => 'Passeriformes',
            'famili' => 'Paradisaeidae',
            'spesies' => 'Helarctos malayanus',
            'genus' => 'Paradisaea',
            'perkembangbiakan' => 'Bertelur 2-3 butir dalam satu masa kawin (1 kali dalam 1 tahun)',
            'makanan' => 'Buah-buahan dan serangga',
            'penyebaran' => 'Indonesia',
            'habitat' => 'Hutan dataran rendah',
            'masa_hidup' => '15 tahun',
            'link_youtube' => 'https://www.youtube.com/watch?v=KIYkpwyKEhY'
        ],
        [
            'nama' => 'Burung Undan (Pelikan)',
            'nama-slug' => 'pelikan',
            'gambar' => '/assets/img/extended/pelikan.jpg',
            // cari yg landscape plz!!
            'icon' => '/assets/img/svg/animals/bird-1.svg',
            'gambar_background' => '/assets/img/extended/pelikan.jpg',
            'deskripsi' => 'Burung undan atau pelikan adalah burung air yang memiliki kantung di bawah paruhnya, dan merupakan bagian dari keluarga burung Pelecanidae. Bersama burung pecuk, pecuk ular, gannet, angsa batu, dan cikalang, mereka membentuk ordo Pelecaniformes.',
            'kelas' => 'aves',
            'orde' => 'Pelecaniformes',
            'famili' => 'Pelecanidae',
            'spesies' => 'Pelecanus',
            'genus' => 'Pelecanus onocrotalus',
            'perkembangbiakan' => 'Kopulasi berlangsung segera setelah mendapatkan pasangan dan berlanjut selama 3 hingga 10 hari sebelum telur dikeluarkan.',
            'makanan' => 'Buah-buahan, madu, serangga, telur burung, dan bangkai',
            'penyebaran' => 'Pelikan modern ditemukan di semua benua kecuali Antartika. Umumnya di wilayah hangat, dan mereka tidak dijumpai di wilayah kutub, laut dalam, kepulauan samudra, dan benua Amerika Selatan',
            'habitat' => 'Perairan air tawar dan asin, danau, dan sungai',
            'masa_hidup' => '15-25 tahun',
            'link_youtube' => 'https://www.youtube.com/watch?v=CJmpx4l0A3Q'
        ],
        [
            'nama' => 'Flamingo',
            'nama-slug' => 'flamingo',
            'gambar' => '/assets/img/extended/flamingo3.jpg',
            // cari yg landscape plz!!
            'icon' => '/assets/img/svg/animals/bird-1.svg',
            'gambar_background' => '/assets/img/extended/flamingo3.jpg',
            'deskripsi' => 'Flamingo adalah spesies burung berkaki jenjang yang hidup berkelompok. Mereka berasal dari genus Phoenicopterus dan familia Phoenicopteridae. Burung ini ditemukan di belahan bumi barat dan timur, namun lebih banyak terdapat di belahan timur. Terdapat 4 spesies flamingo di Amerika dan 2 jenis flamingo di Dunia Lama',
            'kelas' => 'aves',
            'orde' => 'Phoenicopteriformes',
            'famili' => 'Phoenicopteridae',
            'spesies' => 'Phoenicopterus roseus',
            'genus' => 'Phoenicopterus',
            'perkembangbiakan' => 'Flamingo mencapai kematangan seksual beberapa tahun setelah menetas dan biasanya mulai berkembang biak di sekitar enam tahun.',
            'makanan' => 'Udang kecil dan algae',
            'penyebaran' => 'Bagian dari Afrika, Eropa Selatan, dan Asia Tenggara',
            'habitat' => 'Danau basa, garam yang besar, atau muara laguna yang biasanya kurang vegetasi',
            'masa_hidup' => '15-25 tahun',
            'link_youtube' => 'https://www.youtube.com/watch?v=QLV_K7DVeyU'
        ],
        [
            'nama' => 'Merak Biru',
            'nama-slug' => 'merak-biru',
            'gambar' => '/assets/img/extended/merak-biru.jpg',
            // cari yg landscape plz!!
            'icon' => '/assets/img/svg/animals/bird-2.svg',
            'gambar_background' => '/assets/img/extended/merak-biru.jpg',
            'deskripsi' => 'Merak biru (Pavo cristatus) adalah burung besar dan berwarna cerah dari keluarga asli burung dari Asia Selatan, namun diperkenalkan di banyak bagian lain dunia seperti Amerika Serikat, Meksiko, Honduras, Kolombia, Guyana, Suriname, Brazil, Uruguay, Argentina, Afrika Selatan, Madagaskar, Papua, dan Australia.',
            'kelas' => 'aves',
            'orde' => 'Galliformes',
            'famili' => 'Phasianidae',
            'spesies' => 'Pavo cristatus',
            'genus' => 'Pavo',
            'perkembangbiakan' => 'Merak jantan mempunyai pasangan lebih dari satu (poligami). Pada musim berbiak. Burung betina biasanya menetaskan tiga sampai enam butir telur.',
            'makanan' => 'Serangga, reptil, cacing, biji-bijian, dan buah-buahan',
            'penyebaran' => 'Tersebar di hutan terbuka dengan padang rumput di India, Pakistan, Sri Lanka, Nepal dan Bhutan',
            'habitat' => 'Hutan hujan',
            'masa_hidup' => '15 tahun',
            'link_youtube' => 'https://www.youtube.com/watch?v=vbihfdgNzdY'
        ]
    ];
    protected $hewanPisces = [
        [
            'nama' => 'Arapaima',
            'nama-slug' => 'arapaima',
            'gambar' => '/assets/img/extended/arapaima2.jpg',
            // cari yg landscape plz!!
            'icon' => '/assets/img/svg/animals_5/031-shark.svg',
            'gambar_background' => '/assets/img/extended/arapaima2.jpg',
            'deskripsi' => 'Arapaima (Arapaima gigas) adalah jenis ikan air tawar terbesar di dunia yang berasal dari perairan daerah tropis Amerika Selatan. Ikan Arapaima dapat tumbuh maksimal sepanjang 3 meter dan berat 200 kilogram.',
            'kelas' => 'Pisces',
            'orde' => 'Osteoglossiformes',
            'famili' => 'Osteoglossidae',
            'spesies' => 'Arapaima',
            'genus' => 'Panthera',
            'perkembangbiakan' => 'Arapaima jantan akan mengerami ribuan telur di mulutnya saat memasuki musim kawin.',
            'makanan' => 'Ikan, buah-buahan, biji-bijian dan serangga',
            'penyebaran' => 'Amerika selatan',
            'habitat' => 'Aliran sungai hutan hujan di Cekungan Amazon, Amerika Selatan dan danau serta rawa di dekatnya',
            'masa_hidup' => '20 tahun',
            'link_youtube' => 'https://www.youtube.com/watch?v=_-S14neV5dc',
        ]
    ];

    public function getAllAnimalByTipe($tipe)
    {
        if ($tipe == 'mamalia') {
            return $this->hewanMamalia;
        } else if ($tipe == 'reptil') {
            return $this->hewanReptil;
        } else if ($tipe == 'aves') {
            return $this->hewanAves;
        } else if ($tipe == 'pisces') {
            return $this->hewanPisces;
        }
    }

    public function getAnimalBySlug($tipe, $slug)
    {
        if ($tipe == 'mamalia') {
            foreach ($this->hewanMamalia as $animal) {
                if ($animal['nama-slug'] === $slug) {
                    return $animal;
                }
            }
        }
        if ($tipe == 'reptil') {
            foreach ($this->hewanReptil as $animal) {
                if ($animal['nama-slug'] === $slug) {
                    return $animal;
                }
            }
        }
        if ($tipe == 'aves') {
            foreach ($this->hewanAves as $animal) {
                if ($animal['nama-slug'] === $slug) {
                    return $animal;
                }
            }
        }
        if ($tipe == 'pisces') {
            foreach ($this->hewanPisces as $animal) {
                if ($animal['nama-slug'] === $slug) {
                    return $animal;
                }
            }
        }
        return null;
    }
}