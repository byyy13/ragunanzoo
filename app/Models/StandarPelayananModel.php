<?php

namespace App\Models;

use CodeIgniter\Model;

class StandarPelayananModel extends Model
{
    protected $standarPelayanan = [
        [
            'nama' => 'Pelayanan Masuk Umum',
            'deskripsi' => 'Standar Pelayanan Masuk Taman Margasatwa Ragunan (Pengunjung Umum).',
            'icon' => 'fa-solid fa-dungeon',
            'slug' => '1',
            'komponen' => [
                'Persyaratan Pelayanan' => [
                    'Memiliki Jakcard dengan saldo mencukupi sebagai sarana transaksi non tunai.',
                    'Masuk melalui pintu elektronik/gate yang sudah ditentukan.'
                ],
                'Sistem, Mekanisme dan Prosedur' => [
                    'Pengunjung membeli/memiliki Jakcard dengan saldo mencukupi sesuai jumlah orang yang akan masuk.',
                    'Petugas loket melayani pembelian dan melakukan pengisian ulang saldo (Top Up) bila Jakcard yang dimilikinya tidak mencukupi.',
                    'Pengunjung masuk melalui pintu elektronik/gate yang sudah ditentukan sesuai antrian dengan menempelkan Jakcard (tap in) pada mesin sensor sebagai pengurang saldo sesuai tarif masuk dan jumlah pengunjung.'
                ],
                'Jangka Waktu' => [
                    '5 (lima) menit per transaksi'
                ],
                'Biaya/Tarif' => [
                    'Dewasa Rp. 4.000,-',
                    'Anak Rp. 3.000,-',
                    'Bus/Truk Besar Rp. 15.000,-',
                    'Bus/Truk Sedang Rp. 12.500,-',
                    'Mobil Rp. 6.000,-',
                    'Motor Rp. 2.500,-',
                    'Sepeda Rp. 1.000,-',
                ],
                'Produk Pelayanan' => [
                    'Jasa layanan Masuk TMR'
                ],
                'Penanganan, Pengaduan, Saran dan Masukan' => [
                    'Kepala  UP Taman Margasatwa Ragunan',
                    'Telepon/Fax : 021- 78847114',
                    'Email : ragunanzoo@jakarta.go.id',
                    'Kotak Saran dan Pengaduan',
                ]
            ],
        ],
        [
            'nama' => 'Tarif Masuk Rombongan',
            'deskripsi' => 'Standar Pelayanan Pemberian Keringanan Tarif Masuk Rombongan Pelajar/Mahasiswa/ Panti Sosial',
            'icon' => 'fa-solid fa-money-check-dollar',
            'slug' => '2',
            'komponen' => [
                'Persyaratan Pelayanan' => [
                    'Mengajukan surat permohonan yang ditandatangani oleh Kepala Sekolah/Bagian Kemahasiswaan/Kepala Panti minimal 3 (tiga) hari sebelum waktu kunjungan.',
                    'Jumlah Pelajar/Mahasiswa/Panti sosial minimal 40 orang.',
                    'Membeli/memiliki kartu Jakcard bagi pemohon/Panitia rombongan.',
                    'Foto copy KTP pemohon/ketua rombongan'
                ],
                'Sistem, Mekanisme dan Prosedur' => [
                    'Pemohon mengajukan permohonan berupa surat melalui fax/email ataupun datang langsung ke petugas.',
                    'Petugas memproses permohonan keringanan tarif masuk.',
                    'Petugas menghubungi pemohon melalui telepon.',
                    'Pemohon menerima surat ijin keringanan tarif masuk Ragunan.',
                    'Pemohon datang di hari H dengan membawa surat permohonan asli (bagi yang mengajukan permohonan via email/fax).'
                ],
                'Jangka Waktu' => [
                    'Nomor 1 s.d 3 : 1 hari',
                    'Nomor 4 s.d 5 : Selasa - Minggu pukul 07.30 s.d 16.00 WIB'
                ],
                'Biaya/Tarif' => [
                    'Dewasa Rp. 4.000,-',
                    'Anak Rp. 3.000,-',
                ],
                'Produk Pelayanan' => [
                    'Surat ijin keringanan tarif masuk rombongan Pelajar/Mahasiswa/Panti Sosial'
                ],
                'Penanganan, Pengaduan, Saran dan Masukan' => [
                    'Kepala  UP Taman Margasatwa Ragunan',
                    'Telepon/Fax : 021- 78847114',
                    'Email : ragunanzoo@jakarta.go.id',
                    'Kotak Saran dan Pengaduan',
                ]
            ],
        ],
        [
            'nama' => 'Pemakaian Fasilitas Panggung, Gedung, Auditorium & Lahan Terbuka',
            'deskripsi' => 'Standar Pelayanan Pemakaian Fasilitas Taman Margasatwa Ragunan Berupa Panggung/Gedung Informasi/Gedung Auditorium Dan Lahan Terbuka.',
            'icon' => 'fa-solid fa-building',
            'slug' => '3',
            'komponen' => [
                'Persyaratan Pelayanan' => [
                    'Mengajukan Surat permohonan yang ditandatangani oleh penanggungjawab minimal 1 (satu) bulan sebelum waktu kunjungan (via email/fax/datang langsung).',
                    'Foto copy KTP penanggungjawab.',
                    'Membeli/memiliki kartu Jakcard bagi penanggung jawab.'
                ],
                'Sistem, Mekanisme dan Prosedur' => [
                    'Pemohon menyerahkan surat permohonan kepada petugas untuk dicatat sebagai surat masuk.',
                    'Petugas memproses permohonan sesuai disposisi/arahan Kepala Unit dan mencatat jadwal permohonan.',
                    'Petugas  menghubungi pemohon melalui telepon, baik untuk permohonan yang di tolak maupun yang di setujui.',
                    'Pemohon yang disetujui untuk melakukan peninjauan lokasi.',
                    'Pemohon melakukan pembayaran tarif pemakaian fasilitas melalui mekanisme transaksi non tunai (transfer) kerekening Unit Pengelola Taman Margasatwa Ragunan.',
                    'Pemohon menyerahkan bukti transfer pembayaran kepada petugas.',
                    'Petugas mengisi formulir reservasi pemakian fasilitas Taman Margasatwa Ragunan untuk ditantangani pemohon dan Kepala Unit.'
                ],
                'Jangka Waktu' => [
                    'Nomor 1 s.d 5 : 2 hari kerja',
                    'Nomor 6 : 2 hari sebelum hari H',
                    'Nomor 7 : 1 hari kerja'
                ],
                'Biaya/Tarif' => [
                    'Panggung : Rp. 250.000,-/hari',
                    'Gedung Informasi : Rp. 300.000,-/hari',
                    'Gedung Auditorium : Rp. 750.000,-/hari',
                    'Lahan Terbuka : Rp. 2.000,-/m2/hari'
                ],
                'Produk Pelayanan' => [
                    'Surat ijin Pemakaian Fasilitas Panggung, Gedung Informasi/ Gedung Auditorium Dan Lahan Terbuka'
                ],
                'Penanganan, Pengaduan, Saran dan Masukan' => [
                    'Kepala  UP Taman Margasatwa Ragunan',
                    'Telepon/Fax : 021- 78847114',
                    'Email : ragunanzoo@jakarta.go.id',
                    'Kotak Saran dan Pengaduan',
                ]
            ],
        ],
        [
            'nama' => 'Pemakaian Fasilitas Lokasi Promo & Penjualan Produk',
            'deskripsi' => 'Standar Pelayanan Pemakaian Fasilitas Taman Margasatwa Ragunan Berupa Lokasi Promo/Penjualan Produk',
            'icon' => 'fa-solid fa-shop',
            'slug' => '4',
            'komponen' => [
                'Persyaratan Pelayanan' => [
                    'Mengajukan Surat permohonan/proposal yang ditandatangani oleh penanggungjawab minimal 14 (empat belas) hari sebelum pelaksanaan (via email/fax/datang langsung).',
                    'Foto copy KTP penanggungjawab.',
                    'Membeli/memiliki kartu Jakcard bagi penanggung jawab.',
                ],
                'Sistem, Mekanisme dan Prosedur' => [
                    'Pemohon menyerahkan surat permohonan/proposal kepada petugas - (Sub Bag. Tata Usaha).',
                    'Petugas memproses permohonan sesuai disposisi/arahan Kepala Unit dan memcatat jadwal permohonan.',
                    'Petugas menghubungi pemohon melalui telepon, baik untuk permohonan yang di tolak maupun yang di setujui.',
                    'Pemohon yang disetujui untuk melakukan peninjauan lokasi - (Seksi Pelayanan dan Informasi).',
                    'Pemohon menyerahkan bukti transfer pembayaran kepada petugas - (Seksi Pelayanan dan Informasi).',
                    'Petugas memproses perjanjian kerjasama yang ditantangani oleh pemohon dan Kepala Unit – (Seksi Pelayanan dan Informasi).',
                    'Pemohon menerima surat ijin dan Surat Perjanjian Kerjasama'
                ],
                'Jangka Waktu' => [
                    'Nomor 1 s.d 4 : 2 hari kerja',
                    'Nomor 5 : 2 hari sebelum hari H',
                    'Nomor 6 : 1 hari kerja',
                    'Nomor 7 : 2  hari kerja'
                ],
                'Biaya/Tarif' => [
                    'Promo/Penjualan Produk : Rp. 1.000.000,- maksimal 3x3 m2/titik lokasi',
                ],
                'Produk Pelayanan' => [
                    'Surat ijin pemakaian fasilitas untuk promo/ penjualan produk',
                    'Perjanjian Kerjasama'
                ],
                'Penanganan, Pengaduan, Saran dan Masukan' => [
                    'Kepala  UP Taman Margasatwa Ragunan',
                    'Telepon/Fax : 021- 78847114',
                    'Email : ragunanzoo@jakarta.go.id',
                    'Kotak Saran dan Pengaduan',
                ]
            ],
        ],
        [
            'nama' => 'Pemakaian Fasilitas Shooting & Pemotretan',
            'deskripsi' => 'Standar Pelayanan Pemakaian Fasilitas Taman Margasatwa Ragunan Berupa Lokasi/Tempat Untuk Shooting/Pemotretan.',
            'icon' => 'fa-solid fa-camera',
            'slug' => '5',
            'komponen' => [
                'Persyaratan Pelayanan' => [
                    'Mengajukan Surat permohonan yang ditandatangani oleh penanggungjawab minimal 1 (satu) bulan sebelum waktu kunjungan dengan mencantumkan jumlah crew dan kendaraan yang digunakan (via email/fax/datang langsung).',
                    'Foto copy KTP penanggungjawab.',
                    'Membeli/memiliki kartu Jakcard bagi penanggung jawab.',
                ],
                'Sistem, Mekanisme dan Prosedur' => [
                    'Pemohon menyerahkan surat permohonan kepada petugas.',
                    'Petugas memproses permohonan sesuai disposisi/arahan Kepala Unit dan memcatat jadwal permohonan.',
                    'Petugas menghubungi pemohon melalui telepon, baik untuk permohonan yang di tolak maupun yang di setujui).',
                    'Pemohon melakukan peninjauan lokasi.',
                    'Pemohon menyerahkan bukti transfer pembayaran kepada petugas.',
                    'Petugas memproses perjanjian kerjasama yang ditantangani oleh pemohon dan Kepala Unit.',
                    'Pemohon menerima surat ijin pemakaian fasilitas dan Perjanjian Kerjasama.'
                ],
                'Jangka Waktu' => [
                    'Nomor 1 s.d 4 : 2 hari kerja',
                    'Nomor 5 : 2 hari sebelum hari H',
                    'Nomor 6 : 1 hari kerja',
                    'Nomor 7 : 2  hari kerja'
                ],
                'Biaya/Tarif' => [
                    'Film Cerita Rp. 1.500.000,-/hari',
                    'Film Iklan Rp. 2.500.000,-/hari',
                    'Film Dokumenter Rp. 500.000,-/hari',
                    'Film Keluarga Rp. 250.000,-/hari',
                    'Program Televisi Rp. 750.000,-/hari',
                    'Video Clip Rp. 1.000.000,-/hari',
                    'Pre Wedding Rp. 250.000,-/hari sejenisnya'
                ],
                'Produk Pelayanan' => [
                    'Surat ijin pemakaian fasilitas untuk Shooting/Pemotretan',
                    'Perjanjian Kerjasama'
                ],
                'Penanganan, Pengaduan, Saran dan Masukan' => [
                    'Kepala  UP Taman Margasatwa Ragunan',
                    'Telepon/Fax : 021- 78847114',
                    'Email : ragunanzoo@jakarta.go.id',
                    'Kotak Saran dan Pengaduan',
                ]
            ],
        ],
        [
            'nama' => 'Penelitian & PKL',
            'deskripsi' => 'Standar Pelayanan Peemberian Ijin Penelitian Dan Praktek Kerja Lapangan.',
            'icon' => 'fa-solid fa-book',
            'slug' => '6',
            'komponen' => [
                'Persyaratan Pelayanan' => [
                    'Bukti konsultasi awal dengan pihak Taman Margasatwa Ragunan.',
                    'Surat permohonan/proposal dari sekolah/Universitas.',
                    'Foto copy Kartu Pelajar/Kartu Tanda Mahasiswa.',
                    'Foto copy KTP.'
                ],
                'Sistem, Mekanisme dan Prosedur' => [
                    'Pemohon mengajukan surat permohonan ijin penelitian dan praktek kerja lapangan yang ditandatangani oleh Kepala Sekolah/Bagian Kemawasiswaan dan membawa bukti konsultasi awal dengan pihak Taman Margasatwa Ragunan kepada petugas.',
                    'Petugas memproses surat ijin Penelitian dan Praktek Kerja Lapangan.',
                    'Petugas mengirim surat ijin penelitian dan praktek kerja lapangan  ke alamat Pemohon.',
                    'Pemohon menerima  Surat Ijin Penelitian dan Praktek Kerja Lapangan'
                ],
                'Jangka Waktu' => [
                    '7 (tujuh) hari kerja',
                ],
                'Biaya/Tarif' => [
                    'Tidak dipungut biaya (Gratis).',
                ],
                'Produk Pelayanan' => [
                    'Jasa layanan pendidikan dan penelitian.',
                ],
                'Penanganan, Pengaduan, Saran dan Masukan' => [
                    'Kepala  UP Taman Margasatwa Ragunan',
                    'Telepon/Fax : 021- 78847114',
                    'Email : ragunanzoo@jakarta.go.id',
                    'Kotak Saran dan Pengaduan',
                ]
            ],
        ],
        [
            'nama' => 'Pemandu Wisata',
            'deskripsi' => 'Standar Pelayanan Jasa Pemandu Wisata/Guide.',
            'icon' => 'fa-solid fa-map-location-dot',
            'slug' => '7',
            'komponen' => [
                'Persyaratan Pelayanan' => [
                    'Mengajukan Surat permohonan/proposal yang ditandatangani oleh penanggungjawab minimal 14 (empat belas) hari sebelum pelaksanaan (via email/fax/datang langsung).',
                    'Jumlah rombongan maksimal 30 orang/pemandu/2 jam.',
                    'Foto copy KTP penanggungjawab.',
                    'Membeli/memiliki kartu Jakcard bagi penanggung jawab.',
                ],
                'Sistem, Mekanisme dan Prosedur' => [
                    'Pemohon menyerahkan surat permohonan/proposal kepada petugas (bagi pemohon yang datang langsung).',
                    'Petugas memproses surat permohonan dan menghubungi pemohon untuk proses tindaklanjut.',
                    'Pemohon menyerahkan surat permohonan asli (bagi pemohon yang mengirim via email/fax) dan bukti transfer pembayaran kepada petugas.',
                    'Pemohon menerima surat persetujuan jasa pemandu wisata'
                ],
                'Jangka Waktu' => [
                    'Nomor 1 s.d 2 : 2 hari kerja',
                    'Nomor 3 : 1 hari kerja',
                    'Nomor 4 : 2 hari kerja'
                ],
                'Biaya/Tarif' => [
                    'Pemandu Bhs Indonesia : Rp. 75.000,-/2 jam',
                    'Pemandu Bhs Inggris : Rp.100.000,-/2 jam'
                ],
                'Produk Pelayanan' => [
                    'Jasa pemandu wisata.',
                ],
                'Penanganan, Pengaduan, Saran dan Masukan' => [
                    'Kepala  UP Taman Margasatwa Ragunan',
                    'Telepon/Fax : 021- 78847114',
                    'Email : ragunanzoo@jakarta.go.id',
                    'Kotak Saran dan Pengaduan',
                ]
            ],
        ],
        [
            'nama' => 'Konsultasi',
            'deskripsi' => 'Standar Pelayanan Pemberian Konsultasi terkait Taman Margasatwa Ragunan.',
            'icon' => 'fa-solid fa-handshake',
            'slug' => '8',
            'komponen' => [
                'Persyaratan Pelayanan' => [
                    'Foto copy kartu tanda pengenal.',
                    'Membeli/memiliki kartu Jakcard bagi penanggung jawab.',
                ],
                'Sistem, Mekanisme dan Prosedur' => [
                    'Pemohon diterima dan dicatat identitas serta keperluannya oleh petugas.',
                    'Petugas memproses dan mengarahkan sesuai keperluan pemohon.',
                    'Petugas memberikan konsultasi.',
                    'Pemohon menandatangani lembar konsultasi.',
                    'Petugas menyimpan lembaran konsultasi dan kelengkapan berkas.',
                    'Pemohon selesai menerima konsultasi'
                ],
                'Jangka Waktu' => [
                    '30 menit (Selasa s.d Minggu pukul 07.30 s.d 16.00 WIB)',
                ],
                'Biaya/Tarif' => [
                    'Tidak dipungut biaya (gratis)',
                ],
                'Produk Pelayanan' => [
                    'Konsultasi terkait Taman Margasatwa Ragunan.',
                ],
                'Penanganan, Pengaduan, Saran dan Masukan' => [
                    'Kepala  UP Taman Margasatwa Ragunan',
                    'Telepon/Fax : 021- 78847114',
                    'Email : ragunanzoo@jakarta.go.id',
                    'Kotak Saran dan Pengaduan',
                ]
            ],
        ],
    ];

    public function getAllStandarPelayanan()
    {
        return $this->standarPelayanan;
    }
}